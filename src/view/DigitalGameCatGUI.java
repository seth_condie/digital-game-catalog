package view;

import java.awt.event.*;

import controller.Program;
import controller.Globals;
import controller.objectClasses.AdvancedSearchFilters;
import controller.objectClasses.System;
import controller.objectClasses.Toy;
import controller.objectClasses.Video_Game;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The GUI for the program
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public class DigitalGameCatGUI extends JFrame {

    private static final int FRAME_INIT_SCREEN_HEIGHT = 850;
    private static final int FRAME_INIT_SCREEN_WIDTH = 825;

    public static int otherSystemsFields = 0;
    public static int repeatSystemsFields = 0;
    public static int reReleaseYearsFields = 0;

    private Map<String, String> AestheticMap = new HashMap<>();

    private Program program;

    private List<String> systemComboBoxList;
    private List<String> genreComboBoxList;
    private List<String> aestheticComboBoxList = new ArrayList<>();
    private List<String> completionLevelComboBoxList = new ArrayList<>();
    private List<String> ratingComboBoxList = new ArrayList<>();
    private List<String> thoughtsTypeComboBoxList = new ArrayList<>();

    private int openGameId = Globals.MISSINGNO;
    private int openSystemId = Globals.MISSINGNO;
    private int openToyId = Globals.MISSINGNO;

    private String openGenre = null;

    public DigitalGameCatGUI(Program program) {
        this.program = program;
        initComponents();
        this.setSize(FRAME_INIT_SCREEN_WIDTH, FRAME_INIT_SCREEN_HEIGHT);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        AestheticMap.put(Globals.NONE, Globals.NONE);
        AestheticMap.put(Globals.Aesthetic.ABNEGATION, Globals.Aesthetic.ABNEGATION + " - " + Globals.Aesthetic.ABNEGATION_DESC);
        AestheticMap.put(Globals.Aesthetic.CHALLENGE, Globals.Aesthetic.CHALLENGE + " - " + Globals.Aesthetic.CHALLENGE_DESC);
        AestheticMap.put(Globals.Aesthetic.COMPETITION, Globals.Aesthetic.COMPETITION + " - " + Globals.Aesthetic.COMPETITION_DESC);
        AestheticMap.put(Globals.Aesthetic.DISCOVERY, Globals.Aesthetic.DISCOVERY + " - " + Globals.Aesthetic.DISCOVERY_DESC);
        AestheticMap.put(Globals.Aesthetic.EXPRESSION, Globals.Aesthetic.EXPRESSION + " - " + Globals.Aesthetic.EXPRESSION_DESC);
        AestheticMap.put(Globals.Aesthetic.FANTASY, Globals.Aesthetic.FANTASY + " - " + Globals.Aesthetic.FANTASY_DESC);
        AestheticMap.put(Globals.Aesthetic.FELLOWSHIP, Globals.Aesthetic.FELLOWSHIP + " - " + Globals.Aesthetic.FELLOWSHIP_DESC);
        AestheticMap.put(Globals.Aesthetic.NARRATIVE, Globals.Aesthetic.NARRATIVE + " - " + Globals.Aesthetic.NARRATIVE_DESC);
        AestheticMap.put(Globals.Aesthetic.SENSATION, Globals.Aesthetic.SENSATION + " - " + Globals.Aesthetic.SENSATION_DESC);
        AestheticMap.put(Globals.Aesthetic.THINKING, Globals.Aesthetic.THINKING + " - " + Globals.Aesthetic.THINKING_DESC);
        this.newGUISetup();
        panelDebug.setVisible(false);
        this.setVisible(true);
    }

    private void menuItemLoadActionPerformed() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String name = f.getName();
                return name.endsWith(".dgc");
            }

            @Override
            public String getDescription() {
                return "Digital Game Catalog .dgc";
            }
        });
        //disable the open "all files" option
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setDialogTitle("Open an Existing Database");
        //this line causes the dialog box to open in the project directory
        fileChooser.setCurrentDirectory(new File("."));
        int returnValue = fileChooser.showOpenDialog(this);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            addToDebugPanel("Attempting to open" + file.getName());
            if (program.openDatabase(file.getPath())) {
                program.databaseOpenSuccessfully();
                addToDebugPanel("Open Successful");
            } else {
                addToDebugPanel("Open Failed");
            }
        } else {
            addToDebugPanel("Open Canceled");
        }
    }

    private void menuItemNewActionPerformed() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JPanel innerPanel = new JPanel();
        JLabel label = new JLabel("New Database Name: ");
        innerPanel.add(label);
        JTextField inputName = new JTextField(12);
        innerPanel.add(inputName);
        innerPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        panel.add(innerPanel);
        JCheckBox defaultDataBox = new JCheckBox("Include Default Data");
        defaultDataBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        panel.add(defaultDataBox);
        Object[] options = {"Create", "Cancel"};
        int create = JOptionPane.showOptionDialog(this, //the parent object
                panel,                                  //the input to be displayed in the dialog box
                "Create A New Database",                //the title
                JOptionPane.OK_CANCEL_OPTION,           //the options buttons
                JOptionPane.QUESTION_MESSAGE,           //the display for the options buttons, enables custom button names
                null,                                   //the icon
                options,                                //the custom button names, see above
                null);                                  //the button tile
        if (create == JOptionPane.OK_OPTION) {
            String dbName = inputName.getText();
            if (dbName.length() < 1) {
                dbName = "NewDatabase";
            }
            dbName = dbName + ".dgc";
            boolean defaultData = defaultDataBox.isSelected();
            addToDebugPanel("Will try to create a new database with the name: " + dbName);
            if (defaultData) {
                addToDebugPanel("Will include the default data");
            } else {
                addToDebugPanel("Will NOT include the default data");
            }
            program.createNewDatabase(dbName, defaultData);
        } else {
            addToDebugPanel("Database Creation Canceled");
        }
    }

    private void menuItemVersionActionPerformed() {
        JOptionPane.showMessageDialog(this, "Digital Games Catalog\nVersion: " + Globals.CURRENT_VERSION, "About", JOptionPane.PLAIN_MESSAGE);
    }

    private void menuItemViewDebugPanelActionPerformed() {
        panelDebug.setVisible(menuItemViewDebugPanel.getState());
        if (menuItemViewDebugPanel.getState()) {
            splitPaneLarge.setDividerLocation(.75);
        }
    }

    private void buttonAddOtherSystemActionPerformed() {
        switch (otherSystemsFields) {
            case 0:
                panelOtherSystems.add(comboBoxOtherSystems0);
                comboBoxOtherSystems0.setSelectedIndex(systemComboBoxList.indexOf("none"));
                otherSystemsFields++;
                break;
            case 1:
                panelOtherSystems.add(comboBoxOtherSystems1);
                comboBoxOtherSystems1.setSelectedIndex(systemComboBoxList.indexOf("none"));
                otherSystemsFields++;
                break;
            case 2:
                panelOtherSystems.add(comboBoxOtherSystems2);
                comboBoxOtherSystems2.setSelectedIndex(systemComboBoxList.indexOf("none"));
                otherSystemsFields++;
                break;
            case 3:
                panelOtherSystems.add(comboBoxOtherSystems3);
                comboBoxOtherSystems3.setSelectedIndex(systemComboBoxList.indexOf("none"));
                buttonAddOtherSystem.setEnabled(false);
                break;
            default:
                addToDebugPanel("ERROR when adding a new other systems field");
        }
        panelOtherSystems.revalidate();
        panelOtherSystems.repaint();
    }

    private void buttonAddRepeatSystemsActionPerformed() {
        switch (repeatSystemsFields) {
            case 0:
                panelRepeatSystems.add(comboBoxRepeatSystems0);
                comboBoxRepeatSystems0.setSelectedIndex(systemComboBoxList.indexOf("none"));
                repeatSystemsFields++;
                break;
            case 1:
                panelRepeatSystems.add(comboBoxRepeatSystems1);
                comboBoxRepeatSystems1.setSelectedIndex(systemComboBoxList.indexOf("none"));
                repeatSystemsFields++;
                break;
            case 2:
                panelRepeatSystems.add(comboBoxRepeatSystems2);
                comboBoxRepeatSystems2.setSelectedIndex(systemComboBoxList.indexOf("none"));
                repeatSystemsFields++;
                break;
            case 3:
                panelRepeatSystems.add(comboBoxRepeatSystems3);
                comboBoxRepeatSystems3.setSelectedIndex(systemComboBoxList.indexOf("none"));
                buttonAddRepeatSystems.setEnabled(false);
                break;
            default:
                addToDebugPanel("ERROR when adding the next repeat systems field");
        }
        panelRepeatSystems.revalidate();
        panelRepeatSystems.repaint();
    }

    private void buttonAddReReleaseYearActionPerformed() {
        switch (reReleaseYearsFields) {
            case 0:
                panelReReleaseYears.add(textFieldReReleaseYear0);
                textFieldReReleaseYear0.setText("");
                reReleaseYearsFields++;
                break;
            case 1:
                panelReReleaseYears.add(textFieldReReleaseYear1);
                textFieldReReleaseYear1.setText("");
                reReleaseYearsFields++;
                break;
            case 2:
                panelReReleaseYears.add(textFieldReReleaseYear2);
                textFieldReReleaseYear2.setText("");
                reReleaseYearsFields++;
                break;
            case 3:
                panelReReleaseYears.add(textFieldReReleaseYear3);
                textFieldReReleaseYear3.setText("");
                buttonAddReReleaseYear.setEnabled(false);
                break;
            default:
                addToDebugPanel("ERROR when adding the next reRelease field");
        }
        panelReReleaseYears.revalidate();
        panelReReleaseYears.repaint();
    }

    private void buttonNewEntryActionPerformed() {
        openGameId = Globals.MISSINGNO;

        textFieldGameTitle.setText("");
        textFieldGameTitle.setEnabled(true);
        comboBoxSystem.setSelectedIndex(systemComboBoxList.indexOf(Globals.NONE));
        comboBoxSystem.setEnabled(true);
        List defaultList = new ArrayList<>();
        defaultList.add(Globals.NONE);
        setupOtherSystemsDisplay(defaultList);
        comboBoxOtherSystems0.setEnabled(true);
        buttonAddOtherSystem.setEnabled(true);
        setupRepeatSystemsDisplay(defaultList);
        comboBoxRepeatSystems0.setEnabled(true);
        buttonAddRepeatSystems.setEnabled(true);
        textFieldMinPlayers.setText("");
        textFieldMinPlayers.setEnabled(true);
        textFieldMaxPlayers.setText("");
        textFieldMaxPlayers.setEnabled(true);
        textFieldDeveloper.setText("");
        textFieldDeveloper.setEnabled(true);
        textFieldPublisher.setText("");
        textFieldPublisher.setEnabled(true);
        textFieldOriginalReleaseYear.setText("");
        textFieldOriginalReleaseYear.setEnabled(true);
        List newList = new ArrayList<>();
        newList.add("");
        setupReReleaseYearDisplay(newList);
        textFieldReReleaseYear0.setEnabled(true);
        buttonAddReReleaseYear.setEnabled(true);
        comboBoxGenre.setSelectedIndex(genreComboBoxList.indexOf(Globals.NONE));
        comboBoxGenre.setEnabled(true);
        textFieldSeries.setText("");
        textFieldSeries.setEnabled(true);
        buttonDeleteGame.setEnabled(false);

        buttonSaveEntry.setEnabled(true);

        comboBoxCoreAesthetic.setSelectedIndex(aestheticComboBoxList.indexOf(Globals.NONE));
        comboBoxCoreAesthetic.setEnabled(true);
        comboBoxOtherAesthetic.setSelectedIndex(aestheticComboBoxList.indexOf(Globals.NONE));
        comboBoxOtherAesthetic.setEnabled(true);
        textFieldRecommendedFor.setText("");
        textFieldRecommendedFor.setEnabled(true);
        comboBoxCompletionLevel.setSelectedIndex(completionLevelComboBoxList.indexOf(Globals.CompletionLevel.WANT));
        comboBoxCompletionLevel.setEnabled(true);
        comboBoxRating.setSelectedIndex(ratingComboBoxList.indexOf(Globals.Rating.BACKLOGGED));
        comboBoxRating.setEnabled(true);
        textAreaComments.setText("");
        textAreaComments.setEnabled(true);
        buttonOpenLink.setEnabled(true);
        textFieldThoughtsLink.setText("");
        textFieldThoughtsLink.setEnabled(true);
        comboBoxThoughtsType.setSelectedIndex(thoughtsTypeComboBoxList.indexOf(Globals.ThoughtsType.PLACEHOLDER));
        comboBoxThoughtsType.setEnabled(true);
        checkBoxRevisit.setSelected(false);
        checkBoxRevisit.setEnabled(true);
        checkBoxCouch.setSelected(false);
        checkBoxCouch.setEnabled(true);
        checkBoxHallOfFame.setSelected(false);
        checkBoxHallOfFame.setEnabled(true);

        tabbedPaneGames.setSelectedIndex(0);
        textFieldGameTitle.requestFocus();
    }

    private void buttonSaveEntryActionPerformed() {
        Video_Game tempGame = new Video_Game(openGameId);
        //error check fields first
        if (textFieldGameTitle.getText().length() < 1) {
            JOptionPane.showMessageDialog(this, "Please enter a game title before saving.", "Game Title Missing", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (textAreaComments.getText().length() >= 2255) {
            JOptionPane.showMessageDialog(this, "The comments field is too long, please reduce the comments field to 2255 characters to save.", "Too Many Comments", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (textFieldThoughtsLink.getText().length() >= 1048) {
            JOptionPane.showMessageDialog(this, "The thoughts link field is too long, please reduce it to 1048 characters to save.", "Thoughts Link Too Long", JOptionPane.ERROR_MESSAGE);
            return;
        }
        //check each number field to see if it it is there
        //if so then try to cast it to a number
        //otherwise fill in the blank number holder instead
        if (textFieldMinPlayers.getText().length() > 0) {
            try {
                tempGame.setPlayerMin(Integer.parseInt(textFieldMinPlayers.getText()));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Make sure that the player minimum is a whole number", "Player Minimum Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        } else {
            tempGame.setPlayerMin(Globals.MISSINGNO);
        }
        if (textFieldMaxPlayers.getText().length() > 0) {
            try {
                tempGame.setPlayerMax(Integer.parseInt(textFieldMaxPlayers.getText()));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Make sure that the player maximum is a whole number", "Player Maximum Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        } else {
            tempGame.setPlayerMax(Globals.MISSINGNO);
        }
        if (textFieldOriginalReleaseYear.getText().length() > 0) {
            try {
                tempGame.setOriginalReleaseYear(Integer.parseInt((textFieldOriginalReleaseYear.getText())));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Make sure that the Originally Released field is a whole number", "Originally Released Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        List<Integer> reReleaseYears = new ArrayList<>();
        if (textFieldReReleaseYear0.getText().length() > 0) {
            try {
                reReleaseYears.add(Integer.valueOf(textFieldReReleaseYear0.getText()));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Make sure that the ReReleased first field is a whole number", "ReReleased Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        if (textFieldReReleaseYear1.getText().length() > 0) {
            try {
                reReleaseYears.add(Integer.valueOf(textFieldReReleaseYear1.getText()));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Make sure that the ReReleased second field is a whole number", "ReReleased Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        if (textFieldReReleaseYear2.getText().length() > 0) {
            try {
                reReleaseYears.add(Integer.valueOf(textFieldReReleaseYear2.getText()));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Make sure that the ReReleased third field is a whole number", "ReReleased Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        if (textFieldReReleaseYear3.getText().length() > 0) {
            try {
                reReleaseYears.add(Integer.valueOf(textFieldReReleaseYear3.getText()));
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(this, "Make sure that the ReReleased fourth field is a whole number", "ReReleased Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        tempGame.setReReleaseYear(reReleaseYears);

        //gather the information from the general tab
        tempGame.setTitle(textFieldGameTitle.getText());
        tempGame.setSystem((String) comboBoxSystem.getSelectedItem());
        List<String> otherSystems = new ArrayList<>();
        if (comboBoxOtherSystems0.getSelectedIndex() != 0) {
            String newOtherSystem = (String) comboBoxOtherSystems0.getSelectedItem();
            if (!newOtherSystem.equals(Globals.NONE)) {
                otherSystems.add(newOtherSystem);
            }
        }
        if (comboBoxOtherSystems1.getSelectedIndex() != 0) {
            String newOtherSystem = (String) comboBoxOtherSystems1.getSelectedItem();
            if (!newOtherSystem.equals(Globals.NONE) && !otherSystems.contains(newOtherSystem)) {
                otherSystems.add(newOtherSystem);
            }
        }
        if (comboBoxOtherSystems2.getSelectedIndex() != 0) {
            String newOtherSystem = (String) comboBoxOtherSystems2.getSelectedItem();
            if (!newOtherSystem.equals(Globals.NONE) && !otherSystems.contains(newOtherSystem)) {
                otherSystems.add(newOtherSystem);
            }
        }
        if (comboBoxOtherSystems3.getSelectedIndex() != 0) {
            String newOtherSystem = (String) comboBoxOtherSystems3.getSelectedItem();
            if (!newOtherSystem.equals(Globals.NONE) && !otherSystems.contains(newOtherSystem)) {
                otherSystems.add(newOtherSystem);
            }
        }
        tempGame.setOtherSystems(otherSystems);
        List<String> repeatSystems = new ArrayList<>();
        if (comboBoxRepeatSystems0.getSelectedIndex() != 0) {
            String newRepeatSystem = (String) comboBoxRepeatSystems0.getSelectedItem();
            if (!newRepeatSystem.equals(Globals.NONE)) {
                repeatSystems.add(newRepeatSystem);
            }
        }
        if (comboBoxRepeatSystems1.getSelectedIndex() != 0) {
            String newRepeatSystem = (String) comboBoxRepeatSystems1.getSelectedItem();
            if (!newRepeatSystem.equals(Globals.NONE) && !repeatSystems.contains(newRepeatSystem)) {
                repeatSystems.add(newRepeatSystem);
            }
        }
        if (comboBoxRepeatSystems2.getSelectedIndex() != 0) {
            String newRepeatSystem = (String) comboBoxRepeatSystems2.getSelectedItem();
            if (!newRepeatSystem.equals(Globals.NONE) && !repeatSystems.contains(newRepeatSystem)) {
                repeatSystems.add(newRepeatSystem);
            }
        }
        if (comboBoxRepeatSystems3.getSelectedIndex() != 0) {
            String newRepeatSystem = (String) comboBoxRepeatSystems3.getSelectedItem();
            if (!newRepeatSystem.equals(Globals.NONE) && !repeatSystems.contains(newRepeatSystem)) {
                repeatSystems.add(newRepeatSystem);
            }
        }
        tempGame.setRepeatSystems(repeatSystems);
        tempGame.setDeveloper(textFieldDeveloper.getText());
        tempGame.setPublisher(textFieldPublisher.getText());
        tempGame.setGenre((String) comboBoxGenre.getSelectedItem());
        tempGame.setSeries(textFieldSeries.getText());

        //gather the information from the details tab
        String coreAesthetic = (String) comboBoxCoreAesthetic.getSelectedItem();
        if (!coreAesthetic.equals(Globals.NONE)) {
            coreAesthetic = coreAesthetic.substring(0, coreAesthetic.indexOf("-")).trim();
        }
        tempGame.setCoreAesthetic(coreAesthetic);
        String otherAesthetic = (String) comboBoxOtherAesthetic.getSelectedItem();
        if (!otherAesthetic.equals(Globals.NONE)) {
            otherAesthetic = otherAesthetic.substring(0, otherAesthetic.indexOf("-")).trim();
        }
        tempGame.setOtherAesthetic(otherAesthetic);
        tempGame.setRecommendedFor(textFieldRecommendedFor.getText());
        tempGame.setCompletionLevel((String) comboBoxCompletionLevel.getSelectedItem());
        tempGame.setRating((String) comboBoxRating.getSelectedItem());
        tempGame.setComments(textAreaComments.getText());
        tempGame.setThoughtsLink(textFieldThoughtsLink.getText());
        tempGame.setThoughtsType((String) comboBoxThoughtsType.getSelectedItem());
        tempGame.setRevisit(checkBoxRevisit.isSelected());
        tempGame.setCouch(checkBoxCouch.isSelected());
        tempGame.setHallOfFame(checkBoxHallOfFame.isSelected());

        //try to write to the database
        if (program.editDatabaseWithGame(tempGame)) {
            addToDebugPanel(tempGame.getTitle() + " successfully updated in the database");
            buttonDeleteGame.setEnabled(true);
        } else {
            addToDebugPanel("Failure editing the database for game " + tempGame.getTitle() + "there could also have been a uniqueness problem with the title");
        }
    }

    private void buttonSaveSystemActionPerformed() {
        if (textFieldSystemName.getText().length() < 1) {
            JOptionPane.showMessageDialog(this, "Please enter a system name before saving.", "System Name Missing", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (textFieldSystemName.getText().contains("\'")) {
            JOptionPane.showMessageDialog(this, "No single quotes allowed in systems, please remove any '", "Single Quote Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        int gen;
        try {
            gen = Integer.valueOf(textFieldGeneration.getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Make sure that the Generation is a whole number", "Generation Input Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String tempName = textFieldSystemName.getText();
        boolean handheld = checkBoxIsHandheld.isSelected();
        if (program.editDatabaseWithSystem(new System(openSystemId, tempName, gen, handheld))) {
            buttonDeleteSystem.setEnabled(true);
            buttonNewEntryActionPerformed();
            program.runLastSearch();
        }
    }

    private void buttonSaveGenreActionPerformed() {
        if (textFieldGenreName.getText().length() < 1) {
            JOptionPane.showMessageDialog(this, "Please enter a genre before saving.", "Genre Missing", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (textFieldGenreName.getText().contains("\'")) {
            JOptionPane.showMessageDialog(this, "No single quotes allowed in genres, please remove any '", "Single Quote Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String tempGenre = textFieldGenreName.getText();
        if (program.editDatabaseWithGenre(tempGenre, openGenre)) {
            buttonDeleteGenre.setEnabled(true);
            buttonNewEntryActionPerformed();
            program.runLastSearch();
            updateOpenGenre(tempGenre);
        }
    }

    private void buttonSaveToyActionPerformed() {
        if (textFieldToyName.getText().length() < 1) {
            JOptionPane.showMessageDialog(this, "Please enter a toy name before saving.", "Toy Name Missing", JOptionPane.ERROR_MESSAGE);
            return;
        }
        String tempName = textFieldToyName.getText();
        String tempSet = textFieldToySet.getText();
        String tempPlatform = textFieldPlatform.getText();
        if (program.editDatabaseWithToy(new Toy(openToyId, tempName, tempSet, tempPlatform))) {
            buttonDeleteToy.setEnabled(true);
        }
    }

    public void buttonSearchActionPerformed() {
        String title = textFieldTitleSearch.getText();
        String system = (String) comboBoxSystemSearch.getSelectedItem();
        String genre = (String) comboBoxGenreSearch.getSelectedItem();
        boolean repeatSearch = checkBoxRepeatSystemNormalSearch.isSelected();
        program.searchResultsVideoGames(title, system, genre, repeatSearch);
    }

    private void buttonAdvanceSearchActionPerformed() {
        AdvanceSearch openWindow = new AdvanceSearch(this,
                Globals.ADVANCE_SEARCH_LOCATION_SEARCH,
                systemComboBoxList,
                genreComboBoxList,
                aestheticComboBoxList,
                completionLevelComboBoxList,
                ratingComboBoxList,
                thoughtsTypeComboBoxList,
                program.getLastAdvancedSearchFilters());
    }

    private void buttonDeleteGameActionPerformed() {
        program.deleteGameFromDatabase(openGameId);
    }

    private void buttonDeleteSystemActionPerformed() {
        if (program.deleteSystemFromDatabase(openSystemId)) {
            addToDebugPanel("System deleted from database");
            updateOpenSystemId(Globals.MISSINGNO);
            buttonDeleteSystem.setEnabled(false);
        } else {
            addToDebugPanel("Either the system is in use or there was an error when accessing the database");
        }
    }

    private void buttonDeleteToyActionPerformed() {
        if (program.deleteToyFromDatabase(openToyId)) {
            addToDebugPanel("Toy deleted from database");
            updateOpenToyId(Globals.MISSINGNO);
            buttonDeleteToy.setEnabled(false);
        }

    }

    private void buttonDeleteGenreActionPerformed() {
        if (program.deleteGenreFromDatabase(openGenre)) {
            addToDebugPanel(openGenre + " deleted from the database");
            updateOpenGenre(Globals.NEW_GENRE);
            buttonDeleteGenre.setEnabled(false);
        } else {
            addToDebugPanel("Either the genre is in use or there was an error when accessing the database");
        }
    }

    public void buttonSystemSearchActionPerformed() {
        String filter = textFieldSystemSearchFilter.getText();
        if (filter.contains("\'")) {
            JOptionPane.showMessageDialog(this, "Single quotes are not allowed in systems, please remove ' from search", "Single Quote Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        program.searchResultsSystems(filter);
    }

    public void buttonGenreSearchActionPerformed() {
        String filter = textFieldGenreFilter.getText();
        if (filter.contains("\'")) {
            JOptionPane.showMessageDialog(this, "Single quotes are not allowed in genres, please remove ' from search", "Single Quote Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        program.searchResultsGenre(filter);
    }

    public void buttonToysSearchActionPerformed() {
        String nameFilter = textFieldToysFilter.getText();
        String toySetFilter = textFieldToysSetFilter.getText();
        String platformFilter = textFieldPlatformFilter.getText();
        program.searchResultsToys(nameFilter, toySetFilter, platformFilter);
    }

    private void buttonNewSystemActionPerformed() {
        openSystemId = Globals.MISSINGNO;
        buttonSaveSystem.setEnabled(true);
        textFieldSystemName.setText("");
        textFieldSystemName.setEnabled(true);
        textFieldGeneration.setText("");
        textFieldGeneration.setEnabled(true);
        checkBoxIsHandheld.setSelected(false);
        checkBoxIsHandheld.setEnabled(true);
        buttonDeleteSystem.setEnabled(false);
        textFieldSystemName.requestFocus();
    }

    private void buttonNewToyActionPerformed() {
        openToyId = Globals.MISSINGNO;
        buttonSaveToy.setEnabled(true);
        textFieldToyName.setText("");
        textFieldToyName.setEnabled(true);
        textFieldToySet.setText("");
        textFieldToySet.setEnabled(true);
        textFieldPlatform.setText("");
        textFieldPlatform.setEnabled(true);
        buttonDeleteToy.setEnabled(false);
        textFieldToyName.requestFocus();
    }

    private void buttonNewGenreActionPerformed() {
        openGenre = Globals.NEW_GENRE;
        buttonSaveGenre.setEnabled(true);
        textFieldGenreName.setText("");
        textFieldGenreName.setEnabled(true);
        buttonDeleteGenre.setEnabled(false);
        textFieldGenreName.requestFocus();
    }

    private void listSearchResultsMouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            int selected = listSearchResults.getSelectedIndex();
            program.openGameFromSearchResults(selected);
        }
    }

    private void listSearchEnterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            int selected = listSearchResults.getSelectedIndex();
            program.openGameFromSearchResults(selected);
        }
    }

    private void buttonOpenActionPerformed() {
        int selected = listSearchResults.getSelectedIndex();
        if(selected != -1) {
            program.openGameFromSearchResults(selected);
        }
    }

    private void listSystemSearchResultsMouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            int selected = listSystemSearchResults.getSelectedIndex();
            program.openSystemFromSearchResults(selected);
        }
    }

    private void listSystemSearchResultsEnterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            int selected = listSystemSearchResults.getSelectedIndex();
            program.openSystemFromSearchResults(selected);
        }
    }

    private void buttonSystemOpenActionPerformed() {
        int selected = listSystemSearchResults.getSelectedIndex();
        if(selected != -1) {
            program.openSystemFromSearchResults(selected);
        }
    }

    private void listGenreSearchResultsMouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            int selected = listGenreSearchResults.getSelectedIndex();
            program.openGenreFromSearchResults(selected);
        }
    }

    private void listGenreSearchResultsEnterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            int selected = listGenreSearchResults.getSelectedIndex();
            program.openGenreFromSearchResults(selected);
        }
    }

    private void buttonGenreOpenActionPerformed() {
        int selected = listGenreSearchResults.getSelectedIndex();
        if(selected != -1) {
            program.openGenreFromSearchResults(selected);
        }
    }

    private void listToysSearchResultsMouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            int selected = listToysSearchResults.getSelectedIndex();
            program.openToyFromSearchResults(selected);
        }
    }

    private void listToysSearchResultsEnterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            int selected = listToysSearchResults.getSelectedIndex();
            program.openToyFromSearchResults(selected);
        }
    }

    private void buttonToyOpenActionPerformed() {
        int selected = listToysSearchResults.getSelectedIndex();
        if(selected != -1) {
            program.openToyFromSearchResults(selected);
        }
    }

    private void buttonMasterReportActionPerformed() {
        program.searchMasterListReport();
    }

    private void buttonFilterReportActionPerformed() {
        AdvanceSearch filterReport = new AdvanceSearch(this,
                Globals.ADVANCE_SEARCH_LOCATION_REPORT,
                systemComboBoxList,
                genreComboBoxList,
                aestheticComboBoxList,
                completionLevelComboBoxList,
                ratingComboBoxList,
                thoughtsTypeComboBoxList,
                program.getLastAdvancedSearchFilters());
    }

    private void buttonCountReportActionPerformed() {
        textAreaReportsDisplay.setText("");
        program.searchCountReport();
    }

    private void buttonToysReportActionPerformed() {
        program.searchToyListReport();
    }

    public void displayOpenedGame(Video_Game openGame) {
        openGameId = openGame.getId();

        textFieldGameTitle.setText(openGame.getTitle());
        textFieldGameTitle.setEnabled(true);
        comboBoxSystem.setSelectedIndex(systemComboBoxList.indexOf(openGame.getSystem()));
        comboBoxSystem.setEnabled(true);
        List<String> otherSystems = openGame.getOtherSystems();
        comboBoxOtherSystems0.setEnabled(true);
        buttonAddOtherSystem.setEnabled(true);
        setupOtherSystemsDisplay(otherSystems);
        List<String> repeatSystems = openGame.getRepeatSystems();
        comboBoxRepeatSystems0.setEnabled(true);
        buttonAddRepeatSystems.setEnabled(true);
        setupRepeatSystemsDisplay(repeatSystems);
        textFieldMinPlayers.setText(String.valueOf(openGame.getPlayerMin()));
        textFieldMinPlayers.setEnabled(true);
        textFieldMaxPlayers.setText(String.valueOf(openGame.getPlayerMax()));
        textFieldMaxPlayers.setEnabled(true);
        textFieldDeveloper.setText(openGame.getDeveloper());
        textFieldDeveloper.setEnabled(true);
        textFieldPublisher.setText(openGame.getPublisher());
        textFieldPublisher.setEnabled(true);
        textFieldOriginalReleaseYear.setText(String.valueOf(openGame.getOriginalReleaseYear()));
        textFieldOriginalReleaseYear.setEnabled(true);
        List<Integer> reReleaseYears = openGame.getReReleaseYears();
        textFieldReReleaseYear0.setEnabled(true);
        buttonAddReReleaseYear.setEnabled(true);
        setupReReleaseYearDisplay(reReleaseYears);
        comboBoxGenre.setSelectedIndex(genreComboBoxList.indexOf(openGame.getGenre()));
        textFieldSeries.setText(openGame.getSeries());
        textFieldSeries.setEnabled(true);
        comboBoxGenre.setEnabled(true);
        buttonDeleteGame.setEnabled(true);

        comboBoxCoreAesthetic.setSelectedIndex(aestheticComboBoxList.indexOf(AestheticMap.get(openGame.getCoreAesthetic())));
        comboBoxCoreAesthetic.setEnabled(true);
        comboBoxOtherAesthetic.setSelectedIndex(aestheticComboBoxList.indexOf(AestheticMap.get(openGame.getOtherAesthetic())));
        comboBoxOtherAesthetic.setEnabled(true);
        textFieldRecommendedFor.setText(openGame.getRecommendedFor());
        textFieldRecommendedFor.setEnabled(true);
        comboBoxCompletionLevel.setSelectedIndex(completionLevelComboBoxList.indexOf(openGame.getCompletionLevel()));
        comboBoxCompletionLevel.setEnabled(true);
        comboBoxRating.setSelectedIndex(ratingComboBoxList.indexOf(openGame.getRating()));
        comboBoxRating.setEnabled(true);
        textAreaComments.setText(openGame.getComments());
        textAreaComments.setEnabled(true);
        buttonOpenLink.setEnabled(true);
        textFieldThoughtsLink.setText(openGame.getThoughtsLink());
        textFieldThoughtsLink.setEnabled(true);
        comboBoxThoughtsType.setSelectedIndex(thoughtsTypeComboBoxList.indexOf(openGame.getThoughtsType()));
        comboBoxThoughtsType.setEnabled(true);
        checkBoxRevisit.setSelected(openGame.isShouldRevisit());
        checkBoxRevisit.setEnabled(true);
        checkBoxCouch.setSelected(openGame.isLocalCouch());
        checkBoxCouch.setEnabled(true);
        checkBoxHallOfFame.setSelected(openGame.isHallOfFame());
        checkBoxHallOfFame.setEnabled(true);
        buttonSaveEntry.setEnabled(true);
    }

    public void displayOpenedSystem(System openSystem) {
        openSystemId = openSystem.getId();
        buttonSaveSystem.setEnabled(true);
        textFieldSystemName.setText(openSystem.getName());
        textFieldSystemName.setEnabled(true);
        textFieldGeneration.setText(String.valueOf(openSystem.getGeneration()));
        textFieldGeneration.setEnabled(true);
        checkBoxIsHandheld.setSelected(openSystem.isHandheld());
        checkBoxIsHandheld.setEnabled(true);
        buttonDeleteSystem.setEnabled(true);
        if (openSystem.getName().equals("none")) {
            buttonDeleteSystem.setEnabled(false);
            buttonSaveSystem.setEnabled(false);
        }
    }

    public void displayOpenedGenre(String openGenre) {
        this.openGenre = openGenre;
        buttonSaveGenre.setEnabled(true);
        textFieldGenreName.setText(openGenre);
        textFieldGenreName.setEnabled(true);
        buttonDeleteGenre.setEnabled(true);
        if (openGenre.equals("none")) {
            buttonSaveGenre.setEnabled(false);
            buttonDeleteGenre.setEnabled(false);
        }
    }

    public void displayOpenedToy(Toy openToy) {
        openToyId = openToy.getId();
        buttonSaveToy.setEnabled(true);
        textFieldToyName.setText(openToy.getName());
        textFieldToyName.setEnabled(true);
        textFieldToySet.setText(openToy.getToySet());
        textFieldToySet.setEnabled(true);
        textFieldPlatform.setText(openToy.getPlatform());
        textFieldPlatform.setEnabled(true);
        buttonDeleteToy.setEnabled(true);
    }

    public void displaySearchResults(List<Video_Game> searchResults) {
        DefaultListModel<String> model = new DefaultListModel<>();
        for (Video_Game game : searchResults) {
            model.addElement(game.getTitle());
        }
        listSearchResults.setModel(model);
        labelGamesCount.setText(Globals.SEARCH_RESULTS + searchResults.size());
    }

    public void displaySystemSearchResults(List<String> searchResults) {
        DefaultListModel<String> model = new DefaultListModel<>();
        for (String system : searchResults) {
            model.addElement(system);
        }
        listSystemSearchResults.setModel(model);
        labelSystemCount.setText(Globals.SEARCH_RESULTS + searchResults.size());
    }

    public void displayGenreSearchResults(List<String> searchResults) {
        DefaultListModel<String> model = new DefaultListModel<>();
        for (String genre : searchResults) {
            model.addElement(genre);
        }
        listGenreSearchResults.setModel(model);
        labelGenreCount.setText(Globals.SEARCH_RESULTS + searchResults.size());
    }

    public void displayToySearchResults(List<String> searchResults) {
        DefaultListModel<String> model = new DefaultListModel<>();
        for (String toy : searchResults) {
            model.addElement(toy);
        }
        listToysSearchResults.setModel(model);
        labelToyCount.setText(Globals.SEARCH_RESULTS + searchResults.size());
    }

    public void setupSystemComboBoxes(List<String> input) {
        systemComboBoxList = input;
        comboBoxSystem.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxSystemSearch.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxOtherSystems0.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxOtherSystems1.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxOtherSystems2.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxOtherSystems3.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxRepeatSystems0.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxRepeatSystems1.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxRepeatSystems2.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxRepeatSystems3.setModel(new DefaultComboBoxModel(input.toArray()));
    }

    public void setupGenreComboBoxes(List<String> input) {
        genreComboBoxList = input;
        comboBoxGenre.setModel(new DefaultComboBoxModel(input.toArray()));
        comboBoxGenreSearch.setModel(new DefaultComboBoxModel(input.toArray()));
    }

    public void updateOpenGameId(int openGameId) {
        this.openGameId = openGameId;
    }

    public void updateOpenSystemId(int openSystemId) {
        this.openSystemId = openSystemId;
    }

    public void updateOpenGenre(String openGenre) {
        this.openGenre = openGenre;
    }

    public void updateOpenToyId(int openToyId) {
        this.openToyId = openToyId;
    }

    private void setupOtherSystemsDisplay(List<String> otherSystems) {
        //clear out the panel of all text fields currently open
        for (int i = otherSystemsFields; i > -1; i--) {
            switch (i) {
                case 0:
                    comboBoxOtherSystems0.setSelectedIndex(systemComboBoxList.indexOf("none"));
                    panelOtherSystems.remove(comboBoxOtherSystems0);
                    break;
                case 1:
                    comboBoxOtherSystems1.setSelectedIndex(systemComboBoxList.indexOf("none"));
                    panelOtherSystems.remove(comboBoxOtherSystems1);
                    otherSystemsFields--;
                    break;
                case 2:
                    comboBoxOtherSystems2.setSelectedIndex(systemComboBoxList.indexOf("none"));
                    panelOtherSystems.remove(comboBoxOtherSystems2);
                    otherSystemsFields--;
                    break;
                case 3:
                    comboBoxOtherSystems3.setSelectedIndex(systemComboBoxList.indexOf("none"));
                    panelOtherSystems.remove(comboBoxOtherSystems3);
                    otherSystemsFields--;
                    buttonAddOtherSystem.setEnabled(true);
                    break;
                default:
                    addToDebugPanel("ERROR when setting up the other systems fields");
            }
        }
        //add back in the panels that are required
        for (int i = otherSystemsFields; i < otherSystems.size(); i++) {
            switch (i) {
                case 0:
                    panelOtherSystems.add(comboBoxOtherSystems0);
                    comboBoxOtherSystems0.setSelectedIndex(systemComboBoxList.indexOf(otherSystems.get(i)));
                    otherSystemsFields++;
                    break;
                case 1:
                    panelOtherSystems.add(comboBoxOtherSystems1);
                    comboBoxOtherSystems1.setSelectedIndex(systemComboBoxList.indexOf(otherSystems.get(i)));
                    otherSystemsFields++;
                    break;
                case 2:
                    panelOtherSystems.add(comboBoxOtherSystems2);
                    comboBoxOtherSystems2.setSelectedIndex(systemComboBoxList.indexOf(otherSystems.get(i)));
                    otherSystemsFields++;
                    break;
                case 3:
                    panelOtherSystems.add(comboBoxOtherSystems3);
                    comboBoxOtherSystems3.setSelectedIndex(systemComboBoxList.indexOf(otherSystems.get(i)));
                    buttonAddOtherSystem.setEnabled(false);
                    break;
                default:
                    addToDebugPanel("ERROR when setting up the other systems fields");
            }
        }
        //display the changed panel
        panelOtherSystems.revalidate();
        panelOtherSystems.repaint();
    }

    private void setupRepeatSystemsDisplay(List<String> repeatSystems) {
        //clear out the existing text fields
        for (int i = repeatSystemsFields; i > -1; i--) {
            switch (i) {
                case 0:
                    comboBoxRepeatSystems0.setSelectedIndex(systemComboBoxList.indexOf(Globals.NONE));
                    panelRepeatSystems.remove(comboBoxRepeatSystems0);
                    break;
                case 1:
                    comboBoxRepeatSystems1.setSelectedIndex(systemComboBoxList.indexOf(Globals.NONE));
                    panelRepeatSystems.remove(comboBoxRepeatSystems1);
                    repeatSystemsFields--;
                    break;
                case 2:
                    comboBoxRepeatSystems2.setSelectedIndex(systemComboBoxList.indexOf(Globals.NONE));
                    panelRepeatSystems.remove(comboBoxRepeatSystems2);
                    repeatSystemsFields--;
                    break;
                case 3:
                    comboBoxRepeatSystems3.setSelectedIndex(systemComboBoxList.indexOf(Globals.NONE));
                    panelRepeatSystems.remove(comboBoxRepeatSystems3);
                    repeatSystemsFields--;
                    buttonAddRepeatSystems.setEnabled(true);
                    break;
                default:
                    addToDebugPanel("ERROR when setting up the repeat system fields");
            }
        }
        //add back in the panels that are needed
        for (int i = repeatSystemsFields; i < repeatSystems.size(); i++) {
            switch (i) {
                case 0:
                    panelRepeatSystems.add(comboBoxRepeatSystems0);
                    comboBoxRepeatSystems0.setSelectedIndex(systemComboBoxList.indexOf(repeatSystems.get(i)));
                    repeatSystemsFields++;
                    break;
                case 1:
                    panelRepeatSystems.add(comboBoxRepeatSystems1);
                    comboBoxRepeatSystems1.setSelectedIndex(systemComboBoxList.indexOf(repeatSystems.get(i)));
                    repeatSystemsFields++;
                    break;
                case 2:
                    panelRepeatSystems.add(comboBoxRepeatSystems2);
                    comboBoxRepeatSystems2.setSelectedIndex(systemComboBoxList.indexOf(repeatSystems.get(i)));
                    repeatSystemsFields++;
                    break;
                case 3:
                    panelRepeatSystems.add(comboBoxRepeatSystems3);
                    comboBoxRepeatSystems3.setSelectedIndex(systemComboBoxList.indexOf(repeatSystems.get(i)));
                    buttonAddRepeatSystems.setEnabled(false);
                    break;
                default:
                    addToDebugPanel("ERROR when setting up the repeat systems fields");
            }
        }
        //display the changed panel
        panelRepeatSystems.revalidate();
        panelRepeatSystems.repaint();
    }

    private void setupReReleaseYearDisplay(List<Integer> reReleaseYears) {
        //clear out the panel of all existing text fields
        for (int i = reReleaseYearsFields; i > -1; i--) {
            switch (i) {
                case 0:
                    textFieldReReleaseYear0.setText("");
                    panelReReleaseYears.remove(textFieldReReleaseYear0);
                    break;
                case 1:
                    textFieldReReleaseYear1.setText("");
                    panelReReleaseYears.remove(textFieldReReleaseYear1);
                    reReleaseYearsFields--;
                    break;
                case 2:
                    textFieldReReleaseYear2.setText("");
                    panelReReleaseYears.remove(textFieldReReleaseYear2);
                    reReleaseYearsFields--;
                    break;
                case 3:
                    textFieldReReleaseYear3.setText("");
                    panelReReleaseYears.remove(textFieldReReleaseYear3);
                    reReleaseYearsFields--;
                    buttonAddReReleaseYear.setEnabled(true);
                    break;
                default:
                    addToDebugPanel("ERROR when setting up the reRelease fields");
            }
        }
        //add back in the panels that are needed
        for (int i = reReleaseYearsFields; i < reReleaseYears.size(); i++) {
            switch (i) {
                case 0:
                    panelReReleaseYears.add(textFieldReReleaseYear0);
                    textFieldReReleaseYear0.setText(String.valueOf(reReleaseYears.get(i)));
                    reReleaseYearsFields++;
                    break;
                case 1:
                    panelReReleaseYears.add(textFieldReReleaseYear1);
                    textFieldReReleaseYear1.setText(String.valueOf(reReleaseYears.get(i)));
                    reReleaseYearsFields++;
                    break;
                case 2:
                    panelReReleaseYears.add(textFieldReReleaseYear2);
                    textFieldReReleaseYear2.setText(String.valueOf(reReleaseYears.get(i)));
                    reReleaseYearsFields++;
                    break;
                case 3:
                    panelReReleaseYears.add(textFieldReReleaseYear3);
                    textFieldReReleaseYear3.setText(String.valueOf(reReleaseYears.get(i)));
                    buttonAddReReleaseYear.setEnabled(false);
                    break;
                default:
                    addToDebugPanel("ERROR when setting up the reRelease fields");
            }
        }
        //display the new panel
        panelReReleaseYears.revalidate();
        panelReReleaseYears.repaint();
    }

    public void displayVideoGameReportResults(List<Video_Game> reportResults) {
        textAreaReportsDisplay.setText(Globals.VIDEO_GAME_REPORT_HEADERS + "\n\n");
        for (Video_Game game : reportResults) {
            textAreaReportsDisplay.append(game.toReportString() + "\n\n");
        }
        textAreaReportsDisplay.append(reportResults.size() + " Results Found");
    }

    public void buttonFilterReturn(int result, AdvancedSearchFilters filters) {
        switch (result) {
            case Globals.ADVANCE_SEARCH_LOCATION_CANCEL:
                addToDebugPanel("Filter Search/Report Canceled");
                break;
            case Globals.ADVANCE_SEARCH_LOCATION_ERROR:
                addToDebugPanel("Error with the advance search location, this code should not be reached.");
                break;
            case Globals.ADVANCE_SEARCH_LOCATION_REPORT:
                program.searchFiltersReport(filters);
                break;
            case Globals.ADVANCE_SEARCH_LOCATION_SEARCH:
                program.advancedSearchResultsVideoGames(filters);
                break;
        }
    }

    public void displayCountReportNextLine(String nextLine) {
        textAreaReportsDisplay.append(nextLine + "\n");
    }

    public void displayToyListReportResults(List<Toy> reportResults) {
        textAreaReportsDisplay.setText(Globals.TOY_REPORT_HEADERS + "\n\n");
        for (Toy toy : reportResults) {
            textAreaReportsDisplay.append(toy.toReportString() + "\n\n");
        }
        textAreaReportsDisplay.append(reportResults.size() + " Results Found");
    }

    public void displayPlainDialog(String message, String title) {
        JOptionPane.showMessageDialog(this, message, title, JOptionPane.PLAIN_MESSAGE);
    }

    public void displayErrorDialog(String message, String title) {
        JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public void newGUISetup() {
        //toolbar
        buttonNewEntry.setEnabled(false);
        buttonSaveEntry.setEnabled(false);
        //search pane
        buttonSearch.setEnabled(false);
        buttonAdvanceSearch.setEnabled(false);
        //general tab
        textFieldGameTitle.setEnabled(false);
        comboBoxSystem.setEnabled(false);
        comboBoxOtherSystems0.setEnabled(false);
        panelOtherSystems.remove(comboBoxOtherSystems3);
        panelOtherSystems.remove(comboBoxOtherSystems2);
        panelOtherSystems.remove(comboBoxOtherSystems1);
        buttonAddOtherSystem.setEnabled(false);
        comboBoxRepeatSystems0.setEnabled(false);
        panelRepeatSystems.remove(comboBoxRepeatSystems3);
        panelRepeatSystems.remove(comboBoxRepeatSystems2);
        panelRepeatSystems.remove(comboBoxRepeatSystems1);
        buttonAddRepeatSystems.setEnabled(false);
        textFieldMinPlayers.setEnabled(false);
        textFieldMaxPlayers.setEnabled(false);
        textFieldDeveloper.setEnabled(false);
        textFieldPublisher.setEnabled(false);
        textFieldOriginalReleaseYear.setEnabled(false);
        textFieldReReleaseYear0.setEnabled(false);
        panelReReleaseYears.remove(textFieldReReleaseYear3);
        panelReReleaseYears.remove(textFieldReReleaseYear2);
        panelReReleaseYears.remove(textFieldReReleaseYear1);
        buttonAddReReleaseYear.setEnabled(false);
        comboBoxGenre.setEnabled(false);
        buttonDeleteGame.setEnabled(false);
        textFieldSeries.setEnabled(false);
        //details tab
        comboBoxCoreAesthetic.setEnabled(false);
        comboBoxOtherAesthetic.setEnabled(false);
        textFieldRecommendedFor.setEnabled(false);
        comboBoxCompletionLevel.setEnabled(false);
        comboBoxRating.setEnabled(false);
        textAreaComments.setEnabled(false);
        buttonOpenLink.setEnabled(false);
        textFieldThoughtsLink.setEnabled(false);
        comboBoxThoughtsType.setEnabled(false);
        checkBoxRevisit.setEnabled(false);
        checkBoxCouch.setEnabled(false);
        checkBoxHallOfFame.setEnabled(false);
        //systems tab
        buttonSystemSearch.setEnabled(false);
        buttonNewSystem.setEnabled(false);
        buttonSaveSystem.setEnabled(false);
        textFieldSystemName.setEnabled(false);
        textFieldGeneration.setEnabled(false);
        checkBoxIsHandheld.setEnabled(false);
        buttonDeleteSystem.setEnabled(false);
        //genres tab
        buttonGenreSearch.setEnabled(false);
        buttonNewGenre.setEnabled(false);
        buttonSaveGenre.setEnabled(false);
        textFieldGenreName.setEnabled(false);
        buttonDeleteGenre.setEnabled(false);
        //toys tab
        buttonNewToy.setEnabled(false);
        buttonSaveToy.setEnabled(false);
        buttonDeleteToy.setEnabled(false);
        buttonToysSearch.setEnabled(false);
        textFieldToyName.setEnabled(false);
        textFieldToySet.setEnabled(false);
        textFieldPlatform.setEnabled(false);
        //reports tab
        buttonMasterReport.setEnabled(false);
        buttonFilterReport.setEnabled(false);
        buttonCountReport.setEnabled(false);
        buttonToysReport.setEnabled(false);
    }

    public void connectionGUISetup() {
        buttonSearch.setEnabled(true);
        buttonAdvanceSearch.setEnabled(true);
        buttonNewEntry.setEnabled(true);
        buttonSystemSearch.setEnabled(true);
        buttonNewSystem.setEnabled(true);
        buttonGenreSearch.setEnabled(true);
        buttonNewGenre.setEnabled(true);
        buttonToysSearch.setEnabled(true);
        buttonNewToy.setEnabled(true);
        buttonMasterReport.setEnabled(true);
        buttonFilterReport.setEnabled(true);
        buttonCountReport.setEnabled(true);
        buttonToysReport.setEnabled(true);
    }

    public void setupAestheticComboBoxes() {
        aestheticComboBoxList.clear();
        aestheticComboBoxList.addAll(AestheticMap.values());
        java.util.Collections.sort(aestheticComboBoxList);
        comboBoxCoreAesthetic.setModel(new DefaultComboBoxModel(aestheticComboBoxList.toArray()));
        comboBoxOtherAesthetic.setModel(new DefaultComboBoxModel(aestheticComboBoxList.toArray()));
    }

    public void setupRatingComboBox() {
        ratingComboBoxList.clear();
        ratingComboBoxList.add(Globals.Rating.BACKLOGGED);
        ratingComboBoxList.add(Globals.Rating.DISCONTENT);
        ratingComboBoxList.add(Globals.Rating.CONTENT);
        ratingComboBoxList.add(Globals.Rating.SATISFIED);
        comboBoxRating.setModel(new DefaultComboBoxModel(ratingComboBoxList.toArray()));
    }

    public void setupCompletionLevelComboBox() {
        completionLevelComboBoxList.clear();
        completionLevelComboBoxList.add(Globals.CompletionLevel.WANT);
        completionLevelComboBoxList.add(Globals.CompletionLevel.BOUGHT);
        completionLevelComboBoxList.add(Globals.CompletionLevel.PLAY);
        completionLevelComboBoxList.add(Globals.CompletionLevel.PLAYING);
        completionLevelComboBoxList.add(Globals.CompletionLevel.FINISH);
        completionLevelComboBoxList.add(Globals.CompletionLevel.COMPLETE);
        comboBoxCompletionLevel.setModel(new DefaultComboBoxModel(completionLevelComboBoxList.toArray()));
    }

    public void setupThoughtsTypeComboBox() {
        thoughtsTypeComboBoxList.add(Globals.ThoughtsType.PLACEHOLDER);
        thoughtsTypeComboBoxList.add(Globals.ThoughtsType.NOTES);
        thoughtsTypeComboBoxList.add(Globals.ThoughtsType.MEMORIES);
        thoughtsTypeComboBoxList.add(Globals.ThoughtsType.FULL_REVIEW);
        comboBoxThoughtsType.setModel(new DefaultComboBoxModel(thoughtsTypeComboBoxList.toArray()));
    }

    public void addToDebugPanel(String newInfo) {
        textAreaDebug.append(newInfo + ('\n'));
        //will set the scroll pane to the bottom of the scrollbar
        JScrollBar vertical = scrollPaneDebug.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
    }

    public void clearSystemGenreBasicSearch() {
        comboBoxGenreSearch.setSelectedItem(Globals.NONE);
        comboBoxSystemSearch.setSelectedItem(Globals.NONE);
    }

    private void buttonOpenLinkActionPerformed() {
        URL link;
        try {
            link = new URL(textFieldThoughtsLink.getText());
        } catch (MalformedURLException e) {
            addToDebugPanel("Malformed URL Exception, no additional information recorded.");
            return;
        }
        try {
            Desktop.getDesktop().browse(link.toURI());
        } catch (IOException e) {
            addToDebugPanel("IO Exception, no additional information recorded.");
        } catch (URISyntaxException e) {
            addToDebugPanel("URI Syntax Exception, no additional information recorded.");
        }
    }

    private void buttonChangeTabActionPerformed() {
        if (tabbedPaneGames.getSelectedIndex() == 0) {
            tabbedPaneGames.setSelectedIndex(1);
        } else {
            tabbedPaneGames.setSelectedIndex(0);
        }
    }

    private void textFieldTitleSearchKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonSearchActionPerformed();
        }
    }

    private void comboBoxSystemSearchKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonSearchActionPerformed();
        }
    }

    private void comboBoxGenreSearchKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonSearchActionPerformed();
        }
    }

    private void textFieldSystemSearchFilterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonSystemSearchActionPerformed();
        }
    }

    private void textFieldGenreFilterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonGenreSearchActionPerformed();
        }
    }

    private void textFieldToysFilterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonToysSearchActionPerformed();
        }
    }

    private void textFieldToysSetFilterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonToysSearchActionPerformed();
        }
    }

    private void textFieldPlatformFilterKeyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            buttonToysSearchActionPerformed();
        }
    }

    //---JFormDesignerSetup---

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        menuBar = new JMenuBar();
        menuDatabase = new JMenu();
        menuItemNew = new JMenuItem();
        menuItemLoad = new JMenuItem();
        menuAbout = new JMenu();
        menuItemVersion = new JMenuItem();
        menuView = new JMenu();
        menuItemViewDebugPanel = new JCheckBoxMenuItem();
        splitPaneLarge = new JSplitPane();
        tabbedPane = new JTabbedPane();
        splitPaneGames = new JSplitPane();
        panelSearchGames = new JPanel();
        labelTitleSearch = new JLabel();
        textFieldTitleSearch = new JTextField();
        labelSystemSearch = new JLabel();
        comboBoxSystemSearch = new JComboBox();
        labelGenreSearch = new JLabel();
        comboBoxGenreSearch = new JComboBox();
        checkBoxRepeatSystemNormalSearch = new JCheckBox();
        buttonSearch = new JButton();
        buttonAdvanceSearch = new JButton();
        scrollPaneSearch = new JScrollPane();
        listSearchResults = new JList();
        buttonOpen = new JButton();
        labelGamesCount = new JLabel();
        panelWorkGames = new JPanel();
        panel1 = new JPanel();
        buttonNewEntry = new JButton();
        buttonSaveEntry = new JButton();
        buttonDeleteGame = new JButton();
        buttonChangeTab = new JButton();
        tabbedPaneGames = new JTabbedPane();
        panelGameGeneral = new JPanel();
        labelGameTitle = new JLabel();
        textFieldGameTitle = new JTextField();
        labelSystem = new JLabel();
        comboBoxSystem = new JComboBox();
        labelOtherSystems = new JLabel();
        panelOtherSystems = new JPanel();
        comboBoxOtherSystems0 = new JComboBox();
        comboBoxOtherSystems1 = new JComboBox();
        comboBoxOtherSystems2 = new JComboBox();
        comboBoxOtherSystems3 = new JComboBox();
        buttonAddOtherSystem = new JButton();
        labelRepeatSystems = new JLabel();
        panelRepeatSystems = new JPanel();
        comboBoxRepeatSystems0 = new JComboBox();
        comboBoxRepeatSystems1 = new JComboBox();
        comboBoxRepeatSystems2 = new JComboBox();
        comboBoxRepeatSystems3 = new JComboBox();
        buttonAddRepeatSystems = new JButton();
        labelPlayerMin = new JLabel();
        textFieldMinPlayers = new JTextField();
        labelTo = new JLabel();
        textFieldMaxPlayers = new JTextField();
        labelDeveloper = new JLabel();
        textFieldDeveloper = new JTextField();
        labelPublisher = new JLabel();
        textFieldPublisher = new JTextField();
        labelReleaseYear = new JLabel();
        textFieldOriginalReleaseYear = new JTextField();
        labelReReleaseYear = new JLabel();
        panelReReleaseYears = new JPanel();
        textFieldReReleaseYear0 = new JTextField();
        textFieldReReleaseYear1 = new JTextField();
        textFieldReReleaseYear2 = new JTextField();
        textFieldReReleaseYear3 = new JTextField();
        buttonAddReReleaseYear = new JButton();
        labelGenre = new JLabel();
        comboBoxGenre = new JComboBox();
        labelSeries = new JLabel();
        textFieldSeries = new JTextField();
        panelDetailsPanel = new JPanel();
        labelCoreAesthetic = new JLabel();
        comboBoxCoreAesthetic = new JComboBox();
        labelOtherAesthetic = new JLabel();
        comboBoxOtherAesthetic = new JComboBox();
        labelRecommendedFor = new JLabel();
        textFieldRecommendedFor = new JTextField();
        labelCompletionLevel = new JLabel();
        comboBoxCompletionLevel = new JComboBox();
        labelRating = new JLabel();
        comboBoxRating = new JComboBox();
        labelComments = new JLabel();
        scrollPaneComments = new JScrollPane();
        textAreaComments = new JTextArea();
        labelThoughtsLink = new JLabel();
        buttonOpenLink = new JButton();
        textFieldThoughtsLink = new JTextField();
        labelThoughtsType = new JLabel();
        comboBoxThoughtsType = new JComboBox();
        checkBoxRevisit = new JCheckBox();
        checkBoxCouch = new JCheckBox();
        checkBoxHallOfFame = new JCheckBox();
        splitPaneSystems = new JSplitPane();
        panelSearchSystems = new JPanel();
        labelSystemSubSearch = new JLabel();
        textFieldSystemSearchFilter = new JTextField();
        buttonSystemSearch = new JButton();
        scrollPaneSystemSearchResults = new JScrollPane();
        listSystemSearchResults = new JList();
        buttonSystemOpen = new JButton();
        labelSystemCount = new JLabel();
        panelWorkSystem = new JPanel();
        buttonNewSystem = new JButton();
        buttonSaveSystem = new JButton();
        buttonDeleteSystem = new JButton();
        labelSubSystem = new JLabel();
        textFieldSystemName = new JTextField();
        labelGeneration = new JLabel();
        textFieldGeneration = new JTextField();
        checkBoxIsHandheld = new JCheckBox();
        splitPaneGenres = new JSplitPane();
        panelSearchGenres = new JPanel();
        labelGenreSubSearch = new JLabel();
        textFieldGenreFilter = new JTextField();
        buttonGenreSearch = new JButton();
        scrollPaneGenreSearchResults = new JScrollPane();
        listGenreSearchResults = new JList();
        buttonGenreOpen = new JButton();
        labelGenreCount = new JLabel();
        panelWorkGenre = new JPanel();
        buttonNewGenre = new JButton();
        buttonSaveGenre = new JButton();
        buttonDeleteGenre = new JButton();
        labelSubGenre = new JLabel();
        textFieldGenreName = new JTextField();
        splitPaneToys = new JSplitPane();
        panelSearchToys = new JPanel();
        labelToysName = new JLabel();
        textFieldToysFilter = new JTextField();
        labelToysSetFilter = new JLabel();
        textFieldToysSetFilter = new JTextField();
        labelSearchPlatform = new JLabel();
        textFieldPlatformFilter = new JTextField();
        buttonToysSearch = new JButton();
        scrollPaneToysSearchResults = new JScrollPane();
        listToysSearchResults = new JList();
        buttonToyOpen = new JButton();
        labelToyCount = new JLabel();
        panelWorkToys = new JPanel();
        buttonNewToy = new JButton();
        buttonSaveToy = new JButton();
        buttonDeleteToy = new JButton();
        labelToyName = new JLabel();
        textFieldToyName = new JTextField();
        labelToySet = new JLabel();
        textFieldToySet = new JTextField();
        labelPlatform = new JLabel();
        textFieldPlatform = new JTextField();
        panelWorkQueries = new JPanel();
        buttonMasterReport = new JButton();
        buttonFilterReport = new JButton();
        buttonCountReport = new JButton();
        buttonToysReport = new JButton();
        scrollPaneReportsDisplay = new JScrollPane();
        textAreaReportsDisplay = new JTextArea();
        panelDebug = new JPanel();
        scrollPaneDebug = new JScrollPane();
        textAreaDebug = new JTextArea();

        //======== this ========
        setTitle("Digital Game Database");
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== menuBar ========
        {

            //======== menuDatabase ========
            {
                menuDatabase.setText("Database");

                //---- menuItemNew ----
                menuItemNew.setText("New Database");
                menuItemNew.addActionListener(e -> menuItemNewActionPerformed());
                menuDatabase.add(menuItemNew);

                //---- menuItemLoad ----
                menuItemLoad.setText("Load Database");
                menuItemLoad.addActionListener(e -> menuItemLoadActionPerformed());
                menuDatabase.add(menuItemLoad);
            }
            menuBar.add(menuDatabase);

            //======== menuAbout ========
            {
                menuAbout.setText("About");

                //---- menuItemVersion ----
                menuItemVersion.setText("Version");
                menuItemVersion.addActionListener(e -> menuItemVersionActionPerformed());
                menuAbout.add(menuItemVersion);
            }
            menuBar.add(menuAbout);

            //======== menuView ========
            {
                menuView.setText("View");

                //---- menuItemViewDebugPanel ----
                menuItemViewDebugPanel.setText("Debug Pane");
                menuItemViewDebugPanel.addActionListener(e -> menuItemViewDebugPanelActionPerformed());
                menuView.add(menuItemViewDebugPanel);
            }
            menuBar.add(menuView);
        }
        setJMenuBar(menuBar);

        //======== splitPaneLarge ========
        {
            splitPaneLarge.setOrientation(JSplitPane.VERTICAL_SPLIT);
            splitPaneLarge.setResizeWeight(0.75);

            //======== tabbedPane ========
            {

                //======== splitPaneGames ========
                {

                    //======== panelSearchGames ========
                    {
                        panelSearchGames.setLayout(new GridBagLayout());
                        ((GridBagLayout)panelSearchGames.getLayout()).columnWidths = new int[] {85, 120, 0};
                        ((GridBagLayout)panelSearchGames.getLayout()).rowHeights = new int[] {10, 25, 25, 25, 0, 25, 0, 0, 0};
                        ((GridBagLayout)panelSearchGames.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
                        ((GridBagLayout)panelSearchGames.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};

                        //---- labelTitleSearch ----
                        labelTitleSearch.setText("Game Title: ");
                        panelSearchGames.add(labelTitleSearch, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- textFieldTitleSearch ----
                        textFieldTitleSearch.setColumns(10);
                        textFieldTitleSearch.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                textFieldTitleSearchKeyPressed(e);
                            }
                        });
                        panelSearchGames.add(textFieldTitleSearch, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- labelSystemSearch ----
                        labelSystemSearch.setText("System: ");
                        panelSearchGames.add(labelSystemSearch, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- comboBoxSystemSearch ----
                        comboBoxSystemSearch.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                comboBoxSystemSearchKeyPressed(e);
                            }
                        });
                        panelSearchGames.add(comboBoxSystemSearch, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- labelGenreSearch ----
                        labelGenreSearch.setText("Genre: ");
                        panelSearchGames.add(labelGenreSearch, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- comboBoxGenreSearch ----
                        comboBoxGenreSearch.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                comboBoxGenreSearchKeyPressed(e);
                            }
                        });
                        panelSearchGames.add(comboBoxGenreSearch, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- checkBoxRepeatSystemNormalSearch ----
                        checkBoxRepeatSystemNormalSearch.setText("Search System and Repeat Systems");
                        panelSearchGames.add(checkBoxRepeatSystemNormalSearch, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- buttonSearch ----
                        buttonSearch.setText("Search");
                        buttonSearch.setIcon(null);
                        buttonSearch.setMnemonic('A');
                        buttonSearch.addActionListener(e -> buttonSearchActionPerformed());
                        panelSearchGames.add(buttonSearch, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- buttonAdvanceSearch ----
                        buttonAdvanceSearch.setText("Advance Search");
                        buttonAdvanceSearch.setMnemonic('V');
                        buttonAdvanceSearch.addActionListener(e -> buttonAdvanceSearchActionPerformed());
                        panelSearchGames.add(buttonAdvanceSearch, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //======== scrollPaneSearch ========
                        {

                            //---- listSearchResults ----
                            listSearchResults.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                            listSearchResults.setToolTipText("<html>\nSearch results are sorted by: system generation (handheld systems first. <br> then release year, then alphabetically by title.\n<br><br>\nAdvanced search results are sorted by: release year, then alphabetically by title.\n</html>\n");
                            listSearchResults.addMouseListener(new MouseAdapter() {
                                @Override
                                public void mouseClicked(MouseEvent e) {
                                    listSearchResultsMouseClicked(e);
                                }
                            });
                            listSearchResults.addKeyListener(new KeyAdapter() {
                                @Override
                                public void keyPressed(KeyEvent e) {
                                    listSearchEnterKeyPressed(e);
                                }
                            });
                            scrollPaneSearch.setViewportView(listSearchResults);
                        }
                        panelSearchGames.add(scrollPaneSearch, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 0), 0, 0));

                        //---- buttonOpen ----
                        buttonOpen.setText("Open");
                        buttonOpen.setMnemonic('O');
                        buttonOpen.addActionListener(e -> buttonOpenActionPerformed());
                        panelSearchGames.add(buttonOpen, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 0, 5), 0, 0));

                        //---- labelGamesCount ----
                        labelGamesCount.setText("Search Results: 1000");
                        panelSearchGames.add(labelGamesCount, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 0, 0), 0, 0));
                    }
                    splitPaneGames.setLeftComponent(panelSearchGames);

                    //======== panelWorkGames ========
                    {
                        panelWorkGames.setLayout(new BorderLayout());

                        //======== panel1 ========
                        {
                            panel1.setLayout(new GridBagLayout());
                            ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
                            ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {10, 0, 0};
                            ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};
                            ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

                            //---- buttonNewEntry ----
                            buttonNewEntry.setText("New Game");
                            buttonNewEntry.setMnemonic('N');
                            buttonNewEntry.setDisplayedMnemonicIndex(0);
                            buttonNewEntry.addActionListener(e -> buttonNewEntryActionPerformed());
                            panel1.add(buttonNewEntry, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                new Insets(0, 0, 0, 5), 0, 0));

                            //---- buttonSaveEntry ----
                            buttonSaveEntry.setText("Save Game");
                            buttonSaveEntry.setMnemonic('S');
                            buttonSaveEntry.addActionListener(e -> buttonSaveEntryActionPerformed());
                            panel1.add(buttonSaveEntry, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                new Insets(0, 0, 0, 5), 0, 0));

                            //---- buttonDeleteGame ----
                            buttonDeleteGame.setText("Delete Game");
                            buttonDeleteGame.setDisplayedMnemonicIndex(0);
                            buttonDeleteGame.setMnemonic('D');
                            buttonDeleteGame.addActionListener(e -> buttonDeleteGameActionPerformed());
                            panel1.add(buttonDeleteGame, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                new Insets(0, 0, 0, 5), 0, 0));

                            //---- buttonChangeTab ----
                            buttonChangeTab.setText("Alt + T to change tabs");
                            buttonChangeTab.setMnemonic('T');
                            buttonChangeTab.setBorderPainted(false);
                            buttonChangeTab.setContentAreaFilled(false);
                            buttonChangeTab.setFocusable(false);
                            buttonChangeTab.setFocusPainted(false);
                            buttonChangeTab.addActionListener(e -> buttonChangeTabActionPerformed());
                            panel1.add(buttonChangeTab, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
                                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                new Insets(0, 0, 0, 0), 0, 0));
                        }
                        panelWorkGames.add(panel1, BorderLayout.NORTH);

                        //======== tabbedPaneGames ========
                        {

                            //======== panelGameGeneral ========
                            {
                                panelGameGeneral.setLayout(new GridBagLayout());
                                ((GridBagLayout)panelGameGeneral.getLayout()).columnWidths = new int[] {115, 105, 0, 105, 0, 0, 0};
                                ((GridBagLayout)panelGameGeneral.getLayout()).rowHeights = new int[] {10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                                ((GridBagLayout)panelGameGeneral.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0E-4};
                                ((GridBagLayout)panelGameGeneral.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                                //---- labelGameTitle ----
                                labelGameTitle.setText("Game Title: ");
                                panelGameGeneral.add(labelGameTitle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));

                                //---- textFieldGameTitle ----
                                textFieldGameTitle.setColumns(10);
                                panelGameGeneral.add(textFieldGameTitle, new GridBagConstraints(1, 1, 4, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelSystem ----
                                labelSystem.setText("System: ");
                                panelGameGeneral.add(labelSystem, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelGameGeneral.add(comboBoxSystem, new GridBagConstraints(1, 2, 4, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelOtherSystems ----
                                labelOtherSystems.setText("Other Systems:");
                                panelGameGeneral.add(labelOtherSystems, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 5, 5, 5), 0, 0));

                                //======== panelOtherSystems ========
                                {
                                    panelOtherSystems.setLayout(new BoxLayout(panelOtherSystems, BoxLayout.Y_AXIS));
                                    panelOtherSystems.add(comboBoxOtherSystems0);
                                    panelOtherSystems.add(comboBoxOtherSystems1);
                                    panelOtherSystems.add(comboBoxOtherSystems2);
                                    panelOtherSystems.add(comboBoxOtherSystems3);
                                }
                                panelGameGeneral.add(panelOtherSystems, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- buttonAddOtherSystem ----
                                buttonAddOtherSystem.setText("More");
                                buttonAddOtherSystem.setMnemonic('M');
                                buttonAddOtherSystem.addActionListener(e -> buttonAddOtherSystemActionPerformed());
                                panelGameGeneral.add(buttonAddOtherSystem, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelRepeatSystems ----
                                labelRepeatSystems.setText("Repeat Systems:");
                                panelGameGeneral.add(labelRepeatSystems, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 5, 5, 5), 0, 0));

                                //======== panelRepeatSystems ========
                                {
                                    panelRepeatSystems.setLayout(new BoxLayout(panelRepeatSystems, BoxLayout.Y_AXIS));
                                    panelRepeatSystems.add(comboBoxRepeatSystems0);
                                    panelRepeatSystems.add(comboBoxRepeatSystems1);
                                    panelRepeatSystems.add(comboBoxRepeatSystems2);
                                    panelRepeatSystems.add(comboBoxRepeatSystems3);
                                }
                                panelGameGeneral.add(panelRepeatSystems, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- buttonAddRepeatSystems ----
                                buttonAddRepeatSystems.setText("More");
                                buttonAddRepeatSystems.setMnemonic('R');
                                buttonAddRepeatSystems.addActionListener(e -> buttonAddRepeatSystemsActionPerformed());
                                panelGameGeneral.add(buttonAddRepeatSystems, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelPlayerMin ----
                                labelPlayerMin.setText("Number of Players: ");
                                panelGameGeneral.add(labelPlayerMin, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));

                                //---- textFieldMinPlayers ----
                                textFieldMinPlayers.setColumns(4);
                                panelGameGeneral.add(textFieldMinPlayers, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelTo ----
                                labelTo.setText("to");
                                panelGameGeneral.add(labelTo, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- textFieldMaxPlayers ----
                                textFieldMaxPlayers.setColumns(4);
                                panelGameGeneral.add(textFieldMaxPlayers, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelDeveloper ----
                                labelDeveloper.setText("Developer: ");
                                panelGameGeneral.add(labelDeveloper, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelGameGeneral.add(textFieldDeveloper, new GridBagConstraints(1, 6, 3, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelPublisher ----
                                labelPublisher.setText("Publisher:");
                                panelGameGeneral.add(labelPublisher, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelGameGeneral.add(textFieldPublisher, new GridBagConstraints(1, 7, 3, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelReleaseYear ----
                                labelReleaseYear.setText("Originally Released:");
                                panelGameGeneral.add(labelReleaseYear, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelGameGeneral.add(textFieldOriginalReleaseYear, new GridBagConstraints(1, 8, 3, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelReReleaseYear ----
                                labelReReleaseYear.setText("ReReleased: ");
                                panelGameGeneral.add(labelReReleaseYear, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 5, 5, 5), 0, 0));

                                //======== panelReReleaseYears ========
                                {
                                    panelReReleaseYears.setLayout(new BoxLayout(panelReReleaseYears, BoxLayout.Y_AXIS));
                                    panelReReleaseYears.add(textFieldReReleaseYear0);
                                    panelReReleaseYears.add(textFieldReReleaseYear1);
                                    panelReReleaseYears.add(textFieldReReleaseYear2);
                                    panelReReleaseYears.add(textFieldReReleaseYear3);
                                }
                                panelGameGeneral.add(panelReReleaseYears, new GridBagConstraints(1, 9, 3, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- buttonAddReReleaseYear ----
                                buttonAddReReleaseYear.setText("More");
                                buttonAddReReleaseYear.setMnemonic('E');
                                buttonAddReReleaseYear.addActionListener(e -> buttonAddReReleaseYearActionPerformed());
                                panelGameGeneral.add(buttonAddReReleaseYear, new GridBagConstraints(4, 9, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelGenre ----
                                labelGenre.setText("Genre: ");
                                panelGameGeneral.add(labelGenre, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelGameGeneral.add(comboBoxGenre, new GridBagConstraints(1, 10, 3, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelSeries ----
                                labelSeries.setText("Series:");
                                panelGameGeneral.add(labelSeries, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 0, 5), 0, 0));
                                panelGameGeneral.add(textFieldSeries, new GridBagConstraints(1, 11, 3, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 0, 5), 0, 0));
                            }
                            tabbedPaneGames.addTab("General", panelGameGeneral);

                            //======== panelDetailsPanel ========
                            {
                                panelDetailsPanel.setLayout(new GridBagLayout());
                                ((GridBagLayout)panelDetailsPanel.getLayout()).columnWidths = new int[] {115, 0, 0, 0, 0};
                                ((GridBagLayout)panelDetailsPanel.getLayout()).rowHeights = new int[] {10, 0, 0, 0, 0, 0, 45, 0, 0, 0, 0, 0, 0, 0};
                                ((GridBagLayout)panelDetailsPanel.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 0.0, 1.0E-4};
                                ((GridBagLayout)panelDetailsPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                                //---- labelCoreAesthetic ----
                                labelCoreAesthetic.setText("Core Aesthetic:");
                                panelDetailsPanel.add(labelCoreAesthetic, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelDetailsPanel.add(comboBoxCoreAesthetic, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelOtherAesthetic ----
                                labelOtherAesthetic.setText("Other Aesthetic:");
                                panelDetailsPanel.add(labelOtherAesthetic, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelDetailsPanel.add(comboBoxOtherAesthetic, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelRecommendedFor ----
                                labelRecommendedFor.setText("Recommended For: ");
                                panelDetailsPanel.add(labelRecommendedFor, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelDetailsPanel.add(textFieldRecommendedFor, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelCompletionLevel ----
                                labelCompletionLevel.setText("Completion Level:");
                                panelDetailsPanel.add(labelCompletionLevel, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelDetailsPanel.add(comboBoxCompletionLevel, new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelRating ----
                                labelRating.setText("Rating:");
                                panelDetailsPanel.add(labelRating, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelDetailsPanel.add(comboBoxRating, new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelComments ----
                                labelComments.setText("Comments: ");
                                panelDetailsPanel.add(labelComments, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                                    new Insets(0, 5, 5, 5), 0, 0));

                                //======== scrollPaneComments ========
                                {

                                    //---- textAreaComments ----
                                    textAreaComments.setLineWrap(true);
                                    textAreaComments.setWrapStyleWord(true);
                                    textAreaComments.setToolTipText("Max Length: 2255");
                                    scrollPaneComments.setViewportView(textAreaComments);
                                }
                                panelDetailsPanel.add(scrollPaneComments, new GridBagConstraints(1, 6, 2, 2, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelThoughtsLink ----
                                labelThoughtsLink.setText("Link:");
                                panelDetailsPanel.add(labelThoughtsLink, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));

                                //---- buttonOpenLink ----
                                buttonOpenLink.setText("Open");
                                buttonOpenLink.setMnemonic('P');
                                buttonOpenLink.addActionListener(e -> buttonOpenLinkActionPerformed());
                                panelDetailsPanel.add(buttonOpenLink, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- textFieldThoughtsLink ----
                                textFieldThoughtsLink.setToolTipText("Max Length: 1048");
                                panelDetailsPanel.add(textFieldThoughtsLink, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- labelThoughtsType ----
                                labelThoughtsType.setText("Thoughts:");
                                panelDetailsPanel.add(labelThoughtsType, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 5, 5, 5), 0, 0));
                                panelDetailsPanel.add(comboBoxThoughtsType, new GridBagConstraints(1, 9, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- checkBoxRevisit ----
                                checkBoxRevisit.setText("Should Revisit");
                                panelDetailsPanel.add(checkBoxRevisit, new GridBagConstraints(1, 10, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- checkBoxCouch ----
                                checkBoxCouch.setText("Excellent Local Multiplayer");
                                panelDetailsPanel.add(checkBoxCouch, new GridBagConstraints(1, 11, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 5, 5), 0, 0));

                                //---- checkBoxHallOfFame ----
                                checkBoxHallOfFame.setText("Hall of Fame");
                                panelDetailsPanel.add(checkBoxHallOfFame, new GridBagConstraints(1, 12, 2, 1, 0.0, 0.0,
                                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                    new Insets(0, 0, 0, 5), 0, 0));
                            }
                            tabbedPaneGames.addTab("Details", panelDetailsPanel);
                        }
                        panelWorkGames.add(tabbedPaneGames, BorderLayout.CENTER);
                    }
                    splitPaneGames.setRightComponent(panelWorkGames);
                }
                tabbedPane.addTab("Games", splitPaneGames);

                //======== splitPaneSystems ========
                {

                    //======== panelSearchSystems ========
                    {
                        panelSearchSystems.setLayout(new GridBagLayout());
                        ((GridBagLayout)panelSearchSystems.getLayout()).columnWidths = new int[] {85, 120, 0};
                        ((GridBagLayout)panelSearchSystems.getLayout()).rowHeights = new int[] {10, 0, 25, 0, 0, 0};
                        ((GridBagLayout)panelSearchSystems.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
                        ((GridBagLayout)panelSearchSystems.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};

                        //---- labelSystemSubSearch ----
                        labelSystemSubSearch.setText("System:");
                        panelSearchSystems.add(labelSystemSubSearch, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- textFieldSystemSearchFilter ----
                        textFieldSystemSearchFilter.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                textFieldSystemSearchFilterKeyPressed(e);
                            }
                        });
                        panelSearchSystems.add(textFieldSystemSearchFilter, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- buttonSystemSearch ----
                        buttonSystemSearch.setText("Search");
                        buttonSystemSearch.setMnemonic('A');
                        buttonSystemSearch.addActionListener(e -> buttonSystemSearchActionPerformed());
                        panelSearchSystems.add(buttonSystemSearch, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //======== scrollPaneSystemSearchResults ========
                        {

                            //---- listSystemSearchResults ----
                            listSystemSearchResults.addMouseListener(new MouseAdapter() {
                                @Override
                                public void mouseClicked(MouseEvent e) {
                                    listSystemSearchResultsMouseClicked(e);
                                }
                            });
                            listSystemSearchResults.addKeyListener(new KeyAdapter() {
                                @Override
                                public void keyPressed(KeyEvent e) {
                                    listSystemSearchResultsEnterKeyPressed(e);
                                }
                            });
                            scrollPaneSystemSearchResults.setViewportView(listSystemSearchResults);
                        }
                        panelSearchSystems.add(scrollPaneSystemSearchResults, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 0), 0, 0));

                        //---- buttonSystemOpen ----
                        buttonSystemOpen.setText("Open");
                        buttonSystemOpen.setMnemonic('O');
                        buttonSystemOpen.addActionListener(e -> buttonSystemOpenActionPerformed());
                        panelSearchSystems.add(buttonSystemOpen, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 0, 5), 0, 0));

                        //---- labelSystemCount ----
                        labelSystemCount.setText(" Search Results: 1000");
                        panelSearchSystems.add(labelSystemCount, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 0, 0), 0, 0));
                    }
                    splitPaneSystems.setLeftComponent(panelSearchSystems);

                    //======== panelWorkSystem ========
                    {
                        panelWorkSystem.setLayout(new GridBagLayout());
                        ((GridBagLayout)panelWorkSystem.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
                        ((GridBagLayout)panelWorkSystem.getLayout()).rowHeights = new int[] {10, 0, 0, 0, 0, 0, 0};
                        ((GridBagLayout)panelWorkSystem.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};
                        ((GridBagLayout)panelWorkSystem.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                        //---- buttonNewSystem ----
                        buttonNewSystem.setText("New System");
                        buttonNewSystem.setDisplayedMnemonicIndex(0);
                        buttonNewSystem.setMnemonic('N');
                        buttonNewSystem.addActionListener(e -> buttonNewSystemActionPerformed());
                        panelWorkSystem.add(buttonNewSystem, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 5, 5), 0, 0));

                        //---- buttonSaveSystem ----
                        buttonSaveSystem.setText("Save System");
                        buttonSaveSystem.setDisplayedMnemonicIndex(0);
                        buttonSaveSystem.setMnemonic('S');
                        buttonSaveSystem.addActionListener(e -> buttonSaveSystemActionPerformed());
                        panelWorkSystem.add(buttonSaveSystem, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 5), 0, 0));

                        //---- buttonDeleteSystem ----
                        buttonDeleteSystem.setText("Delete System");
                        buttonDeleteSystem.setMnemonic('D');
                        buttonDeleteSystem.addActionListener(e -> buttonDeleteSystemActionPerformed());
                        panelWorkSystem.add(buttonDeleteSystem, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 5), 0, 0));

                        //---- labelSubSystem ----
                        labelSubSystem.setText("System: ");
                        panelWorkSystem.add(labelSubSystem, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 5, 5), 0, 0));
                        panelWorkSystem.add(textFieldSystemName, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- labelGeneration ----
                        labelGeneration.setText("Generation: ");
                        panelWorkSystem.add(labelGeneration, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 5, 5), 0, 0));
                        panelWorkSystem.add(textFieldGeneration, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- checkBoxIsHandheld ----
                        checkBoxIsHandheld.setText("Handheld");
                        panelWorkSystem.add(checkBoxIsHandheld, new GridBagConstraints(1, 4, 2, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 5), 0, 0));
                    }
                    splitPaneSystems.setRightComponent(panelWorkSystem);
                }
                tabbedPane.addTab("Systems", splitPaneSystems);

                //======== splitPaneGenres ========
                {

                    //======== panelSearchGenres ========
                    {
                        panelSearchGenres.setLayout(new GridBagLayout());
                        ((GridBagLayout)panelSearchGenres.getLayout()).columnWidths = new int[] {85, 120, 0};
                        ((GridBagLayout)panelSearchGenres.getLayout()).rowHeights = new int[] {10, 0, 0, 0, 0, 0};
                        ((GridBagLayout)panelSearchGenres.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
                        ((GridBagLayout)panelSearchGenres.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};

                        //---- labelGenreSubSearch ----
                        labelGenreSubSearch.setText("Genre:");
                        panelSearchGenres.add(labelGenreSubSearch, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- textFieldGenreFilter ----
                        textFieldGenreFilter.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                textFieldGenreFilterKeyPressed(e);
                            }
                        });
                        panelSearchGenres.add(textFieldGenreFilter, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- buttonGenreSearch ----
                        buttonGenreSearch.setText("Search");
                        buttonGenreSearch.setMnemonic('A');
                        buttonGenreSearch.addActionListener(e -> buttonGenreSearchActionPerformed());
                        panelSearchGenres.add(buttonGenreSearch, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //======== scrollPaneGenreSearchResults ========
                        {

                            //---- listGenreSearchResults ----
                            listGenreSearchResults.addMouseListener(new MouseAdapter() {
                                @Override
                                public void mouseClicked(MouseEvent e) {
                                    listGenreSearchResultsMouseClicked(e);
                                }
                            });
                            listGenreSearchResults.addKeyListener(new KeyAdapter() {
                                @Override
                                public void keyPressed(KeyEvent e) {
                                    listGenreSearchResultsEnterKeyPressed(e);
                                }
                            });
                            scrollPaneGenreSearchResults.setViewportView(listGenreSearchResults);
                        }
                        panelSearchGenres.add(scrollPaneGenreSearchResults, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 0), 0, 0));

                        //---- buttonGenreOpen ----
                        buttonGenreOpen.setText("Open");
                        buttonGenreOpen.setMnemonic('O');
                        buttonGenreOpen.addActionListener(e -> buttonGenreOpenActionPerformed());
                        panelSearchGenres.add(buttonGenreOpen, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 0, 5), 0, 0));

                        //---- labelGenreCount ----
                        labelGenreCount.setText("Search Results: 1000");
                        panelSearchGenres.add(labelGenreCount, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 0, 0), 0, 0));
                    }
                    splitPaneGenres.setLeftComponent(panelSearchGenres);

                    //======== panelWorkGenre ========
                    {
                        panelWorkGenre.setLayout(new GridBagLayout());
                        ((GridBagLayout)panelWorkGenre.getLayout()).columnWidths = new int[] {101, 99, 0, 0, 0};
                        ((GridBagLayout)panelWorkGenre.getLayout()).rowHeights = new int[] {10, 0, 0, 0, 0};
                        ((GridBagLayout)panelWorkGenre.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};
                        ((GridBagLayout)panelWorkGenre.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

                        //---- buttonNewGenre ----
                        buttonNewGenre.setText("New Genre");
                        buttonNewGenre.setMnemonic('N');
                        buttonNewGenre.addActionListener(e -> buttonNewGenreActionPerformed());
                        panelWorkGenre.add(buttonNewGenre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 5, 5), 0, 0));

                        //---- buttonSaveGenre ----
                        buttonSaveGenre.setText("Save Genre");
                        buttonSaveGenre.setMnemonic('S');
                        buttonSaveGenre.addActionListener(e -> buttonSaveGenreActionPerformed());
                        panelWorkGenre.add(buttonSaveGenre, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 5), 0, 0));

                        //---- buttonDeleteGenre ----
                        buttonDeleteGenre.setText("Delete Genre");
                        buttonDeleteGenre.setMnemonic('D');
                        buttonDeleteGenre.addActionListener(e -> buttonDeleteGenreActionPerformed());
                        panelWorkGenre.add(buttonDeleteGenre, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 5), 0, 0));

                        //---- labelSubGenre ----
                        labelSubGenre.setText("Genre: ");
                        panelWorkGenre.add(labelSubGenre, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 5, 5), 0, 0));
                        panelWorkGenre.add(textFieldGenreName, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));
                    }
                    splitPaneGenres.setRightComponent(panelWorkGenre);
                }
                tabbedPane.addTab("Genres", splitPaneGenres);

                //======== splitPaneToys ========
                {

                    //======== panelSearchToys ========
                    {
                        panelSearchToys.setLayout(new GridBagLayout());
                        ((GridBagLayout)panelSearchToys.getLayout()).columnWidths = new int[] {85, 120, 0};
                        ((GridBagLayout)panelSearchToys.getLayout()).rowHeights = new int[] {10, 0, 0, 0, 0, 0, 0, 0};
                        ((GridBagLayout)panelSearchToys.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
                        ((GridBagLayout)panelSearchToys.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4};

                        //---- labelToysName ----
                        labelToysName.setText("Toy:");
                        panelSearchToys.add(labelToysName, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- textFieldToysFilter ----
                        textFieldToysFilter.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                textFieldToysFilterKeyPressed(e);
                            }
                        });
                        panelSearchToys.add(textFieldToysFilter, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- labelToysSetFilter ----
                        labelToysSetFilter.setText("Set:");
                        panelSearchToys.add(labelToysSetFilter, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- textFieldToysSetFilter ----
                        textFieldToysSetFilter.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                textFieldToysSetFilterKeyPressed(e);
                            }
                        });
                        panelSearchToys.add(textFieldToysSetFilter, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- labelSearchPlatform ----
                        labelSearchPlatform.setText("Platform:");
                        panelSearchToys.add(labelSearchPlatform, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //---- textFieldPlatformFilter ----
                        textFieldPlatformFilter.addKeyListener(new KeyAdapter() {
                            @Override
                            public void keyPressed(KeyEvent e) {
                                textFieldPlatformFilterKeyPressed(e);
                            }
                        });
                        panelSearchToys.add(textFieldPlatformFilter, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- buttonToysSearch ----
                        buttonToysSearch.setText("Search");
                        buttonToysSearch.setMnemonic('A');
                        buttonToysSearch.addActionListener(e -> buttonToysSearchActionPerformed());
                        panelSearchToys.add(buttonToysSearch, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 5), 0, 0));

                        //======== scrollPaneToysSearchResults ========
                        {

                            //---- listToysSearchResults ----
                            listToysSearchResults.addMouseListener(new MouseAdapter() {
                                @Override
                                public void mouseClicked(MouseEvent e) {
                                    listToysSearchResultsMouseClicked(e);
                                }
                            });
                            listToysSearchResults.addKeyListener(new KeyAdapter() {
                                @Override
                                public void keyPressed(KeyEvent e) {
                                    listToysSearchResultsEnterKeyPressed(e);
                                }
                            });
                            scrollPaneToysSearchResults.setViewportView(listToysSearchResults);
                        }
                        panelSearchToys.add(scrollPaneToysSearchResults, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 5, 0), 0, 0));

                        //---- buttonToyOpen ----
                        buttonToyOpen.setText("Open");
                        buttonToyOpen.setMnemonic('O');
                        buttonToyOpen.addActionListener(e -> buttonToyOpenActionPerformed());
                        panelSearchToys.add(buttonToyOpen, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 10, 0, 5), 0, 0));

                        //---- labelToyCount ----
                        labelToyCount.setText("Search Results: 1000");
                        panelSearchToys.add(labelToyCount, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 0, 0), 0, 0));
                    }
                    splitPaneToys.setLeftComponent(panelSearchToys);

                    //======== panelWorkToys ========
                    {
                        panelWorkToys.setLayout(new GridBagLayout());
                        ((GridBagLayout)panelWorkToys.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
                        ((GridBagLayout)panelWorkToys.getLayout()).rowHeights = new int[] {10, 0, 0, 0, 0, 0};
                        ((GridBagLayout)panelWorkToys.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};
                        ((GridBagLayout)panelWorkToys.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                        //---- buttonNewToy ----
                        buttonNewToy.setText("New Toy");
                        buttonNewToy.setMnemonic('N');
                        buttonNewToy.addActionListener(e -> buttonNewToyActionPerformed());
                        panelWorkToys.add(buttonNewToy, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 5, 5), 0, 0));

                        //---- buttonSaveToy ----
                        buttonSaveToy.setText("Save Toy");
                        buttonSaveToy.setMnemonic('S');
                        buttonSaveToy.addActionListener(e -> buttonSaveToyActionPerformed());
                        panelWorkToys.add(buttonSaveToy, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 5), 0, 0));

                        //---- buttonDeleteToy ----
                        buttonDeleteToy.setText("Delete Toy");
                        buttonDeleteToy.setMnemonic('D');
                        buttonDeleteToy.addActionListener(e -> buttonDeleteToyActionPerformed());
                        panelWorkToys.add(buttonDeleteToy, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 5), 0, 0));

                        //---- labelToyName ----
                        labelToyName.setText("Toy:");
                        panelWorkToys.add(labelToyName, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 5, 5), 0, 0));
                        panelWorkToys.add(textFieldToyName, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- labelToySet ----
                        labelToySet.setText("Set:");
                        panelWorkToys.add(labelToySet, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 5, 5), 0, 0));
                        panelWorkToys.add(textFieldToySet, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 5, 0), 0, 0));

                        //---- labelPlatform ----
                        labelPlatform.setText("Platform:");
                        panelWorkToys.add(labelPlatform, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 5, 0, 5), 0, 0));
                        panelWorkToys.add(textFieldPlatform, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0,
                            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                            new Insets(0, 0, 0, 0), 0, 0));
                    }
                    splitPaneToys.setRightComponent(panelWorkToys);
                }
                tabbedPane.addTab("Toys", splitPaneToys);

                //======== panelWorkQueries ========
                {
                    panelWorkQueries.setLayout(new GridBagLayout());
                    ((GridBagLayout)panelWorkQueries.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
                    ((GridBagLayout)panelWorkQueries.getLayout()).rowHeights = new int[] {10, 0, 20, 0};
                    ((GridBagLayout)panelWorkQueries.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};
                    ((GridBagLayout)panelWorkQueries.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};

                    //---- buttonMasterReport ----
                    buttonMasterReport.setText("Master List");
                    buttonMasterReport.addActionListener(e -> buttonMasterReportActionPerformed());
                    panelWorkQueries.add(buttonMasterReport, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- buttonFilterReport ----
                    buttonFilterReport.setText("Filters Report");
                    buttonFilterReport.addActionListener(e -> buttonFilterReportActionPerformed());
                    panelWorkQueries.add(buttonFilterReport, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- buttonCountReport ----
                    buttonCountReport.setText("Count Report");
                    buttonCountReport.addActionListener(e -> buttonCountReportActionPerformed());
                    panelWorkQueries.add(buttonCountReport, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- buttonToysReport ----
                    buttonToysReport.setText("Toys Report");
                    buttonToysReport.addActionListener(e -> buttonToysReportActionPerformed());
                    panelWorkQueries.add(buttonToysReport, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //======== scrollPaneReportsDisplay ========
                    {

                        //---- textAreaReportsDisplay ----
                        textAreaReportsDisplay.setFont(new Font("Consolas", Font.PLAIN, 12));
                        scrollPaneReportsDisplay.setViewportView(textAreaReportsDisplay);
                    }
                    panelWorkQueries.add(scrollPaneReportsDisplay, new GridBagConstraints(0, 2, 5, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
                }
                tabbedPane.addTab("Reports", panelWorkQueries);
            }
            splitPaneLarge.setTopComponent(tabbedPane);

            //======== panelDebug ========
            {
                panelDebug.setLayout(new BorderLayout());

                //======== scrollPaneDebug ========
                {
                    scrollPaneDebug.setViewportView(textAreaDebug);
                }
                panelDebug.add(scrollPaneDebug, BorderLayout.CENTER);
            }
            splitPaneLarge.setBottomComponent(panelDebug);
        }
        contentPane.add(splitPaneLarge, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JMenuBar menuBar;
    private JMenu menuDatabase;
    private JMenuItem menuItemNew;
    private JMenuItem menuItemLoad;
    private JMenu menuAbout;
    private JMenuItem menuItemVersion;
    private JMenu menuView;
    private JCheckBoxMenuItem menuItemViewDebugPanel;
    private JSplitPane splitPaneLarge;
    private JTabbedPane tabbedPane;
    private JSplitPane splitPaneGames;
    private JPanel panelSearchGames;
    private JLabel labelTitleSearch;
    private JTextField textFieldTitleSearch;
    private JLabel labelSystemSearch;
    private JComboBox comboBoxSystemSearch;
    private JLabel labelGenreSearch;
    private JComboBox comboBoxGenreSearch;
    private JCheckBox checkBoxRepeatSystemNormalSearch;
    private JButton buttonSearch;
    private JButton buttonAdvanceSearch;
    private JScrollPane scrollPaneSearch;
    private JList listSearchResults;
    private JButton buttonOpen;
    private JLabel labelGamesCount;
    private JPanel panelWorkGames;
    private JPanel panel1;
    private JButton buttonNewEntry;
    private JButton buttonSaveEntry;
    private JButton buttonDeleteGame;
    private JButton buttonChangeTab;
    private JTabbedPane tabbedPaneGames;
    private JPanel panelGameGeneral;
    private JLabel labelGameTitle;
    private JTextField textFieldGameTitle;
    private JLabel labelSystem;
    private JComboBox comboBoxSystem;
    private JLabel labelOtherSystems;
    private JPanel panelOtherSystems;
    private JComboBox comboBoxOtherSystems0;
    private JComboBox comboBoxOtherSystems1;
    private JComboBox comboBoxOtherSystems2;
    private JComboBox comboBoxOtherSystems3;
    private JButton buttonAddOtherSystem;
    private JLabel labelRepeatSystems;
    private JPanel panelRepeatSystems;
    private JComboBox comboBoxRepeatSystems0;
    private JComboBox comboBoxRepeatSystems1;
    private JComboBox comboBoxRepeatSystems2;
    private JComboBox comboBoxRepeatSystems3;
    private JButton buttonAddRepeatSystems;
    private JLabel labelPlayerMin;
    private JTextField textFieldMinPlayers;
    private JLabel labelTo;
    private JTextField textFieldMaxPlayers;
    private JLabel labelDeveloper;
    private JTextField textFieldDeveloper;
    private JLabel labelPublisher;
    private JTextField textFieldPublisher;
    private JLabel labelReleaseYear;
    private JTextField textFieldOriginalReleaseYear;
    private JLabel labelReReleaseYear;
    private JPanel panelReReleaseYears;
    private JTextField textFieldReReleaseYear0;
    private JTextField textFieldReReleaseYear1;
    private JTextField textFieldReReleaseYear2;
    private JTextField textFieldReReleaseYear3;
    private JButton buttonAddReReleaseYear;
    private JLabel labelGenre;
    private JComboBox comboBoxGenre;
    private JLabel labelSeries;
    private JTextField textFieldSeries;
    private JPanel panelDetailsPanel;
    private JLabel labelCoreAesthetic;
    private JComboBox comboBoxCoreAesthetic;
    private JLabel labelOtherAesthetic;
    private JComboBox comboBoxOtherAesthetic;
    private JLabel labelRecommendedFor;
    private JTextField textFieldRecommendedFor;
    private JLabel labelCompletionLevel;
    private JComboBox comboBoxCompletionLevel;
    private JLabel labelRating;
    private JComboBox comboBoxRating;
    private JLabel labelComments;
    private JScrollPane scrollPaneComments;
    private JTextArea textAreaComments;
    private JLabel labelThoughtsLink;
    private JButton buttonOpenLink;
    private JTextField textFieldThoughtsLink;
    private JLabel labelThoughtsType;
    private JComboBox comboBoxThoughtsType;
    private JCheckBox checkBoxRevisit;
    private JCheckBox checkBoxCouch;
    private JCheckBox checkBoxHallOfFame;
    private JSplitPane splitPaneSystems;
    private JPanel panelSearchSystems;
    private JLabel labelSystemSubSearch;
    private JTextField textFieldSystemSearchFilter;
    private JButton buttonSystemSearch;
    private JScrollPane scrollPaneSystemSearchResults;
    private JList listSystemSearchResults;
    private JButton buttonSystemOpen;
    private JLabel labelSystemCount;
    private JPanel panelWorkSystem;
    private JButton buttonNewSystem;
    private JButton buttonSaveSystem;
    private JButton buttonDeleteSystem;
    private JLabel labelSubSystem;
    private JTextField textFieldSystemName;
    private JLabel labelGeneration;
    private JTextField textFieldGeneration;
    private JCheckBox checkBoxIsHandheld;
    private JSplitPane splitPaneGenres;
    private JPanel panelSearchGenres;
    private JLabel labelGenreSubSearch;
    private JTextField textFieldGenreFilter;
    private JButton buttonGenreSearch;
    private JScrollPane scrollPaneGenreSearchResults;
    private JList listGenreSearchResults;
    private JButton buttonGenreOpen;
    private JLabel labelGenreCount;
    private JPanel panelWorkGenre;
    private JButton buttonNewGenre;
    private JButton buttonSaveGenre;
    private JButton buttonDeleteGenre;
    private JLabel labelSubGenre;
    private JTextField textFieldGenreName;
    private JSplitPane splitPaneToys;
    private JPanel panelSearchToys;
    private JLabel labelToysName;
    private JTextField textFieldToysFilter;
    private JLabel labelToysSetFilter;
    private JTextField textFieldToysSetFilter;
    private JLabel labelSearchPlatform;
    private JTextField textFieldPlatformFilter;
    private JButton buttonToysSearch;
    private JScrollPane scrollPaneToysSearchResults;
    private JList listToysSearchResults;
    private JButton buttonToyOpen;
    private JLabel labelToyCount;
    private JPanel panelWorkToys;
    private JButton buttonNewToy;
    private JButton buttonSaveToy;
    private JButton buttonDeleteToy;
    private JLabel labelToyName;
    private JTextField textFieldToyName;
    private JLabel labelToySet;
    private JTextField textFieldToySet;
    private JLabel labelPlatform;
    private JTextField textFieldPlatform;
    private JPanel panelWorkQueries;
    private JButton buttonMasterReport;
    private JButton buttonFilterReport;
    private JButton buttonCountReport;
    private JButton buttonToysReport;
    private JScrollPane scrollPaneReportsDisplay;
    private JTextArea textAreaReportsDisplay;
    private JPanel panelDebug;
    private JScrollPane scrollPaneDebug;
    private JTextArea textAreaDebug;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
