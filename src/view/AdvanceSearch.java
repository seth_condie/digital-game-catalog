/*
 * Created by JFormDesigner on Fri Jul 01 21:42:02 MDT 2016
 */

package view;

import controller.objectClasses.AdvancedSearchFilters;
import controller.Globals;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The advanceSearch GUI element
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public class AdvanceSearch extends JFrame {

    private DigitalGameCatGUI parent;
    private int location;
    private int andSystemCount;
    private List<String> systemComboBoxList;
    private List<String> genreComboBoxList;
    private List<String> aestheticComboBoxList;
    private List<String> completionLevelComboBoxList;
    private List<String> ratingComboBoxList;
    private List<String> thoughtsTypeComboBoxList;

    public AdvanceSearch(
            DigitalGameCatGUI parent,
            int location,
            List<String> systemComboBoxListInput,
            List<String> genreComboBoxListInput,
            List<String> aestheticComboBoxListInput,
            List<String> completionLevelComboBoxListInput,
            List<String> ratingComboBoxListInput,
            List<String> thoughtsTypeComboBoxListInput,
            AdvancedSearchFilters filters) {
        this.parent = parent;
        this.location = location;
        initComponents();
        this.setLocationRelativeTo(parent);
        if (location == Globals.ADVANCE_SEARCH_LOCATION_REPORT) {
            this.setTitle("Filter Report");
        }

        systemComboBoxList = systemComboBoxListInput;
        if (!systemComboBoxList.contains(Globals.NO_FILTER)) {
            systemComboBoxList.add(Globals.NO_FILTER);
        }

        setupAndSystemDisplay(systemComboBoxListInput);

        comboBoxSystemFilter.setModel(new DefaultComboBoxModel(systemComboBoxList.toArray()));
        comboBoxOtherSystemFilter.setModel(new DefaultComboBoxModel(systemComboBoxList.toArray()));
        comboBoxRepeatSystemFilter.setModel(new DefaultComboBoxModel(systemComboBoxList.toArray()));

        genreComboBoxList = genreComboBoxListInput;
        if (!genreComboBoxList.contains(Globals.NO_FILTER)) {
            genreComboBoxList.add(Globals.NO_FILTER);
        }
        comboBoxGenre.setModel(new DefaultComboBoxModel(genreComboBoxList.toArray()));

        aestheticComboBoxList = aestheticComboBoxListInput;
        if (!aestheticComboBoxList.contains(Globals.NO_FILTER)) {
            aestheticComboBoxList.add(Globals.NO_FILTER);
        }
        comboBoxCoreAestheticFilter.setModel(new DefaultComboBoxModel(aestheticComboBoxList.toArray()));
        comboBoxOtherAestheticFilter.setModel(new DefaultComboBoxModel(aestheticComboBoxList.toArray()));

        completionLevelComboBoxList = completionLevelComboBoxListInput;
        if (!completionLevelComboBoxList.contains(Globals.NO_FILTER)) {
            completionLevelComboBoxList.add(Globals.NO_FILTER);
        }
        comboBoxCompletionLevelFilter.setModel(new DefaultComboBoxModel(completionLevelComboBoxList.toArray()));

        ratingComboBoxList = ratingComboBoxListInput;
        if (!ratingComboBoxList.contains(Globals.NO_FILTER)) {
            ratingComboBoxList.add(Globals.NO_FILTER);
        }
        comboBoxRatingFilter.setModel(new DefaultComboBoxModel(ratingComboBoxList.toArray()));

        thoughtsTypeComboBoxList = thoughtsTypeComboBoxListInput;
        if (!thoughtsTypeComboBoxList.contains(Globals.NO_FILTER)) {
            thoughtsTypeComboBoxList.add(Globals.NO_FILTER);
        }
        comboBoxThoughtsTypeFilter.setModel(new DefaultComboBoxModel(thoughtsTypeComboBoxList.toArray()));

        if(filters == null){
            clearButtonActionPerformed();
        } else {
            textFieldTitleFilter.setText(filters.getTitle());

            List<String> systemFilterList = new ArrayList();
            String system1 = filters.getSystem();
            if (system1 != null && !system1.equals(Globals.NO_FILTER)) {
                systemFilterList.add(system1);
            }
            String system2 = filters.getAndSystem1();
            if (system2 != null && !system2.equals(Globals.NO_FILTER)) {
                systemFilterList.add(system2);
            }
            String system3 = filters.getAndSystem2();
            if (system3 != null && !system3.equals(Globals.NO_FILTER)) {
                systemFilterList.add(system3);
            }
            String system4 = filters.getAndSystem3();
            if (system4 != null && !system4.equals(Globals.NO_FILTER)) {
                systemFilterList.add(system4);
            }
            andSystemCount = systemFilterList.size();
            comboBoxSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
            comboBoxAndSystem1.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
            comboBoxAndSystem2.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
            comboBoxAndSystem3.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
            panelSystem.remove(comboBoxAndSystem1);
            panelSystem.remove(comboBoxAndSystem2);
            panelSystem.remove(comboBoxAndSystem3);
            buttonAddSystem.setEnabled(true);
            switch(andSystemCount){
                case 4:
                    comboBoxSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(0)));
                    panelSystem.add(comboBoxAndSystem1);
                    comboBoxAndSystem1.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(1)));
                    panelSystem.add(comboBoxAndSystem2);
                    comboBoxAndSystem2.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(2)));
                    panelSystem.add(comboBoxAndSystem3);
                    comboBoxAndSystem3.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(3)));
                    buttonAddSystem.setEnabled(false);
                    andSystemCount--;
                    break;
                case 3:
                    comboBoxSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(0)));
                    panelSystem.add(comboBoxAndSystem1);
                    comboBoxAndSystem1.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(1)));
                    panelSystem.add(comboBoxAndSystem2);
                    comboBoxAndSystem2.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(2)));
                    andSystemCount--;
                    break;
                case 2:
                    comboBoxSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(0)));
                    panelSystem.add(comboBoxAndSystem1);
                    comboBoxAndSystem1.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(1)));
                    andSystemCount--;
                    break;
                case 1:
                    comboBoxSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(systemFilterList.get(0)));
                    andSystemCount--;
            }
            panelSystem.revalidate();
            panelSystem.repaint();

            comboBoxOtherSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(filters.getOtherSystem()));
            comboBoxRepeatSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(filters.getRepeatSystem()));

            if(filters.getPlayerMax() == Globals.NO_NUMBER_FILTER){
                textFieldPlayerFilter.setText(Globals.NO_FILTER);
            } else {
                textFieldPlayerFilter.setText(String.valueOf(filters.getPlayerMax()));
            }

            textFieldDeveloperFilter.setText(filters.getDeveloper());
            textFieldPublisherFilter.setText(filters.getPublisher());

            if (filters.getOriginallyReleased() == Globals.NO_NUMBER_FILTER){
                textFieldOriginallyReleasedFilter.setText(Globals.NO_FILTER);
            } else {
                textFieldOriginallyReleasedFilter.setText(String.valueOf(filters.getOriginallyReleased()));
            }

            if (filters.getReRelease() == Globals.NO_NUMBER_FILTER){
                textFieldReReleasedFilter.setText(Globals.NO_FILTER);
            } else {
                textFieldReReleasedFilter.setText(String.valueOf(filters.getReRelease()));
            }

            comboBoxGenre.setSelectedIndex(genreComboBoxList.indexOf(filters.getGenre()));

            textFieldSeries.setText(filters.getSeries());

            comboBoxCoreAestheticFilter.setSelectedIndex(aestheticComboBoxList.indexOf(filters.getCoreAesthetic()));
            comboBoxOtherAestheticFilter.setSelectedIndex(aestheticComboBoxList.indexOf(filters.getOtherAesthetic()));
            comboBoxCompletionLevelFilter.setSelectedIndex(completionLevelComboBoxList.indexOf(filters.getCompletionLevel()));
            comboBoxRatingFilter.setSelectedIndex(ratingComboBoxList.indexOf(filters.getRating()));
            comboBoxThoughtsTypeFilter.setSelectedIndex(thoughtsTypeComboBoxList.indexOf(filters.getThoughtsType()));

            checkBoxUseRevisitFilter.setSelected(filters.isUseRevisitFilter());
            checkBoxRevisit.setSelected(filters.isRevisitFilter());
            checkBoxUseCouchFilter.setSelected(filters.isUseCouchFilter());
            checkBoxCouch.setSelected(filters.isCouchFilter());
            checkBoxUseHallOfFameFilter.setSelected(filters.isUseHallOfFameFilter());
            checkBoxHallOfFame.setSelected(filters.isHallOfFameFilter());
        }

        this.setVisible(true);
    }

    private void cancelButtonActionPerformed() {
        this.dispose();
        parent.buttonFilterReturn(Globals.ADVANCE_SEARCH_LOCATION_CANCEL, null);
    }

    private void searchButtonActionPerformed() {
        AdvancedSearchFilters filters = new AdvancedSearchFilters();
        filters.setTitle(textFieldTitleFilter.getText());
        filters.setSystem((String) comboBoxSystemFilter.getSelectedItem());
        filters.setAndSystem1((String) comboBoxAndSystem1.getSelectedItem());
        filters.setAndSystem2((String) comboBoxAndSystem2.getSelectedItem());
        filters.setAndSystem3((String) comboBoxAndSystem3.getSelectedItem());
        filters.setOtherSystem((String) comboBoxOtherSystemFilter.getSelectedItem());
        filters.setRepeatSystem((String) comboBoxRepeatSystemFilter.getSelectedItem());

        if (textFieldPlayerFilter.getText().length() > 0) {
            if (textFieldPlayerFilter.getText().equals(Globals.NO_FILTER)) {
                filters.setPlayerMax(Globals.NO_NUMBER_FILTER);
            } else {
                try {
                    filters.setPlayerMax(Integer.parseInt(textFieldPlayerFilter.getText()));
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "Make sure that the Max Players Filter is a whole number or 'No Filter'", "Player Max Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        } else {
            filters.setPlayerMax(Globals.MISSINGNO);
        }

        filters.setDeveloper(textFieldDeveloperFilter.getText());
        filters.setPublisher(textFieldPublisherFilter.getText());

        if (textFieldOriginallyReleasedFilter.getText().length() > 0) {
            if (textFieldOriginallyReleasedFilter.getText().equals(Globals.NO_FILTER)) {
                filters.setOriginallyReleased(Globals.NO_NUMBER_FILTER);
            } else {
                try {
                    filters.setOriginallyReleased(Integer.parseInt(textFieldOriginallyReleasedFilter.getText()));
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "Make sure that the Originally Released Filter is a whole number or 'No Filter'", "Originally Released Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        } else {
            filters.setOriginallyReleased(Globals.MISSINGNO);
        }

        if (textFieldReReleasedFilter.getText().length() > 0) {
            if (textFieldReReleasedFilter.getText().equals(Globals.NO_FILTER)) {
                filters.setReRelease(Globals.NO_NUMBER_FILTER);
            } else {
                try {
                    filters.setReRelease(Integer.parseInt(textFieldReReleasedFilter.getText()));
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(this, "Make sure that the ReRelease Filter is a whole number or 'No Filter'", "ReReleased Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        } else {
            filters.setReRelease(Globals.MISSINGNO);
        }

        filters.setGenre((String) comboBoxGenre.getSelectedItem());
        filters.setSeries(textFieldSeries.getText());
        filters.setCoreAesthetic((String) comboBoxCoreAestheticFilter.getSelectedItem());
        filters.setOtherAesthetic((String) comboBoxOtherAestheticFilter.getSelectedItem());
        filters.setCompletionLevel((String) comboBoxCompletionLevelFilter.getSelectedItem());
        filters.setRating((String) comboBoxRatingFilter.getSelectedItem());
        filters.setThoughtsType((String) comboBoxThoughtsTypeFilter.getSelectedItem());
        filters.setUseRevisitFilter(checkBoxUseRevisitFilter.isSelected());
        filters.setRevisitFilter(checkBoxRevisit.isSelected());
        filters.setUseCouchFilter(checkBoxUseCouchFilter.isSelected());
        filters.setCouchFilter(checkBoxCouch.isSelected());
        filters.setUseHallOfFameFilter(checkBoxUseHallOfFameFilter.isSelected());
        filters.setHallOfFameFilter(checkBoxHallOfFame.isSelected());

        if (location == Globals.ADVANCE_SEARCH_LOCATION_REPORT) {
            parent.buttonFilterReturn(Globals.ADVANCE_SEARCH_LOCATION_REPORT, filters);
        } else if (location == Globals.ADVANCE_SEARCH_LOCATION_SEARCH) {
            parent.buttonFilterReturn(Globals.ADVANCE_SEARCH_LOCATION_SEARCH, filters);
        } else {
            parent.buttonFilterReturn(Globals.ADVANCE_SEARCH_LOCATION_ERROR, null);
        }
        this.dispose();
    }

    private void setupAndSystemDisplay(List<String> systemComboBoxList) {
        andSystemCount = 0;
        comboBoxAndSystem1.setModel(new DefaultComboBoxModel(systemComboBoxList.toArray()));
        comboBoxAndSystem2.setModel(new DefaultComboBoxModel(systemComboBoxList.toArray()));
        comboBoxAndSystem3.setModel(new DefaultComboBoxModel(systemComboBoxList.toArray()));
        comboBoxAndSystem1.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxAndSystem2.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxAndSystem3.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        panelSystem.remove(comboBoxAndSystem1);
        panelSystem.remove(comboBoxAndSystem2);
        panelSystem.remove(comboBoxAndSystem3);
        buttonAddSystem.setEnabled(true);
    }

    private void buttonAddSystemActionPerformed() {
        switch(andSystemCount) {
            case 0:
                panelSystem.add(comboBoxAndSystem1);
                andSystemCount++;
                break;
            case 1:
                panelSystem.add(comboBoxAndSystem2);
                andSystemCount++;
                break;
            case 2:
                panelSystem.add(comboBoxAndSystem3);
                buttonAddSystem.setEnabled(false);
                andSystemCount++;
                break;
        }
        panelSystem.revalidate();
        panelSystem.repaint();
    }

    private void comboBoxSystemFilterActionPerformed() {
        if (checkBoxSetRepeatSystem.isSelected()) {
            comboBoxRepeatSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(comboBoxSystemFilter.getSelectedItem()));
        }
    }

    private void clearButtonActionPerformed() {
        textFieldTitleFilter.setText(Globals.NO_FILTER);
        andSystemCount = 0;
        comboBoxAndSystem1.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxAndSystem2.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxAndSystem3.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        panelSystem.remove(comboBoxAndSystem1);
        panelSystem.remove(comboBoxAndSystem2);
        panelSystem.remove(comboBoxAndSystem3);
        buttonAddSystem.setEnabled(true);
        comboBoxSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxOtherSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxRepeatSystemFilter.setSelectedIndex(systemComboBoxList.indexOf(Globals.NO_FILTER));
        textFieldPlayerFilter.setText(Globals.NO_FILTER);
        textFieldDeveloperFilter.setText(Globals.NO_FILTER);
        textFieldPublisherFilter.setText(Globals.NO_FILTER);
        textFieldOriginallyReleasedFilter.setText(Globals.NO_FILTER);
        textFieldReReleasedFilter.setText(Globals.NO_FILTER);
        comboBoxGenre.setSelectedIndex(genreComboBoxList.indexOf(Globals.NO_FILTER));
        textFieldSeries.setText(Globals.NO_FILTER);
        comboBoxCoreAestheticFilter.setSelectedIndex(aestheticComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxOtherAestheticFilter.setSelectedIndex(aestheticComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxCompletionLevelFilter.setSelectedIndex(completionLevelComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxRatingFilter.setSelectedIndex(ratingComboBoxList.indexOf(Globals.NO_FILTER));
        comboBoxThoughtsTypeFilter.setSelectedIndex(thoughtsTypeComboBoxList.indexOf(Globals.NO_FILTER));
        checkBoxRevisit.setSelected(false);
        checkBoxUseRevisitFilter.setSelected(false);
        checkBoxCouch.setSelected(false);
        checkBoxUseCouchFilter.setSelected(false);
        checkBoxHallOfFame.setSelected(false);
        checkBoxUseHallOfFameFilter.setSelected(false);
        checkBoxSetRepeatSystem.setSelected(true);

        panelSystem.revalidate();
        panelSystem.repaint();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner non-commercial license
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        gridPanel = new JPanel();
        labelTitle = new JLabel();
        textFieldTitleFilter = new JTextField();
        labelSystem = new JLabel();
        panelSystem = new JPanel();
        comboBoxSystemFilter = new JComboBox();
        comboBoxAndSystem1 = new JComboBox();
        comboBoxAndSystem2 = new JComboBox();
        comboBoxAndSystem3 = new JComboBox();
        buttonAddSystem = new JButton();
        labelOtherSystem = new JLabel();
        comboBoxOtherSystemFilter = new JComboBox();
        labelCoreAesthetic = new JLabel();
        comboBoxCoreAestheticFilter = new JComboBox();
        labelRepeatSystem = new JLabel();
        comboBoxRepeatSystemFilter = new JComboBox();
        labelOtherAesthetic = new JLabel();
        comboBoxOtherAestheticFilter = new JComboBox();
        labelPlayers = new JLabel();
        textFieldPlayerFilter = new JTextField();
        labelCompletionLevel = new JLabel();
        comboBoxCompletionLevelFilter = new JComboBox();
        labelDeveloper = new JLabel();
        textFieldDeveloperFilter = new JTextField();
        labelRating = new JLabel();
        comboBoxRatingFilter = new JComboBox();
        labelPublisher = new JLabel();
        textFieldPublisherFilter = new JTextField();
        labelThoughtsType = new JLabel();
        comboBoxThoughtsTypeFilter = new JComboBox();
        labelOriginallyReleased = new JLabel();
        textFieldOriginallyReleasedFilter = new JTextField();
        checkBoxUseRevisitFilter = new JCheckBox();
        checkBoxRevisit = new JCheckBox();
        labelRereleased = new JLabel();
        textFieldReReleasedFilter = new JTextField();
        checkBoxUseCouchFilter = new JCheckBox();
        checkBoxCouch = new JCheckBox();
        labelGenre = new JLabel();
        comboBoxGenre = new JComboBox();
        checkBoxUseHallOfFameFilter = new JCheckBox();
        checkBoxHallOfFame = new JCheckBox();
        labelSeries = new JLabel();
        textFieldSeries = new JTextField();
        checkBoxSetRepeatSystem = new JCheckBox();
        buttonBar = new JPanel();
        searchButton = new JButton();
        clearButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setTitle("Advanced Search");
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new BorderLayout());

                //======== gridPanel ========
                {
                    gridPanel.setLayout(new GridBagLayout());
                    ((GridBagLayout)gridPanel.getLayout()).columnWidths = new int[] {0, 185, 0, 0, 0, 0, 0};
                    ((GridBagLayout)gridPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                    ((GridBagLayout)gridPanel.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0E-4};
                    ((GridBagLayout)gridPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                    //---- labelTitle ----
                    labelTitle.setText("Title:");
                    gridPanel.add(labelTitle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- textFieldTitleFilter ----
                    textFieldTitleFilter.setToolTipText("Will filter out any games that don't have the given parameter in somewhere in the title.");
                    gridPanel.add(textFieldTitleFilter, new GridBagConstraints(1, 0, 4, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelSystem ----
                    labelSystem.setText("System:");
                    gridPanel.add(labelSystem, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                        new Insets(5, 0, 5, 5), 0, 0));

                    //======== panelSystem ========
                    {
                        panelSystem.setToolTipText("Will return ANY game that is listed to be on ANY of the systems in these filters.");
                        panelSystem.setLayout(new BoxLayout(panelSystem, BoxLayout.Y_AXIS));

                        //---- comboBoxSystemFilter ----
                        comboBoxSystemFilter.addActionListener(e -> comboBoxSystemFilterActionPerformed());
                        panelSystem.add(comboBoxSystemFilter);
                        panelSystem.add(comboBoxAndSystem1);
                        panelSystem.add(comboBoxAndSystem2);
                        panelSystem.add(comboBoxAndSystem3);
                    }
                    gridPanel.add(panelSystem, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0,
                        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- buttonAddSystem ----
                    buttonAddSystem.setText("More");
                    buttonAddSystem.setMnemonic('M');
                    buttonAddSystem.addActionListener(e -> buttonAddSystemActionPerformed());
                    gridPanel.add(buttonAddSystem, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
                        GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelOtherSystem ----
                    labelOtherSystem.setText("Other System:");
                    gridPanel.add(labelOtherSystem, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- comboBoxOtherSystemFilter ----
                    comboBoxOtherSystemFilter.setToolTipText("Will return ANY game that is listed as an Other System on the selected platform. ");
                    gridPanel.add(comboBoxOtherSystemFilter, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelCoreAesthetic ----
                    labelCoreAesthetic.setText("Core Aesthetic:");
                    gridPanel.add(labelCoreAesthetic, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- comboBoxCoreAestheticFilter ----
                    comboBoxCoreAestheticFilter.setToolTipText("Will filter out any games that don't have the given parameter as the Core Aesthetic.");
                    gridPanel.add(comboBoxCoreAestheticFilter, new GridBagConstraints(3, 2, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelRepeatSystem ----
                    labelRepeatSystem.setText("Repeat System:");
                    gridPanel.add(labelRepeatSystem, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- comboBoxRepeatSystemFilter ----
                    comboBoxRepeatSystemFilter.setToolTipText("Will return ANY game that is listed as a Repeat System on the selected platform. ");
                    gridPanel.add(comboBoxRepeatSystemFilter, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelOtherAesthetic ----
                    labelOtherAesthetic.setText("Other Aesthetic:");
                    gridPanel.add(labelOtherAesthetic, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- comboBoxOtherAestheticFilter ----
                    comboBoxOtherAestheticFilter.setToolTipText("Will filter out any games that don't have the given parameter as the Other Aesthetic.");
                    gridPanel.add(comboBoxOtherAestheticFilter, new GridBagConstraints(3, 3, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelPlayers ----
                    labelPlayers.setText("Max Players:");
                    gridPanel.add(labelPlayers, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- textFieldPlayerFilter ----
                    textFieldPlayerFilter.setToolTipText("Will filter out any games that don't have a max player number higher than the given number.");
                    gridPanel.add(textFieldPlayerFilter, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelCompletionLevel ----
                    labelCompletionLevel.setText("Completion Level:");
                    gridPanel.add(labelCompletionLevel, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- comboBoxCompletionLevelFilter ----
                    comboBoxCompletionLevelFilter.setToolTipText("Will filter out any games that don't have the given parameter as the Completion Level.");
                    gridPanel.add(comboBoxCompletionLevelFilter, new GridBagConstraints(3, 4, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelDeveloper ----
                    labelDeveloper.setText("Developer:");
                    gridPanel.add(labelDeveloper, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- textFieldDeveloperFilter ----
                    textFieldDeveloperFilter.setToolTipText("Will filter out any games that don't have the given parameter in somewhere in the developer.");
                    gridPanel.add(textFieldDeveloperFilter, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelRating ----
                    labelRating.setText("Rating:");
                    gridPanel.add(labelRating, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- comboBoxRatingFilter ----
                    comboBoxRatingFilter.setToolTipText("Will filter out any games that don't have the given parameter as the rating.");
                    gridPanel.add(comboBoxRatingFilter, new GridBagConstraints(3, 5, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelPublisher ----
                    labelPublisher.setText("Publisher:");
                    gridPanel.add(labelPublisher, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- textFieldPublisherFilter ----
                    textFieldPublisherFilter.setToolTipText("Will filter out any games that don't have the given parameter in somewhere in the publisher.");
                    gridPanel.add(textFieldPublisherFilter, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelThoughtsType ----
                    labelThoughtsType.setText("Thoughts Type:");
                    gridPanel.add(labelThoughtsType, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- comboBoxThoughtsTypeFilter ----
                    comboBoxThoughtsTypeFilter.setToolTipText("Will filter out any games that don't have the given parameter as the thoughts type.");
                    gridPanel.add(comboBoxThoughtsTypeFilter, new GridBagConstraints(3, 6, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelOriginallyReleased ----
                    labelOriginallyReleased.setText("Originally Released:");
                    gridPanel.add(labelOriginallyReleased, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- textFieldOriginallyReleasedFilter ----
                    textFieldOriginallyReleasedFilter.setToolTipText("Will filter out any games that don't have the given parameter that match the Originally Released Year.");
                    gridPanel.add(textFieldOriginallyReleasedFilter, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- checkBoxUseRevisitFilter ----
                    checkBoxUseRevisitFilter.setText("Use Filter");
                    gridPanel.add(checkBoxUseRevisitFilter, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- checkBoxRevisit ----
                    checkBoxRevisit.setText("Should Revisit");
                    gridPanel.add(checkBoxRevisit, new GridBagConstraints(3, 7, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelRereleased ----
                    labelRereleased.setText("Rereleased:");
                    gridPanel.add(labelRereleased, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- textFieldReReleasedFilter ----
                    textFieldReReleasedFilter.setToolTipText("Will filter out any games that don't match the given parameters rereleased year.");
                    gridPanel.add(textFieldReReleasedFilter, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- checkBoxUseCouchFilter ----
                    checkBoxUseCouchFilter.setText("Use Filter");
                    gridPanel.add(checkBoxUseCouchFilter, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- checkBoxCouch ----
                    checkBoxCouch.setText("Excellent Local Multiplayer");
                    gridPanel.add(checkBoxCouch, new GridBagConstraints(3, 8, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelGenre ----
                    labelGenre.setText("Genre:");
                    gridPanel.add(labelGenre, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- comboBoxGenre ----
                    comboBoxGenre.setToolTipText("Will filter out any games that don't have the given parameter as the genre.");
                    gridPanel.add(comboBoxGenre, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- checkBoxUseHallOfFameFilter ----
                    checkBoxUseHallOfFameFilter.setText("Use Filter");
                    gridPanel.add(checkBoxUseHallOfFameFilter, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- checkBoxHallOfFame ----
                    checkBoxHallOfFame.setText("Hall of Fame");
                    gridPanel.add(checkBoxHallOfFame, new GridBagConstraints(3, 9, 2, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 5, 5), 0, 0));

                    //---- labelSeries ----
                    labelSeries.setText("Series:");
                    gridPanel.add(labelSeries, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));

                    //---- textFieldSeries ----
                    textFieldSeries.setToolTipText("Will filter out any games that don't have the given parameter in somewhere in the series.");
                    gridPanel.add(textFieldSeries, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));

                    //---- checkBoxSetRepeatSystem ----
                    checkBoxSetRepeatSystem.setText("Set Repeat System When System is Set");
                    checkBoxSetRepeatSystem.setToolTipText("Automatically sets the Repeat System filter to the match the first System filter.");
                    checkBoxSetRepeatSystem.setSelected(true);
                    gridPanel.add(checkBoxSetRepeatSystem, new GridBagConstraints(2, 10, 3, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));
                }
                contentPanel.add(gridPanel, BorderLayout.CENTER);
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 0, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0};

                //---- searchButton ----
                searchButton.setText("Search");
                searchButton.setMnemonic('A');
                searchButton.addActionListener(e -> searchButtonActionPerformed());
                buttonBar.add(searchButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- clearButton ----
                clearButton.setText("Clear Filters");
                clearButton.setMnemonic('R');
                clearButton.addActionListener(e -> clearButtonActionPerformed());
                buttonBar.add(clearButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.setMnemonic('C');
                cancelButton.addActionListener(e -> cancelButtonActionPerformed());
                buttonBar.add(cancelButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        setSize(650, 515);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner non-commercial license
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JPanel gridPanel;
    private JLabel labelTitle;
    private JTextField textFieldTitleFilter;
    private JLabel labelSystem;
    private JPanel panelSystem;
    private JComboBox comboBoxSystemFilter;
    private JComboBox comboBoxAndSystem1;
    private JComboBox comboBoxAndSystem2;
    private JComboBox comboBoxAndSystem3;
    private JButton buttonAddSystem;
    private JLabel labelOtherSystem;
    private JComboBox comboBoxOtherSystemFilter;
    private JLabel labelCoreAesthetic;
    private JComboBox comboBoxCoreAestheticFilter;
    private JLabel labelRepeatSystem;
    private JComboBox comboBoxRepeatSystemFilter;
    private JLabel labelOtherAesthetic;
    private JComboBox comboBoxOtherAestheticFilter;
    private JLabel labelPlayers;
    private JTextField textFieldPlayerFilter;
    private JLabel labelCompletionLevel;
    private JComboBox comboBoxCompletionLevelFilter;
    private JLabel labelDeveloper;
    private JTextField textFieldDeveloperFilter;
    private JLabel labelRating;
    private JComboBox comboBoxRatingFilter;
    private JLabel labelPublisher;
    private JTextField textFieldPublisherFilter;
    private JLabel labelThoughtsType;
    private JComboBox comboBoxThoughtsTypeFilter;
    private JLabel labelOriginallyReleased;
    private JTextField textFieldOriginallyReleasedFilter;
    private JCheckBox checkBoxUseRevisitFilter;
    private JCheckBox checkBoxRevisit;
    private JLabel labelRereleased;
    private JTextField textFieldReReleasedFilter;
    private JCheckBox checkBoxUseCouchFilter;
    private JCheckBox checkBoxCouch;
    private JLabel labelGenre;
    private JComboBox comboBoxGenre;
    private JCheckBox checkBoxUseHallOfFameFilter;
    private JCheckBox checkBoxHallOfFame;
    private JLabel labelSeries;
    private JTextField textFieldSeries;
    private JCheckBox checkBoxSetRepeatSystem;
    private JPanel buttonBar;
    private JButton searchButton;
    private JButton clearButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
