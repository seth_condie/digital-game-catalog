package controller;

/**
 * The Globals Class
 * This class will hold all of the constants that are used in the program
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public final class Globals {

    public static final int CURRENT_VERSION = 6;

    public static final int MISSINGNO = -1;
    public static final String NONE = "none";
    public static final String NEW_GENRE = "NEW_GENRE";
    public static final int ADVANCE_SEARCH_LOCATION_SEARCH = 11;
    public static final int ADVANCE_SEARCH_LOCATION_CANCEL = 12;
    public static final int ADVANCE_SEARCH_LOCATION_REPORT = 13;
    public static final int ADVANCE_SEARCH_LOCATION_ERROR = 14;
    public static final String NO_FILTER = "No Filter";
    public static final String SEARCH_RESULTS = "Search Results: ";
    public static final int NO_NUMBER_FILTER = -2;

    //the headers are missing the comments and the thoughts link
    public static final String VIDEO_GAME_REPORT_HEADERS = "Game Title\t\tSystem\t\tOther Systems\tRepeat Systems\tPlayers\tDeveloper\tPublisher\tRelease Year\tReRelease Years\tGenre\t\tSeries\t\t" +
            "Core Aesthetic\tOther Aesthetic\tRecommended For\tCompletion Level\tRating\t\tThoughts Type\tShould Revisit\tGreat Local Co-op\tHall Of Fame";
    public static final String TOY_REPORT_HEADERS = "Name\t\t\tSet\t\t\tPlatform";

    public static final class Aesthetic {
        public static final String CHALLENGE = "Challenge";
        public static final String CHALLENGE_DESC = "Game as obstacle course";

        public static final String SENSATION = "Sensation";
        public static final String SENSATION_DESC = "Game as Sense Pleasure (Nostalgia)";

        public static final String EXPRESSION = "Expression";
        public static final String EXPRESSION_DESC = "Game as self-discovery";

        public static final String NARRATIVE = "Narrative";
        public static final String NARRATIVE_DESC = "Game as drama";

        public static final String FANTASY = "Fantasy";
        public static final String FANTASY_DESC = "Game as make believe";

        public static final String COMPETITION = "Competition";
        public static final String COMPETITION_DESC = "Game as expression of dominance";

        public static final String FELLOWSHIP = "Fellowship";
        public static final String FELLOWSHIP_DESC = "Game as social framework";

        public static final String DISCOVERY = "Discovery";
        public static final String DISCOVERY_DESC = "Game as uncharted territory";

        public static final String ABNEGATION = "Abnegation";
        public static final String ABNEGATION_DESC = "Game as past time";

        public static final String THINKING = "Thinking";
        public static final String THINKING_DESC = "Game as Mental Exercise";
    }

    public static final class CompletionLevel {
        public static final String WANT = "Want it";
        public static final String BOUGHT = "Bought it";
        public static final String PLAY = "Played it";
        public static final String PLAYING = "Currently Playing";
        public static final String FINISH = "Finished it";
        public static final String COMPLETE = "Completed it";
    }

    public static final class Rating {
        public static final String SATISFIED = "Satisfied";
        public static final String CONTENT = "Content";
        public static final String DISCONTENT = "Discontent";
        public static final String BACKLOGGED = "Backlogged";
    }

    public static final class ThoughtsType {
        public static final String FULL_REVIEW = "Full Review";
        public static final String NOTES = "Notes";
        public static final String MEMORIES = "Memories";
        public static final String PLACEHOLDER = "Placeholder";
    }
}
