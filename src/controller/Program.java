package controller;

import controller.objectClasses.AdvancedSearchFilters;
import controller.objectClasses.System;
import controller.objectClasses.Toy;
import controller.objectClasses.Video_Game;
import model.CreateDBTables;
import model.InsertDefaultInfo;
import model.SearchQueries;
import model.UpdateQueries;
import view.DigitalGameCatGUI;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.*;

/**
 * The Program Class:
 * This is the class that separates the gui from the database and it's calls
 * all of the go between code can be found here including the Main method
 *
 * @author Seth Condie
 * @version 6
 * @date 7/4/2016
 */
public class Program {

    private static final Logger LOGGER = Logger.getLogger(Program.class.getName());
    private static final String ERROR_LOG = "errorLog.txt";
    private static final String LAST_FILE_OPENED = "lastFileOpen.txt";

    private Connection connection;
    private DigitalGameCatGUI gui;
    private List<Video_Game> searchResultsGameList;
    private List<System> searchResultsSystemsList = new ArrayList<>();
    private List<String> searchResultsGenresList = new ArrayList<>();
    private List<Toy> searchResultsToyList = new ArrayList<>();
    private List<Video_Game> gameCache = new ArrayList<>();
    private boolean lastSearchIsAdvancedSearch = false;
    private AdvancedSearchFilters lastAdvancedSearch;

    private static Map<String, Integer> systemMap = new HashMap<>();
    private static Map<String, Integer> genreMap = new HashMap<>();
    private static Map<String, Integer> toyMap = new HashMap<>();

    public static void main(String args[]) {
        Program program = new Program();
    }

    public Program() {
        setupLogger();
        gui = new DigitalGameCatGUI(this);
        gui.setupAestheticComboBoxes();
        gui.setupRatingComboBox();
        gui.setupCompletionLevelComboBox();
        gui.setupThoughtsTypeComboBox();
        openLastFile();
    }

    public boolean openDatabase(String filePath) {
        String fileType = filePath.substring(filePath.length() - 4);
        if (!fileType.equals(".dgc")) {
            gui.displayErrorDialog("This file type is not supported please use a .dgc file type", "File not supported");
            return false;
        }
        try {
            //the Class.forName returns a class but that class is not used in this case instead the Class.forName is used to setup the JDBC driver.
            Class setupDriver = Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + filePath);
        } catch (Exception e) {
            logAndDisplaySevere("Error when setting up a connection to the database");
            logAndDisplaySevere(stackToString(e));
            return false;
        }
        logAndDisplayInfo("Database opened successfully");
        setLastFile(filePath);
        return true;
    }

    private void insertDefaultData() {
        try {
            InsertDefaultInfo.insertDefaultGenres(connection);
            InsertDefaultInfo.insertDefaultSystems(connection);
            InsertDefaultInfo.insertExampleData(connection);
            logAndDisplayInfo("Default data entered successfully");
        } catch (SQLException e) {
            logAndDisplaySevere("Error when inserting data into tables: ");
            logAndDisplaySevere(stackToString(e));
        }
    }

    private void createTables() {
        try {
            CreateDBTables.createTables(connection);
            CreateDBTables.updateDb_v2(connection);
            CreateDBTables.updateDb_v3(connection);
            CreateDBTables.updateDb_v4(connection);
            CreateDBTables.updateDb_v5(connection);
            logAndDisplayInfo("Tables created successfully");
        } catch (SQLException e) {
            logAndDisplaySevere("ERROR when creating tables: ");
            logAndDisplaySevere(stackToString(e));
        }
    }

    private void buildGamesCache() {
        try {
            gameCache = SearchQueries.videoGameSearch(connection, "", -1, -1, false);
        } catch (SQLException e) {
            logAndDisplaySevere("Error when updating the games cache: ");
            logAndDisplaySevere(stackToString(e));
        }
    }

    private void buildGenreMap() {
        try {
            genreMap = SearchQueries.findAllGenresMap(connection);
            List<String> genreListAll = new ArrayList(genreMap.keySet());
            Collections.sort(genreListAll);
            logAndDisplayInfo("genreMap updated");
            gui.setupGenreComboBoxes(genreListAll);
        } catch (SQLException e) {
            logAndDisplaySevere("Error when updating the genreMap from the database: ");
            logAndDisplaySevere(stackToString(e));
        }
    }

    private void buildSystemMap() {
        try {
            systemMap = SearchQueries.findAllSystemsMap(connection);
            List<String> systemListAll = new ArrayList(systemMap.keySet());
            Collections.sort(systemListAll);
            logAndDisplayInfo("systemMap updated");
            gui.setupSystemComboBoxes(systemListAll);
        } catch (SQLException e) {
            logAndDisplaySevere("Error when updating the systemMap from the database: ");
            logAndDisplaySevere(stackToString(e));
        }
    }

    private void buildToysMap() {
        try {
            toyMap = SearchQueries.finalAllToysMap(connection);
            logAndDisplayInfo("toyMap updated");
        } catch (SQLException e) {
            logAndDisplaySevere("Error when updating the toyMap from the database: ");
            logAndDisplaySevere(stackToString(e));
        }
    }

    public void deleteGameFromDatabase(int gameId) {
        try {
            UpdateQueries.deleteGameFromDb(gameId, connection);
            logAndDisplayInfo("Game deleted from database");
            buildGamesCache();
            runLastSearch();
            gui.updateOpenGameId(Globals.MISSINGNO);
        } catch (SQLException e) {
            gui.addToDebugPanel("Error when deleting a game from the database");
            logAndDisplaySevere(stackToString(e));
        }
    }

    public boolean deleteSystemFromDatabase(int systemId) {
        try {
            if (SearchQueries.canDeleteSystemFromDb(systemId, connection)) {
                UpdateQueries.deleteSystemFromDb(systemId, connection);
                buildSystemMap();
                logAndDisplayInfo("System deleted from database");
                gui.buttonSystemSearchActionPerformed();
                gui.updateOpenSystemId(Globals.MISSINGNO);
                return true;
            } else {
                gui.displayErrorDialog("System in use in database, please remove any games using this system before deleting", "System In Use Error");
                return false;
            }
        } catch (SQLException e) {
            gui.addToDebugPanel("Error trying to delete a system from the database");
            logAndDisplaySevere(stackToString(e));
            return false;
        }
    }

    public boolean deleteToyFromDatabase(int toyId) {
        try {
            UpdateQueries.deleteToyFromDb(toyId, connection);
            buildToysMap();
            logAndDisplayInfo("Toy deleted from database");
            gui.buttonToysSearchActionPerformed();
            gui.updateOpenToyId(Globals.MISSINGNO);
            return true;
        } catch (SQLException e) {
            gui.addToDebugPanel("Error trying to delete a toy from the database");
            logAndDisplaySevere(stackToString(e));
            return false;
        }
    }

    public boolean deleteGenreFromDatabase(String deleteGenre) {
        int deleteGenreId = genreMap.get(deleteGenre);
        try {
            if (SearchQueries.canDeleteGenreFromDb(deleteGenreId, connection)) {
                UpdateQueries.deleteGenreFromDb(deleteGenre, connection);
                buildGenreMap();
                logAndDisplayInfo(deleteGenre + " deleted from the database");
                gui.buttonGenreSearchActionPerformed();
                return true;
            } else {
                gui.displayErrorDialog("Genre in use in database, please remove any game using this genre before deleting", "Genre In Use Error");
            }
        } catch (SQLException e) {
            gui.addToDebugPanel("Error trying to delete a genre from the database");
            logAndDisplaySevere(stackToString(e));
        }
        return false;
    }

    public boolean editDatabaseWithGame(Video_Game newGame) {

        if (newGame.getId() < 0) {
            try {
                if (SearchQueries.canAddNewGameToDb(newGame.getTitle(), connection)) {
                    int newId = UpdateQueries.addNewGameToDb(newGame, connection);
                    logAndDisplayInfo(newGame.getTitle() + " successfully added to the database");
                    gui.updateOpenGameId(newId);
                    buildGamesCache();
                } else {
                    gui.displayPlainDialog("Please use a unique title for the game you are entering into the database", "Game Title Not Unique");
                    return false;
                }
            } catch (SQLException e) {
                gui.addToDebugPanel("Error, trouble when saving new game to database");
                logAndDisplaySevere(stackToString(e));
                return false;
            }
        } else {
            try {
                UpdateQueries.updateExistingGameInDb(newGame, connection);
                logAndDisplayInfo(newGame.getTitle() + " successfully updated in database");
                buildGamesCache();
            } catch (SQLException e) {
                gui.addToDebugPanel("Error, trouble updating an existing game to database");
                logAndDisplaySevere(stackToString(e));
                return false;
            }
        }
        runLastSearch();
        return true;
    }

    public boolean editDatabaseWithSystem(System newSystem) {
        boolean success = false;
        if (newSystem.getId() < 0) {
            try {
                if (systemMap.get(newSystem.getName()) == null || !newSystem.getName().equals("none")) {
                    int newId = UpdateQueries.addNewSystemToDb(newSystem, connection);
                    gui.updateOpenSystemId(newId);
                    logAndDisplayInfo(newSystem.getName() + " successfully added to database");
                    buildSystemMap();
                    success = true;
                } else {
                    gui.displayErrorDialog("This system is already included", "System already found");
                }
            } catch (SQLException e) {
                gui.addToDebugPanel("Error, trouble when saving a new system to database");
                logAndDisplaySevere(stackToString(e));
            }
        } else {
            try {
                UpdateQueries.updateExistingSystemInDb(newSystem, connection);
                logAndDisplayInfo(newSystem.getName() + " successfully updated in database");
                buildSystemMap();
                success = true;
            } catch (SQLException e) {
                gui.addToDebugPanel("Error, trouble when updating an existing game to database");
                logAndDisplaySevere(stackToString(e));
            }
        }
        gui.buttonSystemSearchActionPerformed();
        return success;
    }

    public boolean editDatabaseWithGenre(String newGenre, String openGenre) {
        boolean success = false;

        if (openGenre.equals(Globals.NEW_GENRE)) {
            try {
                if (genreMap.get(newGenre) == null || !newGenre.equals("none")) {
                    UpdateQueries.addNewGenreToDb(newGenre, connection);
                    logAndDisplayInfo(newGenre + " successfully added to database");
                    buildGenreMap();
                    success = true;
                } else {
                    gui.displayErrorDialog("This genre is already included", "Genre already found");
                }
            } catch (SQLException e) {
                gui.addToDebugPanel("Error, trouble when saving a new genre to database");
                logAndDisplaySevere(stackToString(e));
            }
        } else {
            try {
                UpdateQueries.updateExistingGenreInDb(newGenre, openGenre, connection);
                buildGenreMap();
                logAndDisplayInfo(newGenre + " successfully updated in database");
                success = true;
            } catch (SQLException e) {
                gui.addToDebugPanel("Error, trouble when updating an existing genre in the database");
                logAndDisplaySevere(stackToString(e));
            }
        }
        gui.buttonGenreSearchActionPerformed();
        return success;
    }

    public boolean editDatabaseWithToy(Toy newToy) {
        boolean success = false;

        if (newToy.getId() < 0) {
            try {
                if (toyMap.get(newToy.getName()) == null) {
                    int newId = UpdateQueries.addNewToyToDb(newToy, connection);
                    gui.updateOpenToyId(newId);
                    logAndDisplayInfo(newToy.getName() + " successfully added to database");
                    buildToysMap();
                    success = true;
                } else {
                    gui.displayErrorDialog("This Toy is already in the system", "Toy Already Found");
                }
            } catch (SQLException e) {
                gui.addToDebugPanel("Error, trouble when saving a new toy to the database");
                logAndDisplaySevere(stackToString(e));
            }
        } else {
            try {
                UpdateQueries.updateExistingToyInDb(newToy, connection);
                logAndDisplayInfo(newToy.getName() + " successfully updated in database");
                buildToysMap();
                success = true;
            } catch (SQLException e) {
                gui.addToDebugPanel("Error trouble when updating an existing game in the database");
                logAndDisplaySevere(stackToString(e));
            }
        }
        gui.buttonToysSearchActionPerformed();
        return success;
    }

    public void searchResultsVideoGames(String name, String system, String genre, boolean repeatSearch) {
        searchResultsGameList = null;
        int systemId = Globals.MISSINGNO;
        int genreId = Globals.MISSINGNO;
        if (!system.equals(Globals.NONE)) {
            systemId = systemMap.get(system);
        }
        if (!genre.equals(Globals.NONE)) {
            genreId = genreMap.get(genre);
        }
        try {
            searchResultsGameList = SearchQueries.videoGameSearch(connection, name, systemId, genreId, repeatSearch);
            lastSearchIsAdvancedSearch = false;
        } catch (SQLException e) {
            logAndDisplaySevere("Error when getting video game search results: ");
            logAndDisplaySevere(stackToString(e));
        }
        logAndDisplayInfo("Video game search pane updated");
        gui.displaySearchResults(searchResultsGameList);
    }

    public void advancedSearchResultsVideoGames(AdvancedSearchFilters filters) {
        searchResultsGameList = null;
        searchResultsGameList = videoGameAdvancedSearch(filters);
        lastAdvancedSearch = filters;
        lastSearchIsAdvancedSearch = true;
        logAndDisplayInfo("Video game search pane updated");
        gui.displaySearchResults(searchResultsGameList);
    }

    public void searchResultsSystems(String filter) {
        searchResultsSystemsList = null;
        try {
            searchResultsSystemsList = SearchQueries.systemSearch(connection, filter);
        } catch (SQLException e) {
            logAndDisplaySevere("Error when getting system search results: ");
            logAndDisplaySevere(stackToString(e));
        }
        List<String> displayList = new ArrayList<>();
        for (System s : searchResultsSystemsList) {
            displayList.add(s.getName());
        }
        logAndDisplayInfo("System search pane updated");
        gui.displaySystemSearchResults(displayList);
    }

    public void searchResultsToys(String nameFilter, String toySetFilter, String platformFilter) {
        searchResultsToyList = null;
        try {
            searchResultsToyList = SearchQueries.toySearch(connection, nameFilter, toySetFilter, platformFilter);
        } catch (SQLException e) {
            logAndDisplaySevere("Error when getting toy search results: ");
            logAndDisplaySevere(stackToString(e));
        }
        List<String> displayList = new ArrayList<>();
        for (Toy t : searchResultsToyList) {
            if (t.getToySet() != null && t.getToySet().length() > 0) {
                displayList.add(t.getName() + " - " + t.getToySet() + " Set");
            } else {
                displayList.add(t.getName());
            }
        }
        logAndDisplayInfo("Toy search pane updated");
        gui.displayToySearchResults(displayList);
    }

    public void searchMasterListReport() {
        List<Video_Game> reportResults = new ArrayList<>();
        try {
            reportResults = SearchQueries.runMasterListReportQuery(connection);
        } catch (SQLException e) {
            logAndDisplaySevere("Error when compiling report");
            logAndDisplaySevere(stackToString(e));
        }
        gui.displayVideoGameReportResults(reportResults);
    }

    public void searchFiltersReport(AdvancedSearchFilters filters) {
        List<Video_Game> reportResults = videoGameAdvancedSearch(filters);
        lastAdvancedSearch = filters;
        gui.displayVideoGameReportResults(reportResults);
    }

    public void searchCountReport() {
        List<String> systems = new ArrayList(systemMap.keySet());
        List<String> genres = new ArrayList(genreMap.keySet());


        try {
            gui.displayCountReportNextLine("----------Games by system----------");
            for (String system : systems) {
                int number = SearchQueries.systemCountQuery(connection, systemMap.get(system));
                if (number > 0) {
                    gui.displayCountReportNextLine(number + " " + system + " Games");
                    gui.displayCountReportNextLine("");
                    List<Video_Game> games = SearchQueries.videoGameSearch(connection, "", -1, -1, true);
                    for (Video_Game game : games) {
                        gui.displayCountReportNextLine(game.getTitle());
                    }
                    gui.displayCountReportNextLine("");
                }
            }

            gui.displayCountReportNextLine("----------Games by Genre----------");
            for (String genre : genres) {
                int number = SearchQueries.genreCountQuery(connection, genreMap.get(genre));
                if (number > 0) {
                    gui.displayCountReportNextLine(number + " " + genre + " Games");
                    gui.displayCountReportNextLine("");
                    List<Video_Game> games = SearchQueries.videoGameSearch(connection, "", -1, -1, false);
                    for (Video_Game game : games) {
                        gui.displayCountReportNextLine(game.getTitle());
                    }
                    gui.displayCountReportNextLine("");
                }
            }

        } catch (SQLException e) {
            logAndDisplaySevere("Error when compiling report");
            logAndDisplaySevere(stackToString(e));
        }
    }

    public void searchToyListReport() {
        List<Toy> reportResults = new ArrayList<>();
        try {
            reportResults = SearchQueries.runToyListReportQuery(connection);
        } catch (SQLException e) {
            logAndDisplaySevere("Error when compiling report");
            logAndDisplaySevere(stackToString(e));
        }
        gui.displayToyListReportResults(reportResults);
    }

    private List<Video_Game> videoGameAdvancedSearch(AdvancedSearchFilters filters) {
        //use a results toySet to store the current results
        //use a builder to build the current results from the previous results
        //with this you don't need to search through all of the video games each time
        //you only need to search the previous results as you go through the filters
        Set<Video_Game> results = new HashSet<>(gameCache);
        Set<Video_Game> builder = new HashSet<>();

        if (!filters.getTitle().equals(Globals.NO_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getTitle().toLowerCase().contains(filters.getTitle().toLowerCase())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (!(filters.getPlayerMax() == Globals.NO_NUMBER_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getPlayerMax() >= filters.getPlayerMax()) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        String developer = filters.getDeveloper();
        if (!developer.equals(Globals.NO_FILTER)) {
            builder.clear();
            if (developer.isEmpty()) {
                for (Video_Game game : results) {
                    if (game.getDeveloper().isEmpty()) {
                        builder.add(game);
                    }
                }
            } else {
                for (Video_Game game : results) {
                    if (game.getSeries().toLowerCase().contains(filters.getDeveloper().toLowerCase())) {
                        builder.add(game);
                    }
                }
            }
            results.retainAll(builder);
        }

        String publisher = filters.getPublisher();
        if (!publisher.equals(Globals.NO_FILTER)) {
            builder.clear();
            if (publisher.isEmpty()) {
                for (Video_Game game : results) {
                    if (game.getPublisher().isEmpty()) {
                        builder.add(game);
                    }
                }
            } else {
                for (Video_Game game : results) {
                    if (game.getSeries().toLowerCase().contains(filters.getPublisher().toLowerCase())) {
                        builder.add(game);
                    }
                }
            }
            results.retainAll(builder);
        }

        if (!(filters.getOriginallyReleased() == Globals.NO_NUMBER_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getOriginalReleaseYear() == filters.getOriginallyReleased()) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (!(filters.getReRelease() == Globals.NO_NUMBER_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                for (int reRelease : game.getReReleaseYears()) {
                    if (reRelease == filters.getReRelease()) {
                        builder.add(game);
                    }
                }
            }
            results.retainAll(builder);
        }

        if (!filters.getGenre().equals(Globals.NO_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getGenre().equals(filters.getGenre())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        String series = filters.getSeries();
        if (!series.equals(Globals.NO_FILTER)) {
            builder.clear();
            if (series.isEmpty()) {
                for (Video_Game game : results) {
                    if (game.getSeries().isEmpty()) {
                        builder.add(game);
                    }
                }
            } else {
                for (Video_Game game : results) {
                    if (game.getSeries().toLowerCase().contains(filters.getSeries().toLowerCase())) {
                        builder.add(game);
                    }
                }
            }
            results.retainAll(builder);
        }

        if (!filters.getCoreAesthetic().equals(Globals.NO_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getCoreAesthetic().equals(filters.getCoreAesthetic())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (!filters.getOtherAesthetic().equals(Globals.NO_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getOtherAesthetic().equals(filters.getOtherAesthetic())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (!filters.getCompletionLevel().equals(Globals.NO_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getCompletionLevel().equals(filters.getCompletionLevel())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (!filters.getRating().equals(Globals.NO_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getRating().equals(filters.getRating())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (!filters.getThoughtsType().equals(Globals.NO_FILTER)) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.getThoughtsType().equals(filters.getThoughtsType())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (filters.isUseRevisitFilter()) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.isShouldRevisit() == (filters.isRevisitFilter())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (filters.isUseCouchFilter()) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.isLocalCouch() == (filters.isCouchFilter())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        if (filters.isUseHallOfFameFilter()) {
            builder.clear();
            for (Video_Game game : results) {
                if (game.isHallOfFame() == (filters.isHallOfFameFilter())) {
                    builder.add(game);
                }
            }
            results.retainAll(builder);
        }

        results = MultiSystemAdvancedSearch(results, filters);

        List<Video_Game> sendBack = new ArrayList<>(results);
        Collections.sort(sendBack);
        return sendBack;
    }

    private Set<Video_Game> MultiSystemAdvancedSearch(Set<Video_Game> results, AdvancedSearchFilters filters) {
        Set<Video_Game> builder = new HashSet<>();
        boolean filtered = false;

        //here don't clear the builder just keep adding to the builder toySet and then return the builder as the new results toySet.
        if (!filters.getSystem().equals(Globals.NO_FILTER)) {
            for (Video_Game game : results) {
                if (game.getSystem().equals(filters.getSystem())) {
                    builder.add(game);
                }
            }
            filtered = true;
        }

        if (!filters.getAndSystem1().equals(Globals.NO_FILTER)) {
            for (Video_Game game : results) {
                if (game.getSystem().equals(filters.getAndSystem1())) {
                    builder.add(game);
                }
            }
            filtered = true;
        }

        if (!filters.getAndSystem2().equals(Globals.NO_FILTER)) {
            for (Video_Game game : results) {
                if (game.getSystem().equals(filters.getAndSystem2())) {
                    builder.add(game);
                }
            }
            filtered = true;
        }

        if (!filters.getAndSystem3().equals(Globals.NO_FILTER)) {
            for (Video_Game game : results) {
                if (game.getSystem().equals(filters.getAndSystem3())) {
                    builder.add(game);
                }
            }
            filtered = true;
        }

        if (!filters.getOtherSystem().equals(Globals.NO_FILTER)) {
            for (Video_Game game : results) {
                for (String otherSystem : game.getOtherSystems()) {
                    if (otherSystem.equals(filters.getOtherSystem())) {
                        builder.add(game);
                    }
                }
            }
            filtered = true;
        }

        if (!filters.getRepeatSystem().equals(Globals.NO_FILTER)) {
            for (Video_Game game : results) {
                for (String repeatSystem : game.getRepeatSystems()) {
                    if (repeatSystem.equals(filters.getRepeatSystem())) {
                        builder.add(game);
                    }
                }
            }
            filtered = true;
        }

        if (filtered) {
           results.retainAll(builder);
        }

        return results;
    }

    public void createNewDatabase(String name, boolean addDefaultData) {
        if (openDatabase(name)) {
            createTables();
            if (addDefaultData) {
                insertDefaultData();
            }
            databaseOpenSuccessfully();
        } else {
            gui.displayErrorDialog("Couldn't setup connection to new database.", "Database Creation Error");
        }
    }

    public void databaseOpenSuccessfully() {
        int dbVersion;
        try {
            dbVersion = SearchQueries.verifyData(connection);
            if (dbVersion == Globals.MISSINGNO) {
                logAndDisplaySevere("Database couldn't be verified, it could be corrupt");
                gui.displayErrorDialog("This database cannot be verified, it could be corrupt, please try another database file", "Database Not Validated");
                return;
            }
        } catch (SQLException e) {
            logAndDisplaySevere("Database couldn't be verified, it could be corrupt: ");
            logAndDisplaySevere(stackToString(e));
            gui.displayErrorDialog("This database cannot be verified, it could be corrupt, please try another database file", "Database Not Validated");
            return;
        }
        if (dbVersion < Globals.CURRENT_VERSION) {
            gui.displayPlainDialog("The database will now be updated, this may take a while to complete", "Database Update");
            try {
                switch (dbVersion) {
                    case 1:
                        CreateDBTables.updateDb_v2(connection);
                    case 2:
                        CreateDBTables.updateDb_v3(connection);
                    case 3:
                        CreateDBTables.updateDb_v4(connection);
                    case 4:
                        CreateDBTables.updateDb_v5(connection);
                    case 5:
                        CreateDBTables.updateDb_v6(connection);
                        logAndDisplayInfo("Database has been updated");
                        break;
                }
            } catch (SQLException e) {
                logAndDisplaySevere("Database couldn't be updated");
                logAndDisplaySevere(stackToString(e));
                return;
            }
        } else if (dbVersion > Globals.CURRENT_VERSION) {
            gui.displayErrorDialog("The database is for a newer version of this software please update your software", "Program is not up to date");
            return;
        } else {
            logAndDisplayInfo("The database is up to date");
        }
        buildGenreMap();
        buildSystemMap();
        buildToysMap();
        buildGamesCache();
        searchResultsSystems("");
        searchResultsGenre("");
        searchResultsVideoGames("", Globals.NONE, Globals.NONE, false);
        searchResultsToys("", "", "");
        gui.clearSystemGenreBasicSearch();
        gui.connectionGUISetup();
    }

    public void searchResultsGenre(String filter) {
        searchResultsGenresList.clear();
        List<String> genresMasterList = new ArrayList<>();
        filter = filter.toLowerCase();
        genresMasterList.addAll(genreMap.keySet());
        for (String genre : genresMasterList) {
            String genreTest = genre.toLowerCase();
            if (genreTest.contains(filter)) {
                searchResultsGenresList.add(genre);
            }
        }
        logAndDisplayInfo("Genre search pane updated");
        gui.displayGenreSearchResults(searchResultsGenresList);
    }

    public void openGameFromSearchResults(int index) {
        Video_Game openGame = searchResultsGameList.get(index);
        logAndDisplayInfo(openGame.getTitle() + " retrieved from database");
        gui.displayOpenedGame(openGame);
    }

    public void openSystemFromSearchResults(int index) {
        System openSystem = searchResultsSystemsList.get(index);
        logAndDisplayInfo(openSystem.getName() + " retrieved from search results");
        gui.displayOpenedSystem(openSystem);
    }

    public void openGenreFromSearchResults(int index) {
        String openGenre = searchResultsGenresList.get(index);
        logAndDisplayInfo(openGenre + " retrieved from search results");
        gui.displayOpenedGenre(openGenre);
    }

    public void openToyFromSearchResults(int index) {
        Toy openToy = searchResultsToyList.get(index);
        logAndDisplayInfo(openToy.getName() + " retrieved from search results");
        gui.displayOpenedToy(openToy);
    }

    public int getVersion() {
        return Globals.CURRENT_VERSION;
    }

    private void openLastFile() {
        String lastFileOpened;
        String notFound = "not found";
        String notRead = "not read";

        try (BufferedReader reader = new BufferedReader(new FileReader(LAST_FILE_OPENED))) {
            lastFileOpened = reader.readLine();
        } catch (FileNotFoundException e) {
            logAndDisplayInfo("Couldn't find " + LAST_FILE_OPENED);
            logAndDisplayInfo(stackToString(e));
            lastFileOpened = notFound;
        } catch (IOException e) {
            logAndDisplayInfo("Couldn't read from " + LAST_FILE_OPENED);
            logAndDisplayInfo(stackToString(e));
            lastFileOpened = notRead;
        }

        if (lastFileOpened.equals(notFound)) {
            gui.displayPlainDialog("Couldn't find the last loaded database text file.  Please manually load or create a new database", "Open Last File Error");
        } else if (lastFileOpened.equals(notRead)) {
            gui.displayPlainDialog("Couldn't load a database from the last loaded text file.  Please manually load or create a new database", "Open Last File Error");
        } else {
            if (openDatabase(lastFileOpened)) {
                databaseOpenSuccessfully();
            } else {
                gui.displayPlainDialog("Couldn't setup a connection to the database.  Please manually load or create a new database", "Open Last File Error");
            }
        }
    }

    public void runLastSearch() {
        if (lastSearchIsAdvancedSearch) {
            if (lastAdvancedSearch != null) {
                advancedSearchResultsVideoGames(lastAdvancedSearch);
            }
        } else {
            gui.buttonSearchActionPerformed();
        }
    }

    private void setLastFile(String filePath) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(LAST_FILE_OPENED))) {
            writer.write(filePath);
            logAndDisplayInfo("Last open file updated");
        } catch (IOException e) {
            logAndDisplaySevere("Error updating the last open file");
            logAndDisplaySevere(stackToString(e));
        }
    }

    private void logAndDisplayInfo(String info) {
        gui.addToDebugPanel(info);
        LOGGER.info(info);
    }

    private void logAndDisplaySevere(String info) {
        gui.addToDebugPanel(info);
        LOGGER.severe(info);
    }

    private void setupLogger() {
        //setup the error logger that will write to a file
        FileHandler fileHandler = null;
        try {
            fileHandler = new FileHandler(ERROR_LOG);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.severe(e.toString());
        }
        fileHandler.setFormatter(new SimpleFormatter());
        fileHandler.setFilter(new Filter() {
            //when the logger's message is SEVERE or higher it will make it past the filter.
            @Override
            public boolean isLoggable(LogRecord record) {
                return record.getLevel().equals(Level.SEVERE);
            }
        });
        LOGGER.addHandler(fileHandler);
    }

    public AdvancedSearchFilters getLastAdvancedSearchFilters() {
        return lastAdvancedSearch;
    }

    public static int getSystemId(String system) {
        return systemMap.get(system);
    }

    public static int getGenreId(String genre) {
        return genreMap.get(genre);
    }

    private String stackToString(Exception e) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        e.printStackTrace(printWriter);
        return stringWriter.toString();
    }
}
