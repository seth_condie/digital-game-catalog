package controller.objectClasses;

/**
 * The Toy Class
 * <p>
 * This class holds the object information for the Toy object used in the program
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public class Toy {
    int id;
    String name;
    String toySet;
    String platform;

    public Toy(int id, String name, String toySet, String platform) {
        this.id = id;
        this.name = name;
        this.toySet = toySet;
        this.platform = platform;
    }

    public void setName(String newName) {
        name = newName;
    }

    public void setPlatform(String newPlatform) {
        platform = newPlatform;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlatform() {
        return platform;
    }

    public String getToySet() {
        return toySet;
    }

    public void setToySet(String toySet) {
        this.toySet = toySet;
    }

    public String toReportString() {
        String display;
        int threeIndCheck = 23;
        int threeSubString = 20;
        int twoIndCheck = 15;
        int oneIndCheck = 7;

        if (name.length() > threeIndCheck) {
            String trucName = name.substring(0, threeSubString) + "...\t";
            display = trucName;
        } else if (name.length() > twoIndCheck) {
            display = name + "\t";
        } else if (name.length() > oneIndCheck) {
            display = name + "\t\t";
        } else {
            display = name + "\t\t\t";
        }

        if (toySet == null) {
            display += "\t\t\t";
        } else if (toySet.length() > threeIndCheck) {
            String trucSet = toySet.substring(0, threeSubString) + "...\t";
            display += trucSet;
        } else if (toySet.length() > twoIndCheck) {
            display += toySet + "\t";
        } else if (toySet.length() > oneIndCheck) {
            display += toySet + "\t\t";
        } else {
            display += toySet + "\t\t\t";
        }

        display += platform;

        return display;
    }
}
