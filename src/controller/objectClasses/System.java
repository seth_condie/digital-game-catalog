package controller.objectClasses;

/**
 * The System Class
 * This details the system object
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public class System {
    int id;
    String name;
    int generation;
    boolean handheld;

    public System(int id, String name, int generation, boolean handheld) {
        this.id = id;
        this.name = name;
        this.generation = generation;
        this.handheld = handheld;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getGeneration() {
        return generation;
    }

    public boolean isHandheld() {
        return handheld;
    }
}
