package controller.objectClasses;

import controller.Globals;

/**
 * AdvancedSearchFilters class
 * This is the object that holds all of the filters
 * for an advanced search
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public class AdvancedSearchFilters {

    private String title;
    private String system;
    private String andSystem1;
    private String andSystem2;
    private String andSystem3;
    private String otherSystem;
    private String repeatSystem;
    private int playerMax;
    private String developer;
    private String publisher;
    private int originallyReleased;
    private int reRelease;
    private String genre;
    private String series;
    private String coreAesthetic;
    private String otherAesthetic;
    private String completionLevel;
    private String rating;
    private String thoughtsType;
    private boolean useRevisitFilter;
    private boolean revisitFilter;
    private boolean useCouchFilter;
    private boolean couchFilter;
    private boolean useHallOfFameFilter;
    private boolean hallOfFameFilter;

    public AdvancedSearchFilters() {
        title = null;
        system = null;
        andSystem1 = null;
        andSystem2 = null;
        andSystem3 = null;
        otherSystem = null;
        repeatSystem = null;
        playerMax = Globals.MISSINGNO;
        developer = null;
        originallyReleased = Globals.MISSINGNO;
        reRelease = Globals.MISSINGNO;
        genre = null;
        coreAesthetic = null;
        otherAesthetic = null;
        completionLevel = null;
        rating = null;
        thoughtsType = null;
        revisitFilter = false;
        couchFilter = false;
        hallOfFameFilter = false;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getAndSystem1() {
        return andSystem1;
    }

    public void setAndSystem1(String andSystem1) {
        this.andSystem1 = andSystem1;
    }

    public String getAndSystem2() {
        return andSystem2;
    }

    public void setAndSystem2(String andSystem2) {
        this.andSystem2 = andSystem2;
    }

    public String getAndSystem3() {
        return andSystem3;
    }

    public void setAndSystem3(String andSystem3) {
        this.andSystem3 = andSystem3;
    }

    public String getOtherSystem() {
        return otherSystem;
    }

    public void setOtherSystem(String otherSystem) {
        this.otherSystem = otherSystem;
    }

    public String getRepeatSystem() {
        return repeatSystem;
    }

    public void setRepeatSystem(String repeatSystem) {
        this.repeatSystem = repeatSystem;
    }

    public int getPlayerMax() {
        return playerMax;
    }

    public void setPlayerMax(int playerMax) {
        this.playerMax = playerMax;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getOriginallyReleased() {
        return originallyReleased;
    }

    public void setOriginallyReleased(int originallyReleased) {
        this.originallyReleased = originallyReleased;
    }

    public int getReRelease() {
        return reRelease;
    }

    public void setReRelease(int reRelease) {
        this.reRelease = reRelease;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getCoreAesthetic() {
        return coreAesthetic;
    }

    public void setCoreAesthetic(String coreAesthetic) {
        this.coreAesthetic = coreAesthetic;
    }

    public String getOtherAesthetic() {
        return otherAesthetic;
    }

    public void setOtherAesthetic(String otherAesthetic) {
        this.otherAesthetic = otherAesthetic;
    }

    public String getCompletionLevel() {
        return completionLevel;
    }

    public void setCompletionLevel(String completionLevel) {
        this.completionLevel = completionLevel;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getThoughtsType() {
        return thoughtsType;
    }

    public void setThoughtsType(String thoughtsType) {
        this.thoughtsType = thoughtsType;
    }

    public boolean isUseRevisitFilter() {
        return useRevisitFilter;
    }

    public void setUseRevisitFilter(boolean useRevisitFilter) {
        this.useRevisitFilter = useRevisitFilter;
    }

    public boolean isRevisitFilter() {
        return revisitFilter;
    }

    public void setRevisitFilter(boolean revisitFilter) {
        this.revisitFilter = revisitFilter;
    }

    public boolean isUseCouchFilter() {
        return useCouchFilter;
    }

    public void setUseCouchFilter(boolean useCouchFilter) {
        this.useCouchFilter = useCouchFilter;
    }

    public boolean isCouchFilter() {
        return couchFilter;
    }

    public void setCouchFilter(boolean couchFilter) {
        this.couchFilter = couchFilter;
    }

    public boolean isUseHallOfFameFilter() {
        return useHallOfFameFilter;
    }

    public void setUseHallOfFameFilter(boolean useHallOfFameFilter) {
        this.useHallOfFameFilter = useHallOfFameFilter;
    }

    public boolean isHallOfFameFilter() {
        return hallOfFameFilter;
    }

    public void setHallOfFameFilter(boolean hallOfFameFilter) {
        this.hallOfFameFilter = hallOfFameFilter;
    }
}
