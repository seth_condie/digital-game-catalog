package controller.objectClasses;

import controller.Globals;

import java.util.ArrayList;
import java.util.List;

/**
 * The Video_Game Class
 * This details the video_game objects used in the program
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */

public class Video_Game implements Comparable<Video_Game> {
    private int id;
    private String title;
    private String system;
    private List<String> otherSystems;
    private List<String> repeatSystems;
    private int playerMin;
    private int playerMax;
    private String developer;
    private String publisher;
    private int originalReleaseYear;
    private List<Integer> reReleaseYears;
    private String genre;
    private String series;
    private String coreAesthetic;
    private String otherAesthetic;
    private String recommendedFor;
    private String completionLevel;
    private String rating;
    private String comments;
    private String thoughtsLink;
    private String thoughtsType;
    private boolean revisit;
    private boolean couch;
    private boolean hallOfFame;

    public Video_Game(int id) {
        this.id = id;
        title = null;
        system = null;
        otherSystems = new ArrayList<>();
        repeatSystems = new ArrayList<>();
        playerMin = Globals.MISSINGNO;
        playerMax = Globals.MISSINGNO;
        developer = null;
        publisher = null;
        originalReleaseYear = Globals.MISSINGNO;
        reReleaseYears = new ArrayList<>();
        genre = null;
        coreAesthetic = null;
        otherAesthetic = null;
        recommendedFor = null;
        completionLevel = null;
        rating = null;
        comments = null;
        thoughtsLink = null;
        thoughtsType = null;
        revisit = false;
        couch = false;
        hallOfFame = false;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSystem() {
        return system;
    }

    public List<String> getOtherSystems() {
        return otherSystems;
    }

    public List<String> getRepeatSystems() {
        return repeatSystems;
    }

    public int getPlayerMin() {
        return playerMin;
    }

    public int getPlayerMax() {
        return playerMax;
    }

    public String getDeveloper() {
        return developer;
    }

    public String getPublisher() {
        return publisher;
    }

    public int getOriginalReleaseYear() {
        return originalReleaseYear;
    }

    public List<Integer> getReReleaseYears() {
        return reReleaseYears;
    }

    public String getGenre() {
        return genre;
    }

    public String getSeries() {
        return series;
    }

    public String getCoreAesthetic() {
        return coreAesthetic;
    }

    public String getOtherAesthetic() {
        return otherAesthetic;
    }

    public String getRecommendedFor() {
        return recommendedFor;
    }

    public String getCompletionLevel() {
        return completionLevel;
    }

    public String getRating() {
        return rating;
    }

    public String getComments() {
        return comments;
    }

    public String getThoughtsLink() {
        return thoughtsLink;
    }

    public String getThoughtsType() {
        return thoughtsType;
    }

    public boolean isShouldRevisit() {
        return revisit;
    }

    public boolean isLocalCouch() {
        return couch;
    }

    public boolean isHallOfFame() {
        return hallOfFame;
    }

    public void setTitle(String newTitle) {
        title = newTitle;
    }

    public void setSystem(String newSystem) {
        system = newSystem;
    }

    public void setOtherSystems(List<String> newOtherSystems) {
        otherSystems = newOtherSystems;
    }

    public void setRepeatSystems(List<String> newRepeatSystems) {
        repeatSystems = newRepeatSystems;
    }

    public void setPlayerMin(int newPlayerMin) {
        playerMin = newPlayerMin;
    }

    public void setPlayerMax(int newPlayerMax) {
        playerMax = newPlayerMax;
    }

    public void setDeveloper(String newDeveloper) {
        developer = newDeveloper;
    }

    public void setPublisher(String newPublisher) {
        publisher = newPublisher;
    }

    public void setOriginalReleaseYear(int newOriginalReleaseYear) {
        originalReleaseYear = newOriginalReleaseYear;
    }

    public void setReReleaseYear(List<Integer> newReReleaseYear) {
        reReleaseYears = newReReleaseYear;
    }

    public void setGenre(String newGenre) {
        genre = newGenre;
    }

    public void setSeries(String newSeries) {
        series = newSeries;
    }

    public void setCoreAesthetic(String newCoreAesthetic) {
        coreAesthetic = newCoreAesthetic;
    }

    public void setOtherAesthetic(String newOtherAesthetic) {
        otherAesthetic = newOtherAesthetic;
    }

    public void setRecommendedFor(String newRecommendedFor) {
        recommendedFor = newRecommendedFor;
    }

    public void setCompletionLevel(String newCompletionLevel) {
        completionLevel = newCompletionLevel;
    }

    public void setRating(String newRating) {
        rating = newRating;
    }

    public void setComments(String newComments) {
        comments = newComments;
    }

    public void setThoughtsLink(String newThoughtsLink) {
        thoughtsLink = newThoughtsLink;
    }

    public void setThoughtsType(String newThoughtsType) {
        thoughtsType = newThoughtsType;
    }

    public void setRevisit(boolean newRevisit) {
        revisit = newRevisit;
    }

    public void setCouch(boolean newCouch) {
        couch = newCouch;
    }

    public void setHallOfFame(boolean newHallOfFame) {
        hallOfFame = newHallOfFame;
    }

    public String toReportString() {
        String display;
        int threeIndCheck = 23;
        int threeSubString = 20;
        int twoIndCheck = 15;
        int twoSubString = 12;
        int oneIndCheck = 7;


        if (title.length() > threeIndCheck) {
            String trucTitle = title.substring(0, threeSubString) + "...\t";
            display = trucTitle;
        } else if (title.length() > twoIndCheck) {
            display = title + "\t";
        } else if (title.length() > oneIndCheck) {
            display = title + "\t\t";
        } else {
            display = title + "\t\t\t";
        }

        if (system.length() > twoIndCheck) {
            display += system.substring(0, twoSubString) + "...\t";
        } else if (system.length() > oneIndCheck) {
            display += system + "\t";
        } else {
            display += system + "\t\t";
        }

        if (otherSystems.size() != 0) {
            if (otherSystems.size() > 1) {
                display += otherSystems.size() + " more\t\t";
            } else if (otherSystems.get(0).length() > twoIndCheck) {
                display += otherSystems.get(0).substring(0, twoSubString) + "...\t";
            } else if (otherSystems.get(0).length() > oneIndCheck) {
                display += otherSystems.get(0) + "\t";
            } else {
                display += otherSystems.get(0) + "\t\t";
            }
        } else {
            display += "\t\t";
        }

        if (repeatSystems.size() != 0) {
            if (repeatSystems.size() > 1) {
                display += repeatSystems.size() + " more \t\t";
            } else if (repeatSystems.get(0).length() > twoIndCheck) {
                display += repeatSystems.get(0).substring(0, twoSubString) + "...\t";
            } else if (repeatSystems.get(0).length() > oneIndCheck) {
                display += repeatSystems.get(0) + "\t";
            } else {
                display += repeatSystems.get(0) + "\t\t";
            }
        } else {
            display += "\t\t";
        }

        if (playerMin == playerMax) {
            display += Integer.toString(playerMin) + "\t";
        } else {
            display += Integer.toString(playerMin) + "-" + Integer.toString(playerMax) + "\t";
        }

        if (developer.length() > twoIndCheck) {
            display += developer.substring(0, twoSubString) + "...\t";
        } else if (developer.length() > oneIndCheck) {
            display += developer + "\t";
        } else {
            display += developer + "\t\t";
        }

        if (publisher.length() > twoIndCheck) {
            display += publisher.substring(0, twoSubString) + "...\t";
        } else if (publisher.length() > oneIndCheck) {
            display += publisher + "\t";
        } else {
            display += publisher + "\t\t";
        }

        display += Integer.toString(originalReleaseYear) + "\t\t";

        String reReleaseDisplay;
        if (reReleaseYears.size() == 0) {
            reReleaseDisplay = "\t";
        } else if (reReleaseYears.size() > 2) {
            reReleaseDisplay = reReleaseYears.size() + " more\t";
        } else if (reReleaseYears.size() == 2) {
            reReleaseDisplay = reReleaseYears.get(0) + ", " + reReleaseYears.get(1);
        } else {
            reReleaseDisplay = reReleaseYears.get(0) + "\t";
        }
        display += reReleaseDisplay + "\t";

        if (genre.length() > twoIndCheck) {
            display += genre.substring(0, twoSubString) + "...\t";
        } else if (genre.length() > oneIndCheck) {
            display += genre + "\t";
        } else {
            display += genre + "\t\t";
        }

        if (series.length() > twoIndCheck) {
            display += series.substring(0, twoSubString) + "...\t";
        } else if (series.length() > oneIndCheck) {
            display += series + "\t";
        } else {
            display += series + "\t\t";
        }

        if (coreAesthetic.length() > oneIndCheck) {
            display += coreAesthetic + "\t";
        } else {
            display += coreAesthetic + "\t\t";
        }

        if (otherAesthetic.length() > oneIndCheck) {
            display += otherAesthetic + "\t";
        } else {
            display += otherAesthetic + "\t\t";
        }

        if (recommendedFor.length() > twoIndCheck) {
            display += recommendedFor.substring(0, twoSubString) + "...\t";
        } else if (recommendedFor.length() > oneIndCheck) {
            display += recommendedFor + "\t";
        } else {
            display += recommendedFor + "\t\t";
        }

        if (completionLevel.length() > oneIndCheck) {
            display += completionLevel + "\t\t";
        } else {
            display += completionLevel + "\t\t\t";
        }

        if (rating.length() > oneIndCheck) {
            display += rating + "\t";
        } else {
            display += rating + "\t\t";
        }

        if (thoughtsType.length() > oneIndCheck) {
            display += thoughtsType + "\t";
        } else {
            display += thoughtsType + "\t\t";
        }

        display += Boolean.toString(revisit) + "\t\t";

        display += Boolean.toString(couch) + "\t\t\t";

        display += Boolean.toString(hallOfFame) + "\t";

        return display;
    }

    @Override
    public boolean equals(Object object) {
        Video_Game game = (Video_Game) object;
        return this.id == game.id;
    }

    @Override
    public int compareTo(Video_Game game) {
        //return -1 if this goes before game
        //return 1 if this after game
        //return 0 if they are equal
        int value = 0;

        if (this.originalReleaseYear < game.originalReleaseYear) {
            return -1;
        } else if (this.originalReleaseYear > game.originalReleaseYear) {
            value = 1;
        } else {
            return this.title.compareTo(game.title);
        }

        return value;
    }
}
