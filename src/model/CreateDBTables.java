package model;

import controller.Globals;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * The CreateDBTables class
 * This final class will hold all of the SQL queries and commands used to update and create the database
 * This class cannot be instantiated, all of the methods are static
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public final class CreateDBTables {

    private CreateDBTables() {}

    public static void createTables(Connection connection) throws SQLException {
        createVideoGamesTable(connection);
        createSystemTable(connection);
        createOtherSystemsTable(connection);
        createRepeatSystemsTable(connection);
        createReReleaseYearsTable(connection);
        createGenresTable(connection);
        createSysDataTable(connection);
    }

    public static void updateDb_v2(Connection connection) throws SQLException {
        updateVideoGamesTable_v2(connection);
        updateDatabaseVersionNumber(connection, 2);
    }

    public static void updateDb_v3(Connection connection) throws SQLException {
        createToysTable(connection);
        updateDatabaseVersionNumber(connection, 3);
    }

    public static void updateDb_v4(Connection connection) throws SQLException {
        updateDatabaseVersionNumber(connection, 4);
    }

    public static void updateDb_v5(Connection connection) throws SQLException {
        updateVideoGamesTable_v3(connection);
        updateDatabaseVersionNumber(connection, 5);
    }

    public static void updateDb_v6(Connection connection) throws SQLException {
        updateToysTable(connection);
        updateDatabaseVersionNumber(connection, 6);
    }

    private static void createVideoGamesTable(Connection connection) throws SQLException {

        String createVideoGameTableSQL = "CREATE TABLE " + GameCatDBSchema.VideoGamesTable.NAME + "(" +
                GameCatDBSchema.VideoGamesTable.Cols.GAME_ID + " INTEGER PRIMARY KEY," +
                GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " VARCHAR(255) NOT NULL UNIQUE," +
                GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID + " INTEGER NOT NULL REFERENCES " +
                GameCatDBSchema.SystemsTable.NAME + "(" + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + ")," +
                GameCatDBSchema.VideoGamesTable.Cols.MIN_PLAYERS + " INTEGER," +
                GameCatDBSchema.VideoGamesTable.Cols.MAX_PLAYERS + " INTEGER," +
                GameCatDBSchema.VideoGamesTable.Cols.DEVELOPER + " VARCHAR(255)," +
                GameCatDBSchema.VideoGamesTable.Cols.PUBLISHER + " VARCHAR(255)," +
                GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR + " INTEGER," +
                GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID + " INTEGER REFERENCES " +
                GameCatDBSchema.SystemsTable.NAME + "(" + GameCatDBSchema.GenresTable.Cols.GENRE_ID + "));";

        //try with resources even if you are throwing the exception
        try (Statement createVideoGameTableStmt = connection.createStatement()) {
            createVideoGameTableStmt.executeUpdate(createVideoGameTableSQL);
        }
    }

    private static void createSystemTable(Connection connection) throws SQLException {

        String createSystemsTableSQL = "CREATE TABLE " + GameCatDBSchema.SystemsTable.NAME + "(" +
                GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + " INTEGER PRIMARY KEY," +
                GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " VARCHAR(255) NOT NULL," +
                GameCatDBSchema.SystemsTable.Cols.GENERATION + " INTEGER NOT NULL," +
                GameCatDBSchema.SystemsTable.Cols.IS_HANDHELD + " INTEGER DEFAULT 0);";

        try (Statement createSystemsTableStmt = connection.createStatement()) {
            createSystemsTableStmt.executeUpdate(createSystemsTableSQL);
            createSystemsTableStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(1, 'none', 0, 0);");
        }
    }

    private static void createOtherSystemsTable(Connection connection) throws SQLException {

        String createOtherSystemsTableSQL = "CREATE TABLE " + GameCatDBSchema.OtherSystemsTable.NAME + "(" +
                GameCatDBSchema.OtherSystemsTable.Cols.GAME_ID + " INTEGER REFERENCES " +
                GameCatDBSchema.VideoGamesTable.NAME + "(" + GameCatDBSchema.VideoGamesTable.Cols.GAME_ID + ")," +
                GameCatDBSchema.OtherSystemsTable.Cols.SYSTEM_ID + " INTEGER REFERENCES " +
                GameCatDBSchema.SystemsTable.NAME + "(" + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + ")," +
                "PRIMARY KEY(" + GameCatDBSchema.OtherSystemsTable.Cols.GAME_ID + "," + GameCatDBSchema.OtherSystemsTable.Cols.SYSTEM_ID + "));";

        try (Statement createOtherSystemsTableStmt = connection.createStatement()) {
            createOtherSystemsTableStmt.executeUpdate(createOtherSystemsTableSQL);
        }
    }

    private static void createRepeatSystemsTable(Connection connection) throws SQLException {

        String createRepeatSystemsTableSQL = "CREATE TABLE " + GameCatDBSchema.RepeatSystemsTable.NAME + "(" +
                GameCatDBSchema.RepeatSystemsTable.Cols.GAME_ID + " INTEGER REFERENCES " +
                GameCatDBSchema.VideoGamesTable.NAME + "(" + GameCatDBSchema.VideoGamesTable.Cols.GAME_ID + ")," +
                GameCatDBSchema.RepeatSystemsTable.Cols.SYSTEM_ID + " INTEGER REFERENCES " +
                GameCatDBSchema.SystemsTable.NAME + "(" + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + ")," +
                "PRIMARY KEY(" + GameCatDBSchema.RepeatSystemsTable.Cols.GAME_ID + "," + GameCatDBSchema.RepeatSystemsTable.Cols.SYSTEM_ID + "));";

        try (Statement createRepeatSystemsTableStmt = connection.createStatement()) {
            createRepeatSystemsTableStmt.executeUpdate(createRepeatSystemsTableSQL);
        }
    }

    private static void createReReleaseYearsTable(Connection connection) throws SQLException {

        String createReReleaseYearsTableSQL = "CREATE TABLE " + GameCatDBSchema.ReReleaseYearsTable.NAME + "(" +
                GameCatDBSchema.ReReleaseYearsTable.Cols.GAME_ID + " INTEGER REFERENCES " +
                GameCatDBSchema.VideoGamesTable.NAME + "(" + GameCatDBSchema.VideoGamesTable.Cols.GAME_ID + ")," +
                GameCatDBSchema.ReReleaseYearsTable.Cols.RE_RELEASE_YEAR + " INTEGER NOT NULL," +
                "PRIMARY KEY(" + GameCatDBSchema.ReReleaseYearsTable.Cols.GAME_ID + "," + GameCatDBSchema.ReReleaseYearsTable.Cols.RE_RELEASE_YEAR + "));";

        try (Statement createReReleaseYearsTableStmt = connection.createStatement()) {
            createReReleaseYearsTableStmt.executeUpdate(createReReleaseYearsTableSQL);
        }
    }

    private static void createGenresTable(Connection connection) throws SQLException {

        String createGenresTableSQL = "CREATE TABLE " + GameCatDBSchema.GenresTable.NAME + "(" +
                GameCatDBSchema.GenresTable.Cols.GENRE_ID + " INTEGER PRIMARY KEY," +
                GameCatDBSchema.GenresTable.Cols.GENRE_NAME + " INTEGER NOT NULL);";

        try (Statement createGenresTableStmt = connection.createStatement()) {
            createGenresTableStmt.executeUpdate(createGenresTableSQL);
            createGenresTableStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(1, 'none');");
        }
    }

    private static void createSysDataTable(Connection connection) throws SQLException {
        String createSysDataTableSQL = "CREATE TABLE " + GameCatDBSchema.SysDataTable.NAME + "(" +
                GameCatDBSchema.SysDataTable.Cols.DB_VERSION + " INTEGER)";

        try (Statement createSysDataTableStmt = connection.createStatement()) {
            createSysDataTableStmt.executeUpdate(createSysDataTableSQL);
            createSysDataTableStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SysDataTable.NAME + " VALUES(" + Globals.CURRENT_VERSION + ");");
        }
    }

    private static void updateVideoGamesTable_v2(Connection connection) throws SQLException {
        try (Statement updateVGTableStmt = connection.createStatement()) {
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.CORE_AESTHETIC + " VARCHAR(255);");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.OTHER_AESTHETIC + " VARCHAR(255);");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.RECOMMENDED_FOR + " VARCHAR(255);");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.COMPLETION_LEVEL + " VARCHAR(255);");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.RATING + " VARCHAR(255);");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.COMMENTS + " VARCHAR(2255);");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_LINK + " VARCHAR(1048);");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_TYPE + " VARCHAR(255);");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.REVISIT + " INTEGER DEFAULT 0;");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.COUCH + " INTEGER DEFAULT 0;");
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.HALL_OF_FAME + " INTEGER DEFAULT 0;");
        }
    }

    private static void updateVideoGamesTable_v3(Connection connection) throws SQLException {
        try (Statement updateVGTableStmt = connection.createStatement()) {
            updateVGTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.VideoGamesTable.NAME + " ADD " + GameCatDBSchema.VideoGamesTable.Cols.SERIES + " VARCHAR(255);");
            updateVGTableStmt.executeUpdate("UPDATE " + GameCatDBSchema.VideoGamesTable.NAME + " SET " + GameCatDBSchema.VideoGamesTable.Cols.SERIES + " = '';" );
        }
    }

    private static void updateDatabaseVersionNumber(Connection connection, int version) throws SQLException {
        String updateSysDataTableSQL = "UPDATE " + GameCatDBSchema.SysDataTable.NAME + " SET " + GameCatDBSchema.SysDataTable.Cols.DB_VERSION + " = " + version + ";";

        try (Statement updateSysDataTableStmt = connection.createStatement()) {
            updateSysDataTableStmt.executeUpdate(updateSysDataTableSQL);
        }
    }

    private static void createToysTable(Connection connection) throws SQLException {
        String createToysTableSQL = "CREATE TABLE " + GameCatDBSchema.ToysTable.NAME + "(" +
                GameCatDBSchema.ToysTable.Cols.TOY_ID + " INTEGER PRIMARY KEY, " +
                GameCatDBSchema.ToysTable.Cols.TOY_NAME + " VARCHAR(255) NOT NULL, " +
                GameCatDBSchema.ToysTable.Cols.PLATFORM + " VARCHAR(255) NOT NULL);";

        try (Statement createToysTableStmt = connection.createStatement()) {
            createToysTableStmt.executeUpdate(createToysTableSQL);
        }
    }

    private static void updateToysTable(Connection connection) throws SQLException {
        try(Statement updateToysTableStmt = connection.createStatement()) {
            updateToysTableStmt.executeUpdate("ALTER TABLE " + GameCatDBSchema.ToysTable.NAME + " ADD " + GameCatDBSchema.ToysTable.Cols.TOY_SET + " VARCHAR(255);");
        }
    }
}
