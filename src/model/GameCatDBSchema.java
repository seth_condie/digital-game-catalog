package model;

/**
 * The GameCatDBSchema Class
 * This will hold the schema for the database
 * The field names will be stored in inner classes called Cols
 * This class cannot be instantiated, all of the methods are static
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public final class GameCatDBSchema {

    private GameCatDBSchema() {}

    public static final class VideoGamesTable {
        public static final String NAME = "video_games";

        public static final class Cols {
            public static final String GAME_ID = "game_id";
            public static final String GAME_TITLE = "game_title";
            public static final String SYSTEM_ID = "system_id";
            public static final String MIN_PLAYERS = "min_players";
            public static final String MAX_PLAYERS = "max_players";
            public static final String DEVELOPER = "developer";
            public static final String PUBLISHER = "publisher";
            public static final String ORIGINAL_RELEASE_YEAR = "original_release_year";
            public static final String GENRE_ID = "genre_id";
            public static final String SERIES = "series";
            public static final String CORE_AESTHETIC = "core_aesthetic";
            public static final String OTHER_AESTHETIC = "other_aesthetic";
            public static final String RECOMMENDED_FOR = "recommended_for";
            public static final String COMPLETION_LEVEL = "completion_level";
            public static final String RATING = "rating";
            public static final String COMMENTS = "comments";
            public static final String THOUGHTS_LINK = "thoughts_link";
            public static final String THOUGHTS_TYPE = "thoughts_type";
            public static final String REVISIT = "revisit";
            public static final String COUCH = "couch";
            public static final String HALL_OF_FAME = "hall_of_fame";
        }
    }

    public static final class SystemsTable {
        public static final String NAME = "systems";

        public static final class Cols {
            public static final String SYSTEM_ID = "system_id";
            public static final String SYSTEM_NAME = "system_name";
            public static final String GENERATION = "generation";
            public static final String IS_HANDHELD = "is_handheld";
        }
    }

    public static final class OtherSystemsTable {
        public static final String NAME = "other_systems";

        public static final class Cols {
            public static final String GAME_ID = "game_id";
            public static final String SYSTEM_ID = "system_id";
        }
    }

    public static final class ReReleaseYearsTable {
        public static final String NAME = "re_release_years";

        public static final class Cols {
            public static final String GAME_ID = "game_id";
            public static final String RE_RELEASE_YEAR = "re_release_year";
        }
    }

    public static final class GenresTable {
        public static final String NAME = "genres";

        public static final class Cols {
            public static final String GENRE_ID = "genre_id";
            public static final String GENRE_NAME = "genre_name";
        }
    }

    public static final class RepeatSystemsTable {
        public static final String NAME = "repeat_systems";

        public static final class Cols {
            public static final String GAME_ID = "game_id";
            public static final String SYSTEM_ID = "system_id";
        }
    }

    public static final class SysDataTable {
        public static final String NAME = "sysdata";

        public static final class Cols {
            public static final String DB_VERSION = "version";
        }
    }

    public static final class ToysTable {
        public static final String NAME = "toys";

        public static final class Cols {
            public static final String TOY_ID = "toy_id";
            public static final String TOY_NAME = "toy_name";
            public static final String TOY_SET = "toy_set";
            public static final String PLATFORM = "platform";
        }
    }
}
