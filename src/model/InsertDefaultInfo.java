package model;

import controller.Globals;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * the InsertDefaultInfo Class
 * This class holds the SQL queries used to enter in default data into the database
 * This class cannot be instantiated, all of the methods are static
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public final class InsertDefaultInfo {

    private InsertDefaultInfo() {}

    public static void insertDefaultGenres(Connection connection) throws SQLException {
        try (Statement insertStmt = connection.createStatement()) {
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(2, 'Abstract');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(3, 'Action Adventure');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(4, 'Action Platformer');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(5, 'Action RPG');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(6, 'Adventure');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(7, 'Adventure Platformer');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(8, 'Beat-em-up');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(9, 'Edutainment');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(10, 'Fighting');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(11, 'First Person Shooter');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(12, 'Kart Racer');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(13, 'MMORPG');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(14, 'Party');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(15, 'Platformer');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(16, 'Point and Click Adventure');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(17, 'Racing');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(18, 'Puzzle');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(19, 'Real Time Strategy');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(20, 'Rhythm');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(21, 'Shoot-em-up');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(22, 'Tactics');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(23, 'Third Person Shooter');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(24, 'Trading Card Game');");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " VALUES(25, 'Turn Based RPG');");
        }
    }

    public static void insertDefaultSystems(Connection connection) throws SQLException {
        try (Statement insertStmt = connection.createStatement()) {
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(2, 'Arcade', 0, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(3, 'Magnavox Odyssey', 1, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(4, 'Atari 2600', 2, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(5, 'Intellivision', 2, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(6, 'ColecoVision', 2, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(7, 'Atari 5200', 2, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(8, 'Nintendo Entertainment System', 3, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(9, 'Sega Master System', 3, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(10, 'Atari 7800', 3, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(11, 'TurboGrafx-16', 4, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(12, 'Sega Genesis', 4, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(13, 'Super Nintendo', 4, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(14, 'Neo Geo', 4, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(15, 'Game Boy', 4, 1);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(16, 'Game Gear', 4, 1);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(17, '3DO', 5, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(18, 'Atari Jaguar', 5, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(19, 'Sega Saturn', 5, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(20, 'Sony Playstation', 5, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(21, 'Nintendo 64', 5, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(22, 'Game Boy Color', 5, 1);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(23, 'Sega Dreamcast', 6, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(24, 'Playstation 2', 6, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(25, 'GameCube', 6, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(26, 'Xbox', 6, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(27, 'Game Boy Advance', 6, 1);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(28, 'Xbox 360', 7, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(29, 'Playstation 3', 7, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(30, 'Wii', 7, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(31, 'Nintendo DS', 7, 1);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(32, 'Playstation Portable', 7, 1);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(33, 'Wii U', 8, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(34, 'Playstation 4', 8, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(35, 'Xbox One', 8, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(36, 'Nintendo 3DS', 8, 1);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(37, 'Playstation Vita', 8, 1);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(38, 'PC', 0, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(39, 'Steam', 0, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(40, 'Wii Digital', 7, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(41, 'Playstation 3 Digital', 7, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(42, 'Wii U Digital', 8, 0);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + " VALUES(43, 'Nintendo 3DS Digital', 8, 1);");
        }
    }

    public static void insertExampleData(Connection connection) throws SQLException {
        try (Statement insertStmt = connection.createStatement()) {
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.VideoGamesTable.NAME + " VALUES(1, " +
                    "'Super Mario All-Stars', " +               //game_title
                    "13, " +                                    //system_id
                    "1, 2, " +                                  //min_players, max_players
                    "'Nintendo', " +                            //developer
                    "'Nintendo', " +                            //publisher
                    "1993, " +                                  //original_release_year
                    "15, '" +                                   //genre_id
                    Globals.Aesthetic.CHALLENGE + "', '" +      //core_aesthetic
                    Globals.NONE + "', " +                      //other_aesthetic
                    "'Everyone', '" +                           //recommended_for
                    Globals.CompletionLevel.PLAY + "', '" +     //completion_level
                    Globals.Rating.SATISFIED + "', " +          //rating
                    "'Comments', '" +                           //comments
                    Globals.NONE + "', '" +                     //thoughts_link
                    Globals.ThoughtsType.PLACEHOLDER + "', " +  //thoughts_type
                    "0, 0, 0, " +                               //revisit, couch, hall_of_fame
                    "'Super Mario Bros');");                    //series

            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.VideoGamesTable.NAME + " VALUES(2, " +
                    "'Legend of Mana', " +                      //game_title
                    "20, " +                                    //system_id
                    "1, 2, " +                                  //min_players, max_players
                    "'SquareEnix', " +                          //developer
                    "'SquareEnix', " +                          //publisher
                    "2000, " +                                  //original_release_year
                    "5, '" +                                    //genre_id
                    Globals.Aesthetic.ABNEGATION + "', '" +     //core_aesthetic
                    Globals.Aesthetic.FANTASY + "', " +         //other_aesthetic
                    "'Everyone', '" +                           //recommended_for
                    Globals.CompletionLevel.FINISH + "', '" +   //completion_level
                    Globals.Rating.SATISFIED + "', " +          //rating
                    "'Comments', '" +                           //comments
                    Globals.NONE + "', '" +                     //thoughts_link
                    Globals.ThoughtsType.PLACEHOLDER + "', " +  //thoughts_type
                    "0, 1, 1, " +                                //revisit, couch, hall_of_fame
                    "'Mana');");                                //series

            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.OtherSystemsTable.NAME + " VALUES(2, 41);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.ReReleaseYearsTable.NAME + " VALUES(2, 2011);");

            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.VideoGamesTable.NAME + " VALUES (3, " +
                    "'Jak and Daxter: The Precursor Legacy', " +//game_title
                    "41, " +                                    //system_id
                    "1, 1, " +                                  //min_players, max_players
                    "'Naughty Dog', " +                         //developer
                    "'Sony Computer Entertainment', " +         //publisher
                    "2001, " +                                  //original_release_year
                    "7, '" +                                    //genre_id
                    Globals.Aesthetic.CHALLENGE + "', '" +      //core_aesthetic
                    Globals.NONE + "', " +                      //other_aesthetic
                    "'Fans of the series', '" +                 //recommended_for
                    Globals.CompletionLevel.FINISH + "', '" +   //completion_level
                    Globals.Rating.SATISFIED + "', " +          //rating
                    "'Comments', '" +                           //comments
                    Globals.NONE + "', '" +                     //thoughts_link
                    Globals.ThoughtsType.PLACEHOLDER + "', " +  //thoughts_type
                    "0, 0, 0, " +                                //revisit, couch, hall_of_fame
                    "'Jak and Daxter');");                      //series
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.OtherSystemsTable.NAME + " VALUES(3, 24);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.ReReleaseYearsTable.NAME + " VALUES(3, 2012);");

            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.VideoGamesTable.NAME + " VALUES(4, " +
                    "'Details Tab Explained', " +               //game_title
                    "8, " +                                     //system_id
                    "1, 1, " +                                  //min_players, max_players
                    "'None', " +                                //developer
                    "'None', " +                                //publisher
                    "0000, " +                                  //original_release_year
                    "4, '" +                                    //genre_id
                    Globals.Aesthetic.CHALLENGE + "', '" +      //core_aesthetic
                    Globals.NONE + "', " +                      //other_aesthetic
                    "'Beginners', '" +                          //recommended_for
                    Globals.CompletionLevel.FINISH + "', '" +   //completion_level
                    Globals.Rating.SATISFIED + "', " +          //rating
                    "'For an explanation of the aesthetics please see the link below click the open button\n" +
                    "For the ratings I usually grade games SUBJECTIVELY on the following scale\n" +
                    "Satisfied, meaning that I like and have played enough of it to recommend it to others\n" +
                    "Content, either I haven''t played it enough to recommend to others but still like what I have played of the game\n" +
                    "or I have played enough of it and I personally like it but I would not recommend it to others\n" +
                    "Discontented, means that I did not like the game and I do not recommend it but for one reason or another I kept it in my collection\n" +
                    "A Hall of Fame game means that the game is engaging enough to me that each time I am compelled to play it until I see the end game credits!\n" +
                    "The thoughts link usually links back to a google doc with some thought on that game an explanation of the thoughts can be found with the thoughts type.', " +   //comments
                    "'https://www.youtube.com/watch?v=uepAJ-rqJKA'" + ", '" +  //thoughts_link
                    Globals.ThoughtsType.PLACEHOLDER + "', " +  //thoughts_type
                    "0, 0, 0, '');");                           //revisit, couch, hall_of_fame, series

            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.RepeatSystemsTable.NAME + " VALUES(4, 42);");
            insertStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.ReReleaseYearsTable.NAME + " VALUES(4, 2007);");
        }
    }
}
