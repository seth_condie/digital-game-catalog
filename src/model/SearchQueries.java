package model;

import controller.Globals;
import controller.objectClasses.AdvancedSearchFilters;
import controller.objectClasses.System;
import controller.objectClasses.Toy;
import controller.objectClasses.Video_Game;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SearchQueries Class
 * This class holds all of the queries that return results,
 * these results are formatted in this class then returned back to the program
 * This class cannot be instantiated, all of the methods are static
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public final class SearchQueries {

    private SearchQueries() {}

    /**
     * verifyData method
     * <p>
     * This method will check to make sure that the none fields of data and the database version exist
     * these fields should be included in all databases designed to work with this program
     *
     * @param connection the connection to the database
     * @return MISSINGNO (-1) if the database cannot be verified, or the version number if it is verified
     * @throws SQLException this could be expanded to include all of the necessary tables
     *                      or even further by checking so that all of the columns are present
     */
    public static int verifyData(Connection connection) throws SQLException {
        String genreCheckSQL = "SELECT * FROM " + GameCatDBSchema.GenresTable.NAME + " WHERE " + GameCatDBSchema.GenresTable.Cols.GENRE_NAME + " = 'none'";
        String systemCheckSQL = "SELECT * FROM " + GameCatDBSchema.SystemsTable.NAME + " WHERE " + GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " = 'none'";
        String versionCheckSQL = "SELECT * FROM " + GameCatDBSchema.SysDataTable.NAME + ";";

        int version;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(genreCheckSQL);
            if (!resultSet.isBeforeFirst()) {
                //the result set returned 'none' if this did NOT happen return false
                return Globals.MISSINGNO;
            }
            resultSet = searchStmt.executeQuery(systemCheckSQL);
            if (!resultSet.isBeforeFirst()) {
                return Globals.MISSINGNO;
            }
            resultSet = searchStmt.executeQuery(versionCheckSQL);
            if (!resultSet.isBeforeFirst()) {
                return Globals.MISSINGNO;
            } else {
                version = resultSet.getInt(GameCatDBSchema.SysDataTable.Cols.DB_VERSION);
            }
        }
        return version;
    }

    /**
     * The videoGameSearch method
     * <p>
     * This method will take the filters provided and build a SQL query to search the database
     * Any results that are returned by the filter are compiled into a List to be returned to the program
     * <p>
     * Not all of the filters are required, if a filter is not provided by the program pass in a blank string ""
     *
     * @param connection   the connection to the database
     * @param titleFilter  the filter for the game's title
     * @param systemId the filter for the system the game is played on
     * @param genreId  the filter for the genre that the game is apart of
     * @param repeatSearch
     * @return a list of video game objects that pass the filter check
     * @throws SQLException
     */
    public static List<Video_Game> videoGameSearch(Connection connection, String titleFilter, int systemId, int genreId, boolean repeatSearch) throws SQLException {

        /**
         *  SELECT * FROM video_games
         *  LEFT JOIN systems ON video_games.system_id = systems.system_id
         *   JOIN genres ON video_games.genre_id = genres.genre_id
         */
        String getGamesSQL = "SELECT * FROM " + GameCatDBSchema.VideoGamesTable.NAME +
                " LEFT JOIN " + GameCatDBSchema.SystemsTable.NAME +
                " ON " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID +
                " = " + GameCatDBSchema.SystemsTable.NAME + "." + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID +
                " JOIN " + GameCatDBSchema.GenresTable.NAME +
                " ON " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID +
                " = " + GameCatDBSchema.GenresTable.NAME + "." + GameCatDBSchema.GenresTable.Cols.GENRE_ID;

        if (repeatSearch) {
            getGamesSQL += " LEFT JOIN " + GameCatDBSchema.RepeatSystemsTable.NAME +
                " ON " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.VideoGamesTable.Cols.GAME_ID +
                " = " + GameCatDBSchema.RepeatSystemsTable.NAME + "." + GameCatDBSchema.RepeatSystemsTable.Cols.GAME_ID;
        }

        boolean first = true;

        if (titleFilter.length() > 0) {
            getGamesSQL = getGamesSQL + " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " LIKE '%" + UpdateQueries.fixQuoteStringIssue(titleFilter) + "%'";
            first = false;
        }
        if (systemId > Globals.MISSINGNO) {
            if (!first) {
                getGamesSQL = getGamesSQL + " AND " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + " = " + systemId;
            } else {
                getGamesSQL = getGamesSQL + " WHERE " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + " = " + systemId;
                first = false;
            }
            if (repeatSearch) {
                getGamesSQL = getGamesSQL + " OR " + GameCatDBSchema.RepeatSystemsTable.NAME + "." + GameCatDBSchema.RepeatSystemsTable.Cols.SYSTEM_ID + " = " + systemId;
            }
        }
        if (genreId > Globals.MISSINGNO) {
            if (!first) {
                getGamesSQL = getGamesSQL + " AND " + GameCatDBSchema.GenresTable.Cols.GENRE_ID + " = " + genreId;
            } else {
                getGamesSQL = getGamesSQL + " WHERE " + GameCatDBSchema.GenresTable.Cols.GENRE_ID + " = " + genreId;
            }
        }


        getGamesSQL = getGamesSQL + " ORDER BY " + GameCatDBSchema.SystemsTable.Cols.GENERATION + " ASC, ";
        getGamesSQL = getGamesSQL + GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " ASC, ";
        getGamesSQL = getGamesSQL + GameCatDBSchema.SystemsTable.Cols.IS_HANDHELD + " DESC, ";
        getGamesSQL = getGamesSQL + GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR + " ASC, ";
        getGamesSQL = getGamesSQL + GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " ASC; ";

        List<Video_Game> results;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getGamesSQL);
            results = buildGameFromResultSet(connection, resultSet);
        }
        return results;
    }

    /**
     * The videoGameSearch method
     * <p>
     * This method will take the filters provided and build a SQL query to search the database
     * Any results that are returned by the filter are compiled into a List to be returned to the program
     *
     * @param connection the connection to the database
     * @param filters    the filter for the game
     * @return a list of video games that passed the filters
     * @throws SQLException
     */
    @Deprecated
    public static List<Video_Game> videoGameAdvanceSearch(Connection connection, AdvancedSearchFilters filters) throws SQLException {
        String getGamesSQL = buildQueryFromAdvancedSearchFilters(filters);

        List<Video_Game> results;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getGamesSQL);
            results = buildGameFromAdvanceSearchResultSet(connection, resultSet, filters);
        }
        return results;
    }

    /**
     * the findGenreById method
     * This method will return the genre name using the provided ID
     *
     * @param connection the connection to the database
     * @param id         the id of the genre, if the id is not in the database a SQLException will be thrown
     * @return the name of the Genre in the form of a string or null if that genre is in the database
     * @throws SQLException
     */
    private static String findGenreById(Connection connection, int id) throws SQLException {
        String getGenreSQL = "SELECT * FROM " + GameCatDBSchema.GenresTable.NAME +
                " WHERE " + GameCatDBSchema.GenresTable.Cols.GENRE_ID + " = " + String.valueOf(id);

        String genreFound;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getGenreSQL);
            genreFound = resultSet.getString(GameCatDBSchema.GenresTable.Cols.GENRE_NAME);
        }

        return genreFound;
    }

    /**
     * the searchOtherSystemForGameById method
     * This method is used by the videoGameSearch method to gather the other systems list from the database
     *
     * @param connection the connection to the database
     * @param id         the id of the game to search
     * @return a list of the other systems this game can be played on
     * @throws SQLException
     */
    private static List<String> searchOtherSystemsForGameById(Connection connection, int id) throws SQLException {
        //SELECT * FROM other_systems LEFT JOIN systems ON systems.system_id = other_systems.system_id WHERE game_id = [game_id]
        String getOtherSystemsSQL = "SELECT * FROM " + GameCatDBSchema.OtherSystemsTable.NAME +
                " LEFT JOIN " + GameCatDBSchema.SystemsTable.NAME +
                " ON " + GameCatDBSchema.SystemsTable.NAME + "." + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID +
                " = " + GameCatDBSchema.OtherSystemsTable.NAME + "." + GameCatDBSchema.OtherSystemsTable.Cols.SYSTEM_ID +
                " WHERE " + GameCatDBSchema.OtherSystemsTable.Cols.GAME_ID + " = " + String.valueOf(id);

        List<String> otherSystems = new ArrayList<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getOtherSystemsSQL);
            while (resultSet.next()) {
                otherSystems.add(resultSet.getString(GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME));
            }
        }

        return otherSystems;
    }

    /**
     * the searchRepeatSystemsForGameById method
     * This method is used by the videoGameSearch to gather the repeat systems list from the database
     *
     * @param connection the connection to the database
     * @param id         the id of the game to search
     * @return a list of the repeat systems for this game
     * @throws SQLException
     */
    private static List<String> searchRepeatSystemsForGameById(Connection connection, int id) throws SQLException {
        String getRepeatSystemsSQL = "SELECT * FROM " + GameCatDBSchema.RepeatSystemsTable.NAME +
                " LEFT JOIN " + GameCatDBSchema.SystemsTable.NAME +
                " ON " + GameCatDBSchema.SystemsTable.NAME + "." + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID +
                " = " + GameCatDBSchema.RepeatSystemsTable.NAME + "." + GameCatDBSchema.RepeatSystemsTable.Cols.SYSTEM_ID +
                " WHERE " + GameCatDBSchema.RepeatSystemsTable.Cols.GAME_ID + " = " + String.valueOf(id);

        List<String> repeatSystems = new ArrayList<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getRepeatSystemsSQL);
            while (resultSet.next()) {
                repeatSystems.add(resultSet.getString(GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME));
            }
        }

        return repeatSystems;
    }

    /**
     * the searchReReleaseYearsForGameById method
     * This method is used by the videoGameSearch to gather the re release years for this game
     *
     * @param connection the connection to the database
     * @param id         the id of the game to search
     * @return a list of the re release years for this game
     * @throws SQLException
     */
    public static List<Integer> searchReReleaseYearsForGameById(Connection connection, int id) throws SQLException {
        String getReReleaseYearsSQL = "SELECT * FROM " + GameCatDBSchema.ReReleaseYearsTable.NAME +
                " WHERE " + GameCatDBSchema.ReReleaseYearsTable.Cols.GAME_ID + " = " + String.valueOf(id);

        List<Integer> reReleaseYears = new ArrayList<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getReReleaseYearsSQL);
            while (resultSet.next()) {
                reReleaseYears.add(resultSet.getInt(GameCatDBSchema.ReReleaseYearsTable.Cols.RE_RELEASE_YEAR));
            }
        }
        return reReleaseYears;
    }

    /**
     * the systemSearch method
     * This method will use the filter provided to search for all systems that meet the filter requirements
     *
     * @param connection the connection to the database
     * @param filter     the string used to filter the search
     * @return a list of the systems that meet the filter requirements
     * @throws SQLException
     */
    public static List<System> systemSearch(Connection connection, String filter) throws SQLException {
        String getSystemsSQL = "SELECT * FROM " + GameCatDBSchema.SystemsTable.NAME;

        if (filter.length() > 0) {
            getSystemsSQL = getSystemsSQL + " WHERE " + GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " LIKE '%" + filter + "%'";
        }

        getSystemsSQL = getSystemsSQL + " ORDER BY " + GameCatDBSchema.SystemsTable.Cols.GENERATION + " ASC; ";

        List<System> results = new ArrayList<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getSystemsSQL);
            while (resultSet.next()) {
                int id = resultSet.getInt(GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID);
                String system = resultSet.getString(GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME);
                int generation = resultSet.getInt(GameCatDBSchema.SystemsTable.Cols.GENERATION);
                int isHandheld = resultSet.getInt(GameCatDBSchema.SystemsTable.Cols.IS_HANDHELD);
                if (isHandheld == 1) {
                    results.add(new System(id, system, generation, true));
                } else {
                    results.add(new System(id, system, generation, false));
                }
            }
        }
        return results;
    }

    /**
     * the toySearch method
     * This method will use the filter provided and search for different toys
     * all toys that make it through the filter will be complied into a list and returned
     *
     * @param connection     the connection to the database
     * @param nameFilter     the string used to filter for toy names
     * @param platformFilter the string used to filter for platforms
     * @return a list of the toys that meet the filter requirements
     * @throws SQLException
     */
    public static List<Toy> toySearch(Connection connection, String nameFilter, String toySearchFilter, String platformFilter) throws SQLException {
        String getToysSQL = "SELECT * FROM " + GameCatDBSchema.ToysTable.NAME;

        boolean first = true;

        if (nameFilter.length() > 0) {
            getToysSQL = getToysSQL + " WHERE " + GameCatDBSchema.ToysTable.Cols.TOY_NAME + " LIKE '%" + UpdateQueries.fixQuoteStringIssue(nameFilter) + "%'";
            first = false;
        }
        if (toySearchFilter.length() > 0) {
            if (first) {
                getToysSQL = getToysSQL + " WHERE " + GameCatDBSchema.ToysTable.Cols.TOY_SET + " LIKE '%" + UpdateQueries.fixQuoteStringIssue(toySearchFilter) + "%' ";
                first = false;
            } else {
                getToysSQL = getToysSQL + " AND " + GameCatDBSchema.ToysTable.Cols.TOY_SET + " LIKE '%" + UpdateQueries.fixQuoteStringIssue(toySearchFilter) + "%'";
            }
        }
        if (platformFilter.length() > 0) {
            if (first) {
                getToysSQL = getToysSQL + " WHERE " + GameCatDBSchema.ToysTable.Cols.PLATFORM + " LIKE '%" + UpdateQueries.fixQuoteStringIssue(platformFilter) + "%' ";
            } else {
                getToysSQL = getToysSQL + " AND " + GameCatDBSchema.ToysTable.Cols.PLATFORM + " LIKE '%" + UpdateQueries.fixQuoteStringIssue(platformFilter) + "%'";
            }
        }

        getToysSQL = getToysSQL + " ORDER BY " + GameCatDBSchema.ToysTable.Cols.TOY_NAME + " ASC; ";

        List<Toy> results = new ArrayList<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getToysSQL);
            while (resultSet.next()) {
                int id = resultSet.getInt(GameCatDBSchema.ToysTable.Cols.TOY_ID);
                String name = resultSet.getString(GameCatDBSchema.ToysTable.Cols.TOY_NAME);
                String toySet = resultSet.getString(GameCatDBSchema.ToysTable.Cols.TOY_SET);
                String platform = resultSet.getString(GameCatDBSchema.ToysTable.Cols.PLATFORM);
                results.add(new Toy(id, name, toySet, platform));
            }
        }
        return results;
    }

    /**
     * the findAllGenresMap method
     * This method will build a map of using all of the genres found in the database
     * in the form Map<String, Integer> then return that to the program
     *
     * @param connection the connection to the database
     * @return a Map of the genres along with their ID's
     * @throws SQLException
     */
    public static Map<String, Integer> findAllGenresMap(Connection connection) throws SQLException {
        String getGenresSQL = "SELECT * FROM " + GameCatDBSchema.GenresTable.NAME + " ORDER BY " + GameCatDBSchema.GenresTable.Cols.GENRE_NAME + " ASC;";

        Map<String, Integer> results = new HashMap<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getGenresSQL);
            while (resultSet.next()) {
                Integer id = resultSet.getInt(GameCatDBSchema.GenresTable.Cols.GENRE_ID);
                String genre = resultSet.getString(GameCatDBSchema.GenresTable.Cols.GENRE_NAME);
                results.put(genre, id);
            }
        }
        return results;
    }

    /**
     * the findAllSystemsMap method
     * This method will build a map of using all of the systems found in the database
     * in the form Map<String, Integer> then return that to the program
     *
     * @param connection the connection to the database
     * @return a Map of the systems along with their ID's
     * @throws SQLException
     */
    public static Map<String, Integer> findAllSystemsMap(Connection connection) throws SQLException {
        String getSystemSQL = "SELECT * FROM " + GameCatDBSchema.SystemsTable.NAME + " ORDER BY " + GameCatDBSchema.SystemsTable.Cols.GENERATION + " ASC;";

        Map<String, Integer> results = new HashMap<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getSystemSQL);
            while (resultSet.next()) {
                Integer id = resultSet.getInt(GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID);
                String system = resultSet.getString(GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME);
                results.put(system, id);
            }
        }
        return results;
    }

    /**
     * the finalAllToysMap method
     * This method will build a map using all of the toys found in the database
     * in the form Map<String, Integer> then return that to the program
     *
     * @param connection the connection to the database
     * @return a Map of the toys along with their ID's
     * @throws SQLException
     */
    public static Map<String, Integer> finalAllToysMap(Connection connection) throws SQLException {
        String getToySQL = "SELECT * FROM " + GameCatDBSchema.ToysTable.NAME + " ORDER BY " + GameCatDBSchema.ToysTable.Cols.TOY_NAME + " ASC;";

        Map<String, Integer> results = new HashMap<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(getToySQL);
            while (resultSet.next()) {
                Integer id = resultSet.getInt(GameCatDBSchema.ToysTable.Cols.TOY_ID);
                String toy = resultSet.getString(GameCatDBSchema.ToysTable.Cols.TOY_NAME);
                results.put(toy, id);
            }
        }
        return results;
    }

    /**
     * the canAddNewGameToDb method
     * This method will check the title passed in and determine if that is being used currently
     * in the database as a title of a game, commonly used before adding a new game to the database
     *
     * @param title      the string that you are checking the title for
     * @param connection the connection to the database
     * @return true if that title is not being used, false if it is being used
     * @throws SQLException
     */
    public static boolean canAddNewGameToDb(String title, Connection connection) throws SQLException {
        String titleCheckSQL = "SELECT " + GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " FROM " + GameCatDBSchema.VideoGamesTable.NAME +
                " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " = '" + UpdateQueries.fixQuoteStringIssue(title) + "'";

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(titleCheckSQL);
            if (resultSet.isBeforeFirst()) {
                //there is a resultSet of one entry so this title is in use
                return false;
            }
        }
        return true;
    }

    /**
     * the canDeleteSystemFromDb method
     * This method will search the database to see if a system is being used on any games
     * this is commonly used before deleting a system from the database
     *
     * @param systemId   the systemId to be checked
     * @param connection the connection to the database
     * @return true if the system is not being used, false if it is being used
     * @throws SQLException
     */
    public static boolean canDeleteSystemFromDb(int systemId, Connection connection) throws SQLException {
        //can't delete the none system
        if (systemId == 0) {
            return false;
        }

        String systemCheckGameSQL = "SELECT " + GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID + " FROM " + GameCatDBSchema.VideoGamesTable.NAME +
                " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID + " = " + systemId;
        String systemCheckOtherSQL = "SELECT " + GameCatDBSchema.OtherSystemsTable.Cols.SYSTEM_ID + " FROM " + GameCatDBSchema.OtherSystemsTable.NAME +
                " WHERE " + GameCatDBSchema.OtherSystemsTable.Cols.SYSTEM_ID + " = " + systemId;
        String systemCheckRepeatSQL = "SELECT " + GameCatDBSchema.RepeatSystemsTable.Cols.SYSTEM_ID + " FROM " + GameCatDBSchema.RepeatSystemsTable.NAME +
                " WHERE " + GameCatDBSchema.RepeatSystemsTable.Cols.SYSTEM_ID + " = " + systemId;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(systemCheckGameSQL);
            if (resultSet.isBeforeFirst()) {
                return false;
            }
            resultSet = searchStmt.executeQuery(systemCheckOtherSQL);
            if (resultSet.isBeforeFirst()) {
                return false;
            }
            resultSet = searchStmt.executeQuery(systemCheckRepeatSQL);
            if (resultSet.isBeforeFirst()) {
                return false;
            }
        }
        return true;
    }

    /**
     * the canDeleteGenreFromDb method
     * This method will search the database to see if a genre is being used on any games
     * this is commonly used before deleting a genre from the database
     *
     * @param genreId    the genreId to be checked
     * @param connection the connection to the database
     * @return true if the genre is not being used, false if it is being used
     * @throws SQLException
     */
    public static boolean canDeleteGenreFromDb(int genreId, Connection connection) throws SQLException {
        //can't delete none
        if (genreId == 0) {
            return false;
        }
        String genreCheckGameSQL = "SELECT " + GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID + " FROM " + GameCatDBSchema.VideoGamesTable.NAME +
                " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID + " = " + genreId;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(genreCheckGameSQL);
            if (resultSet.isBeforeFirst()) {
                return false;
            }
        }
        return true;
    }

    /**
     * the runMasterListReportQuery
     * this will run a query then return a list of all games in the database back to the program
     *
     * @param connection the connection to the database
     * @return a List of all video games
     * @throws SQLException
     */
    public static List<Video_Game> runMasterListReportQuery(Connection connection) throws SQLException {
        String masterListReportSQL = "SELECT * FROM " + GameCatDBSchema.VideoGamesTable.NAME +
                " LEFT JOIN " + GameCatDBSchema.SystemsTable.NAME + " ON " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID +
                " = " + GameCatDBSchema.SystemsTable.NAME + "." + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID +
                " JOIN " + GameCatDBSchema.GenresTable.NAME + " ON " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID +
                " = " + GameCatDBSchema.GenresTable.NAME + "." + GameCatDBSchema.GenresTable.Cols.GENRE_ID +
                " ORDER BY " + GameCatDBSchema.SystemsTable.Cols.GENERATION + " ASC, " +
                GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " ASC, " +
                GameCatDBSchema.SystemsTable.Cols.IS_HANDHELD + " ASC, " +
                GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR + " ASC, " +
                GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " ASC;";

        List<Video_Game> results;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(masterListReportSQL);
            results = buildGameFromResultSet(connection, resultSet);
        }
        return results;
    }

    /**
     * the runFilterListReportQuery
     * this will run a query then return a list of all games that pass the filters back to the program
     *
     * @param connection the connection to the database
     * @param filters    the AdvancedSearchFilters
     * @return a List of all video games
     * @throws SQLException
     */
    //This method is unused
    @Deprecated
    public static List<Video_Game> runFilterListReportQuery(Connection connection, AdvancedSearchFilters filters) throws SQLException {
        String filterListReportSQL = buildQueryFromAdvancedSearchFilters(filters);

        List<Video_Game> results;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(filterListReportSQL);
            results = buildGameFromAdvanceSearchResultSet(connection, resultSet, filters);
        }
        return results;
    }

    /**
     * the systemCountQuery method
     * This will return the number of games on that console
     *
     * @param connection the connection to the database
     * @param systemId   the system that you want to count
     * @return the number of games with that system
     * @throws SQLException
     */
    public static int systemCountQuery(Connection connection, int systemId) throws SQLException {
        String countSQL = "SELECT COUNT(*) FROM " + GameCatDBSchema.VideoGamesTable.NAME + " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID + " = " + systemId;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(countSQL);
            return resultSet.getInt(1);
        }
    }

    /**
     * the genreCountQuery method
     * This will return the number of games in the genre parameter
     *
     * @param connection the connection to the database
     * @param genreId    the system that you want to count
     * @return the number of games from that genre
     * @throws SQLException
     */
    public static int genreCountQuery(Connection connection, int genreId) throws SQLException {
        String countSQL = "SELECT COUNT(*) FROM " + GameCatDBSchema.VideoGamesTable.NAME + " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID + " = " + genreId;

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(countSQL);
            return resultSet.getInt(1);
        }
    }

    /**
     * the runToyListReportQuery
     * This will return a list of all of the toys in the database
     *
     * @param connection the connection to the database
     * @return the list of all of the Toys
     * @throws SQLException
     */
    public static List<Toy> runToyListReportQuery(Connection connection) throws SQLException {
        String toyListReportSQL = "SELECT * FROM " + GameCatDBSchema.ToysTable.NAME;
        List<Toy> results = new ArrayList<>();

        try (Statement searchStmt = connection.createStatement()) {
            ResultSet resultSet = searchStmt.executeQuery(toyListReportSQL);
            while (resultSet.next()) {
                int id = resultSet.getInt(GameCatDBSchema.ToysTable.Cols.TOY_ID);
                String name = resultSet.getString(GameCatDBSchema.ToysTable.Cols.TOY_NAME);
                String toySet = resultSet.getString(GameCatDBSchema.ToysTable.Cols.TOY_SET);
                String platform = resultSet.getString(GameCatDBSchema.ToysTable.Cols.PLATFORM);
                Toy tempToy = new Toy(id, name, toySet, platform);
                results.add(tempToy);
            }
        }
        return results;
    }

    /**
     * the buildGameFromResultSet method
     * This method will take a result set from a query and then build a list of games from it
     *
     * @param connection the connection to the database
     * @param resultSet  the result Set to be converted
     * @return the list of Video Game Objects
     * @throws SQLException
     */
    private static List<Video_Game> buildGameFromResultSet(Connection connection, ResultSet resultSet) throws SQLException {
        List<Video_Game> videoGameList = new ArrayList<>();

        while (resultSet.next()) {
            int gameId = resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.GAME_ID);
            Video_Game tempGame = new Video_Game(gameId);
            tempGame.setTitle(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE));
            tempGame.setSystem(resultSet.getString(GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME));
            List<String> gameOtherSystems = searchOtherSystemsForGameById(connection, gameId);
            tempGame.setOtherSystems(gameOtherSystems);
            List<String> gameRepeatSystems = searchRepeatSystemsForGameById(connection, gameId);
            tempGame.setRepeatSystems(gameRepeatSystems);
            tempGame.setPlayerMin(resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.MIN_PLAYERS));
            tempGame.setPlayerMax(resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.MAX_PLAYERS));
            tempGame.setDeveloper(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.DEVELOPER));
            tempGame.setPublisher(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.PUBLISHER));
            tempGame.setOriginalReleaseYear(resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR));
            List<Integer> gameReReleaseYears = searchReReleaseYearsForGameById(connection, gameId);
            tempGame.setReReleaseYear(gameReReleaseYears);
            tempGame.setGenre(findGenreById(connection, resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID)));
            tempGame.setSeries(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.SERIES));
            tempGame.setCoreAesthetic(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.CORE_AESTHETIC));
            tempGame.setOtherAesthetic(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.OTHER_AESTHETIC));
            tempGame.setRecommendedFor(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.RECOMMENDED_FOR));
            tempGame.setCompletionLevel(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.COMPLETION_LEVEL));
            tempGame.setRating(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.RATING));
            tempGame.setComments(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.COMMENTS));
            tempGame.setThoughtsLink(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_LINK));
            tempGame.setThoughtsType(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_TYPE));
            if (resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.REVISIT) == 1) {
                tempGame.setRevisit(true);
            }
            if (resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.COUCH) == 1) {
                tempGame.setCouch(true);
            }
            if (resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.HALL_OF_FAME) == 1) {
                tempGame.setHallOfFame(true);
            }
            if (!videoGameList.contains(tempGame)){
                videoGameList.add(tempGame);
            }
        }
        return videoGameList;
    }

    /**
     * the buildGameFromResultSet method
     * This method will take a result set and the advanceSearchFilters from a query and then build a list of games from it
     *
     * @param connection the connection to the database
     * @param resultSet  the result Set to be converted
     * @param filters    the AdvancedSearchFilters to be used
     * @return the list of Video Game Objects
     * @throws SQLException
     */
    //This method is only called by another deprecated method
    @Deprecated
    private static List<Video_Game> buildGameFromAdvanceSearchResultSet(Connection connection, ResultSet resultSet, AdvancedSearchFilters filters) throws SQLException {
        List<Video_Game> videoGameList = new ArrayList<>();

        while (resultSet.next()) {
            int gameId = resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.GAME_ID);
            Video_Game tempGame = new Video_Game(gameId);
            tempGame.setTitle(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE));
            tempGame.setSystem(resultSet.getString(GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME));
            List<String> gameOtherSystems = searchOtherSystemsForGameById(connection, gameId);
            tempGame.setOtherSystems(gameOtherSystems);
            List<String> gameRepeatSystems = searchRepeatSystemsForGameById(connection, gameId);
            tempGame.setRepeatSystems(gameRepeatSystems);
            tempGame.setPlayerMin(resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.MIN_PLAYERS));
            tempGame.setPlayerMax(resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.MAX_PLAYERS));
            tempGame.setDeveloper(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.DEVELOPER));
            tempGame.setPublisher(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.PUBLISHER));
            tempGame.setOriginalReleaseYear(resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR));
            List<Integer> gameReReleaseYears = searchReReleaseYearsForGameById(connection, gameId);
            tempGame.setReReleaseYear(gameReReleaseYears);
            tempGame.setGenre(findGenreById(connection, resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID)));
            tempGame.setSeries(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.SERIES));
            tempGame.setCoreAesthetic(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.CORE_AESTHETIC));
            tempGame.setOtherAesthetic(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.OTHER_AESTHETIC));
            tempGame.setRecommendedFor(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.RECOMMENDED_FOR));
            tempGame.setCompletionLevel(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.COMPLETION_LEVEL));
            tempGame.setRating(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.RATING));
            tempGame.setComments(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.COMMENTS));
            tempGame.setThoughtsLink(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_LINK));
            tempGame.setThoughtsType(resultSet.getString(GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_TYPE));
            if (resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.REVISIT) == 1) {
                tempGame.setRevisit(true);
            }
            if (resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.COUCH) == 1) {
                tempGame.setCouch(true);
            }
            if (resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.HALL_OF_FAME) == 1) {
                tempGame.setHallOfFame(true);
            }

            boolean add = true;

            if (!filters.getOtherSystem().equals(Globals.NO_FILTER)) {
                add = tempGame.getOtherSystems().contains(filters.getOtherSystem());
            }

            if (!filters.getRepeatSystem().equals(Globals.NO_FILTER)) {
                add = tempGame.getRepeatSystems().contains(filters.getRepeatSystem());
            }

            if (!(filters.getReRelease() == Globals.NO_NUMBER_FILTER)) {
                add = tempGame.getReReleaseYears().contains(filters.getReRelease());
            }

            if (add) {
                videoGameList.add(tempGame);
            }

        }
        return videoGameList;
    }

    /**
     * the buildQueryFromAdvancedSearchFilters
     * This method will take a filters object and then build a SQL query used to find only return those games with it
     *
     * @param filters
     * @return the SQL query
     */
    //This method is only called by another deprecated method
    @Deprecated
    private static String buildQueryFromAdvancedSearchFilters(AdvancedSearchFilters filters) {
        //SELECT * FROM video_games
        //LEFT JOIN systems ON video_games.system_id = systems.system_id
        //JOIN genres ON video_games.genre_id = genres.genre_id
        String query = "SELECT * FROM " + GameCatDBSchema.VideoGamesTable.NAME +
                " LEFT JOIN " + GameCatDBSchema.SystemsTable.NAME +
                " ON " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID +
                " = " + GameCatDBSchema.SystemsTable.NAME + "." + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID +
                " JOIN " + GameCatDBSchema.GenresTable.NAME +
                " ON " + GameCatDBSchema.VideoGamesTable.NAME + "." + GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID +
                " = " + GameCatDBSchema.GenresTable.NAME + "." + GameCatDBSchema.GenresTable.Cols.GENRE_ID;

        boolean first = true;

        if (!filters.getTitle().equals(Globals.NO_FILTER)) {
            query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " LIKE '%" + UpdateQueries.fixQuoteStringIssue(filters.getTitle()) + "%'";
            first = false;
        }

        if (!filters.getSystem().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " = '" + filters.getSystem() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " = '" + filters.getSystem() + "'";
                first = false;
            }
        }

        if (!(filters.getPlayerMax() == Globals.NO_NUMBER_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.MAX_PLAYERS + " = " + filters.getPlayerMax();
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.MAX_PLAYERS + " = " + filters.getPlayerMax();
                first = false;
            }
        }

        if (!filters.getDeveloper().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.DEVELOPER + " = '" + filters.getDeveloper() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.DEVELOPER + " = '" + filters.getDeveloper() + "'";
                first = false;
            }
        }

        if (!filters.getPublisher().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.PUBLISHER + " = '" + filters.getPublisher() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.PUBLISHER + " = '" + filters.getPublisher() + "'";
                first = false;
            }
        }

        if (!(filters.getOriginallyReleased() == Globals.NO_NUMBER_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR + " = " + filters.getOriginallyReleased();
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR + " = " + filters.getOriginallyReleased();
                first = false;
            }
        }

        if (!filters.getGenre().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.GenresTable.Cols.GENRE_NAME + " = '" + filters.getGenre() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.GenresTable.Cols.GENRE_NAME + " = '" + filters.getGenre() + "'";
                first = false;
            }
        }

        if (!filters.getCoreAesthetic().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.CORE_AESTHETIC + " = '" + filters.getCoreAesthetic() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.CORE_AESTHETIC + " = '" + filters.getCoreAesthetic() + "'";
                first = false;
            }
        }

        if (!filters.getOtherAesthetic().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.OTHER_AESTHETIC + " = '" + filters.getOtherAesthetic() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.OTHER_AESTHETIC + " = '" + filters.getOtherAesthetic() + "'";
                first = false;
            }
        }

        if (!filters.getCompletionLevel().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.COMPLETION_LEVEL + " = '" + filters.getCompletionLevel() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.COMPLETION_LEVEL + " = '" + filters.getCompletionLevel() + "'";
                first = false;
            }
        }

        if (!filters.getRating().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.RATING + " = '" + filters.getRating() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.RATING + " = '" + filters.getRating() + "'";
                first = false;
            }
        }

        if (!filters.getThoughtsType().equals(Globals.NO_FILTER)) {
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_TYPE + " = '" + filters.getThoughtsType() + "'";
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_TYPE + " = '" + filters.getThoughtsType() + "'";
                first = false;
            }
        }

        if (filters.isUseRevisitFilter()) {
            int search = 0;
            if (filters.isRevisitFilter()) {
                search = 1;
            }
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.REVISIT + " = " + search;
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.REVISIT + " = " + search;
                first = false;
            }
        }

        if (filters.isUseCouchFilter()) {
            int search = 0;
            if (filters.isCouchFilter()) {
                search = 1;
            }
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.COUCH + " = " + search;
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.COUCH + " = " + search;
                first = false;
            }
        }

        if (filters.isUseHallOfFameFilter()) {
            int search = 0;
            if (filters.isHallOfFameFilter()) {
                search = 1;
            }
            if (!first) {
                query += " AND " + GameCatDBSchema.VideoGamesTable.Cols.HALL_OF_FAME + " = " + search;
            } else {
                query += " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.HALL_OF_FAME + " = " + search;
            }
        }

        return query;
    }
}
