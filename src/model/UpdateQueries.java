package model;

import controller.Program;
import controller.objectClasses.System;
import controller.objectClasses.Toy;
import controller.objectClasses.Video_Game;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * The UpdateQueries class
 * This class holds all of the SQL statements used to modify the database
 * This class cannot be instantiated, all of the methods are static
 *
 * @author Seth Condie
 * @version 6
 * @date 6/8/2017
 */
public final class UpdateQueries {

    private UpdateQueries() {}

    /**
     * the addNewGameToDb method
     * This will take the newGame passed in and add it to the database
     *
     * @param newGame    the game that you are going to add to the database, the title shouldn't already be in the database
     * @param connection the connection to the database
     * @return the ID of the new game after it has been added to the database
     * @throws SQLException
     */
    public static int addNewGameToDb(Video_Game newGame, Connection connection) throws SQLException {

        List<String> otherSystems = newGame.getOtherSystems();
        List<String> repeatSystems = newGame.getRepeatSystems();
        List<Integer> reReleaseYears = newGame.getReReleaseYears();

        int revisit;
        if (newGame.isShouldRevisit()) {
            revisit = 1;
        } else {
            revisit = 0;
        }
        int couch;
        if (newGame.isLocalCouch()) {
            couch = 1;
        } else {
            couch = 0;
        }
        int hallOfFame;
        if (newGame.isHallOfFame()) {
            hallOfFame = 1;
        } else {
            hallOfFame = 0;
        }

        int newGameId;

        try (Statement newStmt = connection.createStatement()) {
            newStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.VideoGamesTable.NAME + "( " +
                    GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.MIN_PLAYERS + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.MAX_PLAYERS + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.DEVELOPER + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.PUBLISHER + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.CORE_AESTHETIC + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.OTHER_AESTHETIC + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.RECOMMENDED_FOR + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.COMPLETION_LEVEL + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.RATING + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.COMMENTS + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_LINK + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_TYPE + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.REVISIT + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.COUCH + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.HALL_OF_FAME + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.SERIES + ") VALUES ('" +
                    fixQuoteStringIssue(newGame.getTitle()) + "', " +
                    Program.getSystemId(newGame.getSystem()) + ", " +
                    newGame.getPlayerMin() + ", " +
                    newGame.getPlayerMax() + ", '" +
                    fixQuoteStringIssue(newGame.getDeveloper()) + "', '" +
                    fixQuoteStringIssue(newGame.getPublisher()) + "', " +
                    newGame.getOriginalReleaseYear() + ", " +
                    Program.getGenreId(newGame.getGenre()) + ", '" +
                    newGame.getCoreAesthetic() + "', '" +
                    newGame.getOtherAesthetic() + "', '" +
                    fixQuoteStringIssue(newGame.getRecommendedFor()) + "', '" +
                    newGame.getCompletionLevel() + "', '" +
                    newGame.getRating() + "', '" +
                    fixQuoteStringIssue(newGame.getComments()) + "', '" +
                    newGame.getThoughtsLink() + "', '" +
                    newGame.getThoughtsType() + "', " +
                    revisit + ", " +
                    couch + ", " +
                    hallOfFame + ", '" +
                    fixQuoteStringIssue(newGame.getSeries()) + "');");

            ResultSet resultSet = newStmt.executeQuery("SELECT " + GameCatDBSchema.VideoGamesTable.Cols.GAME_ID + " FROM " + GameCatDBSchema.VideoGamesTable.NAME +
                    " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " = '" + fixQuoteStringIssue(newGame.getTitle()) + "';");
            newGameId = resultSet.getInt(GameCatDBSchema.VideoGamesTable.Cols.GAME_ID);

            for (String system : otherSystems) {
                newStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.OtherSystemsTable.NAME + " VALUES (" + newGameId + ", " + Program.getSystemId(system) + ");");
            }

            for (String system : repeatSystems) {
                newStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.RepeatSystemsTable.NAME + " VALUES (" + newGameId + ", " + Program.getSystemId(system) + ");");
            }

            for (int year : reReleaseYears) {
                newStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.ReReleaseYearsTable.NAME + " VALUES (" + newGameId + ", " + year + ");");
            }
        }
        return newGameId;
    }

    /**
     * the updateExistingGameInDb method
     * This will take the updateGame and use it to update that entry in the database
     *
     * @param updateGame the game that needs to be updated in the database
     * @param connection the connection to the database
     * @throws SQLException
     */
    public static void updateExistingGameInDb(Video_Game updateGame, Connection connection) throws SQLException {

        List<String> otherSystems = updateGame.getOtherSystems();
        List<String> repeatSystems = updateGame.getRepeatSystems();
        List<Integer> reReleaseYears = updateGame.getReReleaseYears();

        int revisit;
        if (updateGame.isShouldRevisit()) {
            revisit = 1;
        } else {
            revisit = 0;
        }
        int couch;
        if (updateGame.isLocalCouch()) {
            couch = 1;
        } else {
            couch = 0;
        }
        int hallOfFame;
        if (updateGame.isHallOfFame()) {
            hallOfFame = 1;
        } else {
            hallOfFame = 0;
        }

        try (Statement updateStmt = connection.createStatement()) {
            updateStmt.executeUpdate("UPDATE " + GameCatDBSchema.VideoGamesTable.NAME + " SET " +
                    GameCatDBSchema.VideoGamesTable.Cols.GAME_TITLE + " = '" + fixQuoteStringIssue(updateGame.getTitle()) + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.SYSTEM_ID + " = " + Program.getSystemId(updateGame.getSystem()) + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.MIN_PLAYERS + " = " + updateGame.getPlayerMin() + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.MAX_PLAYERS + " = " + updateGame.getPlayerMax() + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.DEVELOPER + " = '" + fixQuoteStringIssue(updateGame.getDeveloper()) + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.PUBLISHER + " = '" + fixQuoteStringIssue(updateGame.getPublisher()) + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.ORIGINAL_RELEASE_YEAR + " = " + updateGame.getOriginalReleaseYear() + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.GENRE_ID + " = " + Program.getGenreId(updateGame.getGenre()) + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.SERIES + " = '" + fixQuoteStringIssue(updateGame.getSeries()) + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.CORE_AESTHETIC + " = '" + updateGame.getCoreAesthetic() + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.OTHER_AESTHETIC + " = '" + updateGame.getOtherAesthetic() + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.RECOMMENDED_FOR + " = '" + fixQuoteStringIssue(updateGame.getRecommendedFor()) + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.COMPLETION_LEVEL + " = '" + updateGame.getCompletionLevel() + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.RATING + " = '" + updateGame.getRating() + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.COMMENTS + " = '" + fixQuoteStringIssue(updateGame.getComments()) + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_LINK + " = '" + updateGame.getThoughtsLink() + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.THOUGHTS_TYPE + " = '" + updateGame.getThoughtsType() + "', " +
                    GameCatDBSchema.VideoGamesTable.Cols.REVISIT + " = " + revisit + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.COUCH + " = " + couch + ", " +
                    GameCatDBSchema.VideoGamesTable.Cols.HALL_OF_FAME + " = " + hallOfFame + " WHERE " +
                    GameCatDBSchema.VideoGamesTable.Cols.GAME_ID + " = " + updateGame.getId() + ";");

            //delete the previous entries for this game
            updateStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.OtherSystemsTable.NAME + " WHERE " + GameCatDBSchema.OtherSystemsTable.Cols.GAME_ID + " = " + updateGame.getId() + ";");
            updateStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.RepeatSystemsTable.NAME + " WHERE " + GameCatDBSchema.RepeatSystemsTable.Cols.GAME_ID + " = " + updateGame.getId() + ";");
            updateStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.ReReleaseYearsTable.NAME + " WHERE " + GameCatDBSchema.ReReleaseYearsTable.Cols.GAME_ID + " = " + updateGame.getId() + ";");

            //then add the new or old entries
            for (String system : otherSystems) {
                updateStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.OtherSystemsTable.NAME + " VALUES (" + updateGame.getId() + ", " + Program.getSystemId(system) + ");");
            }

            for (String system : repeatSystems) {
                updateStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.RepeatSystemsTable.NAME + " VALUES (" + updateGame.getId() + ", " + Program.getSystemId(system) + ");");
            }

            for (int year : reReleaseYears) {
                updateStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.ReReleaseYearsTable.NAME + " VALUES (" + updateGame.getId() + ", " + year + ");");
            }
        }
    }

    /**
     * the deleteGameFromDb method
     * This will take the ID of the game that should be deleted and then remove it from the database
     *
     * @param videoGameId the gameID to be removed from the database
     * @param connection  the connection to the database
     * @throws SQLException
     */
    public static void deleteGameFromDb(int videoGameId, Connection connection) throws SQLException {
        try (Statement deleteStmt = connection.createStatement()) {
            deleteStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.VideoGamesTable.NAME + " WHERE " + GameCatDBSchema.VideoGamesTable.Cols.GAME_ID + " = " + videoGameId + ";");
            deleteStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.OtherSystemsTable.NAME + " WHERE " + GameCatDBSchema.OtherSystemsTable.Cols.GAME_ID + " = " + videoGameId + ";");
            deleteStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.RepeatSystemsTable.NAME + " WHERE " + GameCatDBSchema.RepeatSystemsTable.Cols.GAME_ID + " = " + videoGameId + ";");
            deleteStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.ReReleaseYearsTable.NAME + " WHERE " + GameCatDBSchema.ReReleaseYearsTable.Cols.GAME_ID + " = " + videoGameId + ";");
        }
    }

    /**
     * the addNewSystemToDb method
     * This will take a new System and then add it to the database then return the new ID
     *
     * @param newSystem  the system to be added
     * @param connection the connection to the database
     * @return the ID of the system that is after it has been added to the database
     * @throws SQLException
     */
    public static int addNewSystemToDb(System newSystem, Connection connection) throws SQLException {

        int isHandheld;
        if (newSystem.isHandheld()) {
            isHandheld = 1;
        } else {
            isHandheld = 0;
        }

        String getNewIdSQL = "SELECT " + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + " FROM " + GameCatDBSchema.SystemsTable.NAME +
                " WHERE " + GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " = '" + newSystem.getName() + "';";

        try (Statement newStmt = connection.createStatement()) {
            newStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.SystemsTable.NAME + "(" +
                    GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + ", " +
                    GameCatDBSchema.SystemsTable.Cols.GENERATION + ", " +
                    GameCatDBSchema.SystemsTable.Cols.IS_HANDHELD + ") VALUES ('" +
                    newSystem.getName() + "', " +
                    newSystem.getGeneration() + ", " +
                    isHandheld + ");");

            ResultSet resultSet = newStmt.executeQuery(getNewIdSQL);

            int newId = resultSet.getInt(GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID);
            return newId;
        }
    }

    /**
     * the updateExistingSystemInDb method
     * This will take an exist system and modify it in the database
     *
     * @param updateSystem the system to be updated
     * @param connection   the connection to the database
     * @throws SQLException
     */
    public static void updateExistingSystemInDb(System updateSystem, Connection connection) throws SQLException {

        int isHandheld;
        if (updateSystem.isHandheld()) {
            isHandheld = 1;
        } else {
            isHandheld = 0;
        }

        try (Statement updateStmt = connection.createStatement()) {
            updateStmt.executeUpdate("UPDATE " + GameCatDBSchema.SystemsTable.NAME + " SET " +
                    GameCatDBSchema.SystemsTable.Cols.SYSTEM_NAME + " = '" + updateSystem.getName() + "', " +
                    GameCatDBSchema.SystemsTable.Cols.GENERATION + " = " + updateSystem.getGeneration() + ", " +
                    GameCatDBSchema.SystemsTable.Cols.IS_HANDHELD + " = " + isHandheld + " WHERE " +
                    GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + " = " + updateSystem.getId() + ";");
        }
    }

    /**
     * the deleteSystemFromDb method
     * This will take an existing system in the database and delete it
     *
     * @param systemId   the id of the system to be deleted, this system should not be used on any games before it is deleted
     * @param connection the connection to the database
     * @throws SQLException
     */
    public static void deleteSystemFromDb(int systemId, Connection connection) throws SQLException {
        //don't delete the 'none' system
        if (systemId == 1) {
            return;
        }

        try (Statement updateStmt = connection.createStatement()) {
            updateStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.SystemsTable.NAME + " WHERE " + GameCatDBSchema.SystemsTable.Cols.SYSTEM_ID + " = " + systemId + ";");
        }
    }

    /**
     * the addNewGenreToDb method
     * This will take a genre and add it to the database
     *
     * @param newGenre   the genre to be added
     * @param connection the connection to the database
     * @throws SQLException
     */
    public static void addNewGenreToDb(String newGenre, Connection connection) throws SQLException {
        try (Statement newStmt = connection.createStatement()) {
            newStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.GenresTable.NAME + " (" +
                    GameCatDBSchema.GenresTable.Cols.GENRE_NAME + ") VALUES ('" +
                    newGenre + "');");
        }
    }

    /**
     * the updateExistingGenreInDb method
     * This will take an existing genre and update it in the database
     *
     * @param newGenre   the new String that this genre will become
     * @param oldGenre   the old String that is going to be replaced
     * @param connection the connection to the database
     * @throws SQLException
     */
    public static void updateExistingGenreInDb(String newGenre, String oldGenre, Connection connection) throws SQLException {
        try (Statement updateStmt = connection.createStatement()) {
            updateStmt.executeUpdate("UPDATE " + GameCatDBSchema.GenresTable.NAME + " SET " +
                    GameCatDBSchema.GenresTable.Cols.GENRE_NAME + " = '" + newGenre + "' WHERE " +
                    GameCatDBSchema.GenresTable.Cols.GENRE_NAME + " = '" + oldGenre + "';");
        }
    }

    /**
     * the deleteGenreFromDb method
     * This will take an existing genre and delete it from the database
     *
     * @param deleteGenre the genre to be deleted, it should not be used on any games if it's going to be deleted
     * @param connection  the connection to the database
     * @throws SQLException
     */
    public static void deleteGenreFromDb(String deleteGenre, Connection connection) throws SQLException {
        if (deleteGenre.equals("none")) {
            return;
        }

        try (Statement updateStmt = connection.createStatement()) {
            updateStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.GenresTable.NAME + " WHERE " + GameCatDBSchema.GenresTable.Cols.GENRE_NAME + " = '" + deleteGenre + "';");
        }
    }

    /**
     * The addNewToyToDb method
     * This will take a toy and add it to the database then return the new Id
     *
     * @param newToy     the toy to be added
     * @param connection the connection to the database
     * @return the id of the newly added Toy
     * @throws SQLException
     */
    public static int addNewToyToDb(Toy newToy, Connection connection) throws SQLException {
        String getNewIdSQL = "SELECT " + GameCatDBSchema.ToysTable.Cols.TOY_ID + " FROM " + GameCatDBSchema.ToysTable.NAME +
                " WHERE " + GameCatDBSchema.ToysTable.Cols.TOY_NAME + " = '" + fixQuoteStringIssue(newToy.getName()) + "';";

        try (Statement newStmt = connection.createStatement()) {
            newStmt.executeUpdate("INSERT INTO " + GameCatDBSchema.ToysTable.NAME + "(" +
                    GameCatDBSchema.ToysTable.Cols.TOY_NAME + ", " +
                    GameCatDBSchema.ToysTable.Cols.PLATFORM + ", " +
                    GameCatDBSchema.ToysTable.Cols.TOY_SET + ") VALUES ('" +
                    fixQuoteStringIssue(newToy.getName()) + "', '" +
                    fixQuoteStringIssue(newToy.getPlatform()) + "', '" +
                    fixQuoteStringIssue(newToy.getToySet()) + "')");

            ResultSet resultSet = newStmt.executeQuery(getNewIdSQL);

            int newId = resultSet.getInt(GameCatDBSchema.ToysTable.Cols.TOY_ID);
            return newId;
        }
    }

    /**
     * the updateExistingToyInDb method
     * This will take an Toy already found in the database and update the information for it
     *
     * @param updateToy  the toy to be edited
     * @param connection the connection to the database
     * @throws SQLException
     */
    public static void updateExistingToyInDb(Toy updateToy, Connection connection) throws SQLException {
        try (Statement updateStmt = connection.createStatement()) {
            updateStmt.executeUpdate("UPDATE " + GameCatDBSchema.ToysTable.NAME + " SET " +
                    GameCatDBSchema.ToysTable.Cols.TOY_NAME + " = '" + fixQuoteStringIssue(updateToy.getName()) + "', " +
                    GameCatDBSchema.ToysTable.Cols.TOY_SET + " = '" + fixQuoteStringIssue(updateToy.getToySet()) + "', " +
                    GameCatDBSchema.ToysTable.Cols.PLATFORM + " = '" + fixQuoteStringIssue(updateToy.getPlatform()) + "' WHERE " +
                    GameCatDBSchema.ToysTable.Cols.TOY_ID + " = " + updateToy.getId() + ";");
        }
    }

    /**
     * the deleteToyFromDb method
     * This will take an existing toy in the database and delete it
     *
     * @param toyId      the id of the toy to be deleted
     * @param connection the connection to the database
     * @throws SQLException
     */
    public static void deleteToyFromDb(int toyId, Connection connection) throws SQLException {
        try (Statement updateStmt = connection.createStatement()) {
            updateStmt.executeUpdate("DELETE FROM " + GameCatDBSchema.ToysTable.NAME + " WHERE " + GameCatDBSchema.ToysTable.Cols.TOY_ID + " = " + toyId + ";");
        }
    }

    /**
     * the fixQuoteStringIssue
     * This will take a String and replace all of the ' with '' so that they will work with SQL queries
     *
     * @param toChange the string to be adjusted
     * @return the string after it has been changed
     * @throws SQLException
     */
    protected static String fixQuoteStringIssue(String toChange) {
        return toChange.replaceAll("\'", "\'\'");
    }
}
