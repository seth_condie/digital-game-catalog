# Welcome to my Digital Game Catalog Repository #

### This repository is a personal project of mine.  This application will take data as input from the user, create java objects, and then save them to a SQLite database for persistence.  This data can then be retrieved and then searched, filtered, and displayed in some reports inside the application. ###

Steps to setup this project with IntelliJ IDE

	• Clone from Bitbucket
	
	• Open project with intelliJ
	
	• Click File > Project Structure > Set the Project SDK to 1.8 and the language level to 8
	
	• Also in Project Structure go to Libraries > + > Java > navigate to the SQLiteDriver.jar include that as a library.
	
	• Right click on the src folder, select Mark Directory As > Source Root
	
	• Click Run > Edit Configurations > + > Application > Fill in a name, and select the Main class: as Program > click OK
	
	• You may need to open the front end gui classes and remove the icon
	
Select the Project Configuration that you created earlier and click run, the project will compile and open.

This project requires SQLite Driver, and was created with the help of JFormDesigner, and Launch4J

Additional questions can be sent to my e-mail sethhaskellcondie@yahoo.com